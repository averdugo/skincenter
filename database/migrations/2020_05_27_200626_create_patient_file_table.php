<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePatientFileTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('patient_file', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id');
            $table->integer('clientservice_id');
            $table->integer('created_by');
            $table->integer('est_id');
            $table->date('date');
            $table->integer('session');
            $table->string('treatment_area')->nullable();
            $table->string('wavelength')->nullable();
            $table->string('j_cm2')->nullable();
            $table->string('wide_pulse')->nullable();
            $table->string('size_mm')->nullable();
            $table->longText('observations');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('patient_file');
    }
}
