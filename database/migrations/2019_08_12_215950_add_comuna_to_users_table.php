<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddComunaToUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->string('comuna')->nullable();
			$table->date('birthday_at')->nullable();
			$table->float('weight',8,2)->nullable();
			$table->float('height',8,2)->nullable();
			$table->string('refer_by')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
			$table->string('comuna')->nullable();
			$table->date('birthday_at')->nullable();
			$table->float('weight',8,2)->nullable();
			$table->float('height',8,2)->nullable();
			$table->string('refer_by')->nullable();
        });
    }
}
