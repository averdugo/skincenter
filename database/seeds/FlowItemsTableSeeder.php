<?php

use Illuminate\Database\Seeder;

class FlowItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('flow_items')->insert([
		   'name' => 'Productos',
		   'cashflow_id' => 1,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Servicios',
		   'cashflow_id' => 1,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Promociones',
		   'cashflow_id' => 1,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'GiftCard',
		   'cashflow_id' => 1,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Agua',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Luz',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Triple pack',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Celular',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Alarma',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Seguros',
		   'cashflow_id' => 2,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Eliminación de residuos',
		   'cashflow_id' => 2,
	   	]);

		DB::table('flow_items')->insert([
		   'name' => 'Crédito de consumo banco chile',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Crédito de consumo banco estado',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Crédito de consumo banco santander',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Impuestos banco Chile',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Impuestos banco Estado',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Impuestos banco Santander',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Intereses linea de crédito banco Chile',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Intereses linea de crédito banco Estado',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Intereses linea de crédito banco Santander',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Tarjeta de crédito Banco Santander',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Mantención banco Chile',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Mantención banco Estado',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Mantención banco Santander',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Transbank',
		   'cashflow_id' => 3,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Leasing banco Santander',
		   'cashflow_id' => 3,
	   	]);

		DB::table('flow_items')->insert([
		   'name' => 'Equipos láser',
		   'cashflow_id' => 4,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Otros equipos',
		   'cashflow_id' => 4,
	   	]);

		DB::table('flow_items')->insert([
		   'name' => 'Community Manager',
		   'cashflow_id' => 5,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Diseños',
		   'cashflow_id' => 5,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Facebook ads',
		   'cashflow_id' => 5,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Instagram ads',
		   'cashflow_id' => 5,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Impresiones',
		   'cashflow_id' => 5,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Otros',
		   'cashflow_id' => 5,
	   	]);


		DB::table('flow_items')->insert([
		   'name' => 'Arriendo',
		   'cashflow_id' => 6,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Mano de obra',
		   'cashflow_id' => 6,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Materiales',
		   'cashflow_id' => 6,
	   	]);


		DB::table('flow_items')->insert([
		   'name' => 'Descartables',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Spa',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Skin centro',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Dabalash',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Fontbote venta',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Dermik venta',
		   'cashflow_id' => 7,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Labcconte venta',
		   'cashflow_id' => 7,
	   	]);


		DB::table('flow_items')->insert([
		   'name' => 'Hilos',
		   'cashflow_id' => 8,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Ac. Hialurónico',
		   'cashflow_id' => 8,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Botox',
		   'cashflow_id' => 8,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Filorga',
		   'cashflow_id' => 8,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Insumos descartables',
		   'cashflow_id' => 8,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Otros',
		   'cashflow_id' => 8,
	   	]);


		DB::table('flow_items')->insert([
		   'name' => 'Oficina',
		   'cashflow_id' => 9,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Cocina',
		   'cashflow_id' => 9,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Aseo',
		   'cashflow_id' => 9,
	   	]);

		DB::table('flow_items')->insert([
		   'name' => 'Impuestos',
		   'cashflow_id' => 10,
	   	]);


		DB::table('flow_items')->insert([
		   'name' => 'Fletes',
		   'cashflow_id' => 11,
	   	]);

		DB::table('flow_items')->insert([
		   'name' => 'Gastos sin justificar',
		   'cashflow_id' => 11,
	   	]);
		DB::table('flow_items')->insert([
		   'name' => 'Otros sin justificar',
		   'cashflow_id' => 11,
	   	]);
    }
}
