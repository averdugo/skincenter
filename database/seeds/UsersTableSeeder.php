<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('users')->insert([
		   'name' => 'Administrador',
		   'email' => 'admin@gmail.com',
		   'password' => bcrypt('skincenter'),
		   'phone' => '+56988',
		   'type' => 1
	   ]);
	   DB::table('users')->insert([
		  'name' => 'Admin',
		  'email' => 'admin@gmail.com',
		  'password' => bcrypt('skincenter'),
		  'phone' => '+5698',
		  'type' => 1
	  ]);
    }
}
