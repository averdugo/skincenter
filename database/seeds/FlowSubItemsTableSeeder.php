<?php

use Illuminate\Database\Seeder;

class FlowSubItemsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('flow_sub_items')->insert([
		   'name' => 'Ondas',
		   'cashflowitems_id' => 2,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Vacumterapia',
		   'cashflowitems_id' => 2,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Radiofrecuencia',
		   'cashflowitems_id' => 2,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Ultracavitación',
		   'cashflowitems_id' => 2,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Masaje + Yeso',
		   'cashflowitems_id' => 2,
	   	]);


		DB::table('flow_sub_items')->insert([
		   'name' => 'Hollywood peel',
		   'cashflowitems_id' => 3,
	   	]);

		DB::table('flow_sub_items')->insert([
		   'name' => 'Peeling',
		   'cashflowitems_id' => 3,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Rem tatuajes',
		   'cashflowitems_id' => 3,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Dermapen',
		   'cashflowitems_id' => 3,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Melasma',
		   'cashflowitems_id' => 3,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Léntigos',
		   'cashflowitems_id' => 3,
	   	]);

		DB::table('flow_sub_items')->insert([
		   'name' => 'Limpiezas faciales',
		   'cashflowitems_id' => 4,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Bronceado',
		   'cashflowitems_id' => 4,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Masoterapia',
		   'cashflowitems_id' => 4,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Permanente de pestañas',
		   'cashflowitems_id' => 4,
	   	]);


		DB::table('flow_sub_items')->insert([
		   'name' => 'Hilos de PDO',
		   'cashflowitems_id' => 5,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Rinomodelación',
		   'cashflowitems_id' => 5,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Dermapen medico',
		   'cashflowitems_id' => 5,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Mesoterapia',
		   'cashflowitems_id' => 5,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Relleno con acido hialuronico',
		   'cashflowitems_id' => 5,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Botox',
		   'cashflowitems_id' => 5,
	   	]);


		DB::table('flow_sub_items')->insert([
		   'name' => 'Dabalash',
		   'cashflowitems_id' => 6,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Copita menstrual',
		   'cashflowitems_id' => 6,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Dr fontbote',
		   'cashflowitems_id' => 6,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Bloqueadores',
		   'cashflowitems_id' => 6,
	   	]);

		DB::table('flow_sub_items')->insert([
		   'name' => 'Mantención Spectra',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Mantención Mini 1',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Mantención Mini 2',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Mantención Pro',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Repuestos Spectra',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Repuestos Mini',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Repuestos Pro',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Reparaciones',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Consumibles',
		   'cashflowitems_id' => 30,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Criogeno',
		   'cashflowitems_id' => 30,
	   	]);


		DB::table('flow_sub_items')->insert([
		   'name' => 'Reparaciones',
		   'cashflowitems_id' => 31,
	   	]);
		DB::table('flow_sub_items')->insert([
		   'name' => 'Repuestos',
		   'cashflowitems_id' => 31,
	   	]);
    }
}
