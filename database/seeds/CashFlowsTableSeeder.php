<?php

use Illuminate\Database\Seeder;

class CashFlowsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('cash_flows')->insert([
		   'name' => 'Ingresos',
		   'type' => 1,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Gastos fijos',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Gastos bancarios',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Equipos',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Marketing',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Gastos de local',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Insumos local',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Insumos medicina estética',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Gastos básicos',
		   'type' => 2,
	   	]);

		DB::table('cash_flows')->insert([
		   'name' => 'Impuestos',
		   'type' => 2,
	   	]);
		DB::table('cash_flows')->insert([
		   'name' => 'Otros',
		   'type' => 2,
	   	]);



    }
}
