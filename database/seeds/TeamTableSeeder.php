<?php

use Illuminate\Database\Seeder;

class TeamTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('teams')->insert([
			'store_id'=>1,
			'name'=>'Carolina Matus',
			'profession'=>'Kinesióloga',
			'description'=>'',
			'image'=>'carolina_matus.jpg',
			'status'=>'1'
	   	]);

	   	DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Josseline Paredes',
		   'profession'=>'Recepcionista',
		   'description'=>'',
		   'image'=>'josseline_paredes.jpg',
		   'status'=>'1'
	  	]);

		DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Andrea Tarbes',
		   'profession'=>'Enfermera',
		   'description'=>'',
		   'image'=>'andre_tarbes.jpg',
		   'status'=>'1'
	  	]);

		DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Claudia Gonzales',
		   'profession'=>'Kinesióloga',
		   'description'=>'',
		   'image'=>'claudia_gonzales.jpg',
		   'status'=>'1'
	  	]);

		DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Javiera Troncoso',
		   'profession'=>'Kinesióloga',
		   'description'=>'',
		   'image'=>'javiera_troncoso.jpg',
		   'status'=>'1'
	  	]);

		DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Maria Jose Toro',
		   'profession'=>'Recepcionista',
		   'description'=>'',
		   'image'=>'maria_toro.jpg',
		   'status'=>'1'
	  	]);

		DB::table('teams')->insert([
		   'store_id'=>1,
		   'name'=>'Stefannie Molsave',
		   'profession'=>'Kinesióloga',
		   'description'=>'',
		   'image'=>'stefannie_molsave.jpg',
		   'status'=>'1'
	  	]);


    }
}
