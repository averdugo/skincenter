<?php

use Illuminate\Database\Seeder;

class TimetableTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('timetables')->insert([
			'category_id' => 6,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '9:30',
			'end_time' => '20:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 6,
			'start_day' => 6,
			'end_day' => 6,
			'start_time' => '10:00',
			'end_time' => '13:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 6,
			'start_day' => 6,
			'end_day' => 6,
			'start_time' => '14:00',
			'end_time' => '17:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 8,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '9:30',
			'end_time' => '20:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 8,
			'start_day' => 6,
			'end_day' => 6,
			'start_time' => '10:00',
			'end_time' => '20:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 9,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '10:00',
			'end_time' => '13:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 9,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '15:00',
			'end_time' => '20:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 9,
			'start_day' => 6,
			'end_day' => 6,
			'start_time' => '10:00',
			'end_time' => '14:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 9,
			'start_day' => 6,
			'end_day' => 6,
			'start_time' => '15:00',
			'end_time' => '17:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 4,
			'start_day' => 1,
			'end_day' => 6,
			'start_time' => '10:00',
			'end_time' => '13:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 4,
			'start_day' => 1,
			'end_day' => 6,
			'start_time' => '15:00',
			'end_time' => '20:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 7,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '10:00',
			'end_time' => '13:00'
		]);
		DB::table('timetables')->insert([
			'category_id' => 7,
			'start_day' => 1,
			'end_day' => 5,
			'start_time' => '15:00',
			'end_time' => '20:00'
		]);
    }
}
