<?php

use Illuminate\Database\Seeder;

class StoresTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
		DB::table('stores')->insert([
		   'name' => 'Skin Center Los Angeles',
		   'address' => 'Avenida Alemania #258, local 2',
		   'comuna' => 'Los Ángeles',
		   'region' => 'Region del Biobío',
		   'phone' => '43-2326622',
		   'lat'=> -37.469538,
		   'lng'=> -72.34736
	   ]);
	   DB::table('stores')->insert([
		  'name' => 'Skin Center Temuco',
		  'address' => 'Hochstetter #1069, local 1',
		  'comuna' => 'Temuco',
		  'region' => 'Region del Biobío',
		  'phone' => '45-2427090',
		  'lat' => -38.740103,
		  'lng' => -72.6205087
	  ]);
	  DB::table('stores')->insert([
		 'name' => 'Skin Center Concepción',
		 'address' => 'Paicavi #483',
		 'comuna' => 'Concepción',
		 'region' => 'Region del Biobío',
		 'phone' => '41-2462695',
		 'lat' => -36.8234067,
		 'lng' => -73.0445063
	 ]);
    }
}
