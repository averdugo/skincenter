<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/', 'WebController@index');

Route::get('/productos/{id}', 'WebController@productos');

Route::get('/perfil', 'WebController@userSection');

Route::get('/productos/{id}/details', 'WebController@pDetails');

Route::get('/promociones/{id}/details', 'WebController@proDetails');

Route::get('/servicios/{name}/{id}', 'WebController@sDetails');

Route::get('/giftcards/{id}', 'WebController@gDetails');

Route::get('/servicios/{name}', 'WebController@servicios');

Route::get('/contacto', 'WebController@contacto');

Route::get('/promociones', 'WebController@promociones');

Route::get('/giftcards', 'WebController@giftcards');

Auth::routes();

Route::get('/logOut', function(){
	Auth::logout();
	return Redirect::back();
});

Route::get('/home', 'HomeController@index');

//Route::get('/administrador', 'AdminController@login');

Route::get('/administrador/dashboard', 'AdminController@index');

Route::get('/administrador/productos', 'AdminController@products');

Route::get('/administrador/categorias', 'AdminController@categories');

Route::get('/administrador/servicios', 'AdminController@services');

Route::get('/administrador/usuarios', 'AdminController@users');

Route::get('/administrador/clientes', 'AdminController@clients');

Route::get('/administrador/agenda', 'AdminController@schedule');

Route::get('/administrador/giftcards', 'AdminController@giftcards');

Route::get('/administrador/promociones', 'AdminController@promociones');

Route::get('/administrador/ventas', 'AdminController@sales');

Route::get('/administrador/flujo_caja', 'AdminController@cashFlow');

Route::resource('/products','ProductsController');

Route::resource('/imagesApp','ImagesController');

Route::resource('/categories','CategoryController');

Route::resource('/user_history', 'UserHistoryController');

Route::post('/categories/addBox','CategoryController@addBox');

Route::get('/categories/deleteBox/{id}/{category}','CategoryController@deleteBox');

Route::resource('/services','ServicesController');

Route::resource('/users','UserController');

Route::resource('/giftcardsApp','GiftCardController');

Route::resource('/promotions','PromotionController');

Route::resource('/cartApp','CartController');

Route::resource('/sales','SalesController');

Route::get('/getHoursByDate/{date}/{item}', 'ScheduleController@getHoursByDate');

Route::resource('/schedules','ScheduleController');

Route::get('/goDahsboard/{store}', 'AdminController@goDahsboard');

Route::get('/goStore/{store}', 'WebController@goStore');

Route::get('/goTransbank', 'CartController@goTransbank');

Route::get('/goTransbankResponse', 'CartController@goTransbankResponse');

Route::post('/goTransbankResponse', 'CartController@goTransbankResponse');

Route::post('/goTransbankFinish', 'CartController@goTransbankFinish');

Route::get('login/{provider}', 'Auth\LoginController@redirectToProvider');

Route::get('login/{provider}/callback', 'Auth\LoginController@handleProviderCallback');

Route::get('web/getService/{id}', 'WebController@getService');
Route::get('web/getService2/{id}', 'WebController@getService2');
Route::post('web/reservationRequest', 'WebController@reservationRequest');

Route::post('web/contact', 'WebController@contact');

Route::get('/cartClean', 'CartController@clear');

Route::get('/users/estetic/{store_id}', 'UserController@estetic');

Route::post('/getDoctorsByDate', 'WebController@getDoctorsByDate');

Route::post('/schedules/clientStore', 'ScheduleController@clientStore');

Route::get('/tratamiento/{id}','WebController@tratamientos');

Route::get('/tratamiento/{id}/{sub}','WebController@subtratamientos');

Route::post('/categoryImgApp/{id}', 'CategoryController@categoryImgApp');

Route::get('/categories/getSubCategories/{id}', 'CategoryController@getSubCategories');

Route::post('/categories/addSub', 'CategoryController@addSub');

Route::get('/categories/deleteSub/{id}/{category}','CategoryController@deleteSub');

Route::get('/categoriesServices', 'CategoryController@categoriesServices');

Route::get('/administrador/cashFlowEgresos/{type}/{id}', 'AdminController@cashFlowEgresos');

Route::post('/administrador/cashFlowEgresos/{type}/{id}', 'AdminController@cashFlowEgresosP');

Route::get('/administrador/cashFlowIngresos/{type}/{id}', 'AdminController@cashFlowIngresos');

Route::post('/administrador/cashFlowIngresos/{type}/{id}', 'AdminController@cashFlowIngresosP');

Route::resource('/expenses', 'ExpensesController');

Route::get('/getCategoryByType/{type}', 'CategoryController@getCategoryByType');

Route::get('/getItemsByType/{type}/{cat}', 'AdminController@getItemsByType');

Route::get('/salesItems/{id}', 'SalesController@salesItems');

Route::get('/getSalesbyUser/{id}', 'SalesController@getSalesbyUser');

Route::get('/saleitem/{id}', 'SalesController@saleitem');

Route::post('/saleItemStatus', 'SalesController@saleItemStatus');

Route::get('/administrador/historial_venta/{id}', 'AdminController@history_sales');

Route::get('/goFlow', 'CartController@goFlow');

Route::post('/flowConfirm', 'CartController@flowConfirm');

Route::post('/flowResult', 'CartController@flowResult');

Route::put('/usersUpdate/{id}', 'UserController@update2');

Route::resource('/administrador/cliente/{id}/tratamientos', 'ClientServiceController');

Route::resource('/administrador/tratamientosFicha', 'PatientFileController');

Route::get('/venta', 'UserController@ventaT');

Route::get('/serviceFindBySubcategory/{id}', 'ServicesController@findBySubcategory');

Route::post('/clientService', 'ServicesController@clientService');

Route::post('/subirConsentimiento', 'ClientServiceController@subirConsentimiento');

Route::resource('/clientServices', 'ClientServiceController');


