const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/productCrud.js',
],'public/js/products.js');
/*
mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/categoryCrud.js',
],'public/js/category.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/servicesCrud.js',
],'public/js/services.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/usersCrud.js',
],'public/js/users.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/clientsCrud.js',
],'public/js/clients.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/giftCardCrud.js',
],'public/js/giftcards.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'node_modules/@ckeditor/ckeditor5-build-classic/build/ckeditor.js',
	'node_modules/@ckeditor/ckeditor5-vue/dist/ckeditor.js',
	'resources/assets/js/promotionCrud.js',
],'public/js/promotions.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'resources/assets/js/agendaCrud.js',
],'public/js/agenda.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'resources/assets/js/saleCrud.js',
],'public/js/sales.js');

mix.scripts([
	'resources/assets/js/toastr.js',
	'resources/assets/js/vue.js',
	'resources/assets/js/axios.js',
	'resources/assets/js/web.js',
],'public/js/web.js');
*/