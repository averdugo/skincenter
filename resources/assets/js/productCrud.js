var ProductsVue = new Vue({
	el: '#mainProducts',
	components: {
		   ckeditor: CKEditor.component
	},
	created: function() {
		this.getList();
	},
	data: {
		url:'/products',
		lists: [],
        editor: ClassicEditor,
        editorData: '<p>Descripcion.</p>',
		editorConfig: {},
		elementId:'',
		file: '',
		file2: '',
		data:{},
		element:{},
		errors:[],
		errors2:[]
	},
	methods: {
		getList: function(){
			axios.get(this.url).then(response => {
				console.log(response)
				this.lists = response.data;
				if(this.lists.lenght > 0)
					this.showElement(this.lists[0].id);
			});
		},
		submitImage(){
			
			let formData = new FormData();
		    formData.append('file', this.file2);
			formData.append('type', 1);
			
			if (this.element.images) {
				formData.append('id', this.element.images.id);
			}else{
				formData.append('foreign_id', this.element.id);
			}

			axios.post('/imagesApp', formData, { headers: {'Content-Type': 'multipart/form-data'} }
		   ).then( r => {
			   this.getList();
			   $('#editPortadaModal').modal('hide');
			   this.file2 = '';
			   this.showElement(this.element.id)
		   })
		   .catch( error =>{
			   console.log(error);
		   });
		},
		
		submit(){
		    let formData = new FormData();
		    formData.append('file', this.file);
			formData.append('data',JSON.stringify(this.data));

		    axios.post(this.url, formData, { headers: {'Content-Type': 'multipart/form-data'} }
			).then( r => {
				this.getList();
				this.data = {};
				$("#createProductModal").modal('hide');
				toastr.success('Producto Guardado Exitosamente');
				this.file = '';
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});

		},
		update(){
		    let formData = new FormData();
		    formData.append('file', this.file2);
			formData.append('data',JSON.stringify(this.element));

		    axios.put(this.url+'/'+this.element.id, this.element)
			.then( r => {
				this.getList();
				this.element = r.data;
				$("#editProductModal").modal('hide');
				toastr.success('Producto Actualizado Exitosamente');
				this.errors2 = [];
			})
			.catch( error =>{
				this.errors2 = error.response.data.errors
			});

		},
		handleFileUpload(){
			this.file = this.$refs.file.files[0];
      	},
		handleFileUpload2(){
			this.file2 = this.$refs.file2.files[0];
      	},
		editPhoto(){
			console.log(this.element);
			$('#editPortadaModal').modal();
		},
		showElement(id){
			axios.get(this.url+'/'+id+'/edit').then(response => {
				this.element = response.data;
				console.log(this.element);
				if (this.element.images) {
					this.element.portada = '/uploads/products/'+this.element.images.file;
				}

			});
		},
		deleteElement(){
			if (confirm("Esta Seguro que desea Eliminar!")) {
				axios.delete(this.url+'/'+this.element.id).then(response => {
					this.getList();
				});
			}
		}

	}
});
