var ReservationVue = new Vue({
	el: '#resarvation_from',
	data: {
		url:'/web',
		reservation: {},
		services:[],
	},
	methods: {
		getCategory(event){
			let id = event.target.value;
			axios.get(this.url+`/getService/${id}`).then(response => {
				this.services = response.data
			});
		},
		submit(){
		    let formData = new FormData();
			formData.append('reservation',JSON.stringify(this.reservation));

		    axios.post(this.url+`/reservationRequest`, formData
			).then( r => {

				this.reservation = {};
				$("#reservationModal").modal('hide');
				toastr.success('Pronto se Pondran en comunicacion contigo','Reservacion Exitosa');
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});
		}
	}
});

var WebVue = new Vue({
	el: '#webApp',
	data: {
		elementId:null,
		user:{},
		cartBadger:'',
		element: {},
		typeArray:['','Producto','Tratamiento','Promocion', 'GiftCard'],
		cartList:[],
		agendaServiceName:'',
		agendaStoreName:'',
		esteticistas:[],
		hours:[],
		cartTotal:'',
		tratamiento:{},
		esteticistaSelect:'',
		agenda:{},
		contact:{},
		itemId:''
	},
	created: function() {
		this.getCartCount();
	},
	methods: {
		getCartCount(){
			axios.get('/cartApp').then( r => {
				this.cartBadger = r.data;
			})
		},
		dateSelect(){
			if (!this.agenda.esteticistaSelect) {
				toastr.error('Debe Seleccionar Esteticista');
				return false;
			}
			let date = event.target.value;
			let token = document.head.querySelector('meta[name="csrf-token"]');
			window.axios.defaults.headers.common['X-CSRF-TOKEN'] = token.content;

			let data = {
				'tratamiento':this.tratamiento,
				'fecha':date,
				'este_id':this.agenda.esteticistaSelect
			}
			axios.post('getDoctorsByDate',data).then(r => {
				console.log(r.data);
				if (r.data == '0') {
					toastr.error('Dia Domingo no se Atiende');
				}
				this.hours = r.data;
			})
		},
		openAgenda(id,item_id){
			this.itemId = item_id;
			this.agenda = {};
			axios.get(`/services/${id}`).then( r => {
				this.tratamiento = r.data
				this.agendaServiceName = r.data.name
				this.agendaStoreName = r.data.store.name
				axios.get(`/users/estetic/${r.data.store.id}`).then( p =>{
					this.esteticistas = p.data
				})

				$('#agendarModal').modal();
			})
		},
		agendar(id){
			console.log(id);
			let data = {
				'sale_id':id,
				'agendar':this.agenda,
				'tratamiento':this.tratamiento
			}
			axios.post('/schedules/clientStore',data).then(r => {
				console.log(r.data);
				if (r.data) {
					$('#agendarModal').modal('hide');
					toastr.success('Tratamiento Agendado');
					$('#fechaAgendar'+this.itemId).text('Agendado para '+r.data.start_at)
				}

			})

		},
		addCartList(id,type){
			this.user = window.auth_user
			if (!this.user) {
				toastr.warning('Debes loguearte antes de Comprar');
				$('#userLogin').modal()
			}else{
				var data = {
					'id': id,
					'type': type,
					'qty': 1,
				}
				axios.post('/cartApp',data).then( r => {
					this.cartBadger = r.data;
					toastr.success(`${this.typeArray[type]} agregado`);
				})
			}

		},
		showCartList(){
			axios.get('/cartApp/create').then( r => {

				this.cartList = JSON.parse(r.data.list);
				this.cartTotal = r.data.total;
				$('#cartListModal').modal()
			})
		},
		submit(){
		    let formData = new FormData();
			formData.append('reservation',JSON.stringify(this.reservation));

		    axios.post(this.url+`/reservationRequest`, formData
			).then( r => {
				console.log(r);
				this.reservation = {};
				$("#reservationModal").modal('hide');
				toastr.success('Pronto se Pondran en comunicacion contigo','Reservacion Exitosa');
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});
		},
		contactMail(){
			let formData = new FormData();
			formData.append('reservation',JSON.stringify(this.contact));

			axios.post(`/web/contact`, formData
			).then( r => {
				this.contact = {};
				toastr.success('Pronto se Pondran en comunicacion contigo');
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});
		}
	}
});
