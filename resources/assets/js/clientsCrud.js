var ProductsVue = new Vue({
	el: '#mainClients',
	components: {
		   ckeditor: CKEditor.component
	},
	created: function() {
		this.getList();
	},
	data: {
		url:'/users',
		lists: [],
        editor: ClassicEditor,
        editorData: '<p>Descripcion.</p>',
        editorConfig: {},
		file: '',
		file2: '',
		data:{},
		element:{},
		errors:[],
		services:[],
		errors2:[]
	},
	methods: {
		getList: function(){
			axios.get(this.url).then(response => {
				this.lists = response.data.filter( d =>{
					if (d.type == 4) { return d	}
				});
				if (this.lists[0]) {
					this.showElement(this.lists[0].id);
				}

			});
		},
		evaluationModal(user_id){
			$('#evaluationModal input[name="user_id"]').val(user_id)
			$('#evaluationModal').modal();
		},
		getCategory(event){
			let id = event.target.value;
			axios.get(`/web/getService/${id}`).then(response => {
				this.services = response.data
			});
		},
		submitImage(){
			let formData = new FormData();
		    formData.append('file', this.file2);
			formData.append('type', 4);
			if (this.element.images[0]) {
				formData.append('id', this.element.images[0].id);
			}else{
				formData.append('foreign_id', this.element.id);
			}

			axios.post('/imagesApp', formData, { headers: {'Content-Type': 'multipart/form-data'} }
		    ).then( r => {
				this.getList();
				$('#editPortadaModal').modal('hide');
				this.file2 = '';
		    })
		    .catch( error =>{
				console.log(error);
		    });
		},
		submit(){
		   	this.data.store_id = $('input[name="store_id"]').val();
			this.data.type = 4;
			let formData = new FormData();
		    formData.append('file', this.file);
			formData.append('data',JSON.stringify(this.data));

		    axios.post(this.url, formData, { headers: {'Content-Type': 'multipart/form-data'} }
			).then( r => {
				this.getList();
				this.data = {};
				$("#createModal").modal('hide');
				toastr.success('Cliente Guardado Exitosamente');
				this.file = '';
				this.errors = [];
				location.reload()
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});

		},
		update(){
		    let formData = new FormData();
		    formData.append('file', this.file2);
			formData.append('data',JSON.stringify(this.element));

		    axios.put(this.url+'/'+this.element.id, this.element)
			.then( r => {
				this.getList();
				this.element = r.data;
				$("#editModal").modal('hide');
				toastr.success('Cliente Actualizado Exitosamente');
				this.errors2 = [];
				location.reload()
			})
			.catch( error =>{
				this.errors2 = error.response.data.errors
			});

		},
		handleFileUpload(){
			this.file = this.$refs.file.files[0];
      	},
		handleFileUpload2(){
			this.file2 = this.$refs.file2.files[0];
      	},
		editPhoto(){
			$('#editPortadaModal').modal();
		},
		showElement(id){
			axios.get(this.url+'/'+id+'/edit').then(response => {
				this.element = response.data;
				if (this.element.images[0]) {
					this.element.portada = '/uploads/users/'+this.element.images[0].file;
				}

			});
		},
		deleteElement(){
			if (confirm("Esta Seguro que desea Eliminar!")) {
				axios.delete(this.url+'/'+this.element.id).then(response => {
					location.reload()
				});
			}
		}

	}
});
