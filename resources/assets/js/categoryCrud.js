var ProductsVue = new Vue({
	el: '#mainCategory',
	components: {
		   ckeditor: CKEditor.component
	},
	created: function() {
		this.getList();
		this.box.store_id = 0;
	},
	data: {
		url:'/categories',
		lists: [],
		subcategories: [],
		subcat:{},
		boxes:[],
        editor: ClassicEditor,
        editorData: '<p>Descripcion.</p>',
        editorConfig: {},
		data:{},
		file: '',
		file2: '',
		box:{},
		typeArray:['','Productos','Servicios'],
		element:{},
		categoryLabel:'',
		errors:[],
		errors2:[],
		catLabel:''
	},
	methods: {
		getList: function(){
			axios.get(this.url).then(response => {
				this.lists = response.data.filter( d =>{
					d.typeName =  this.typeArray[d.type]
					return d
				})
			});
		},
		editPhoto(l){
			this.element = l;
			$('#editPortadaModal').modal();
		},
		subCategory(l)
		{
			this.categoryLabel = l.name;
			this.subcat.category_id = l.id;
			axios.get(this.url+'/getSubCategories/'+l.id).then(response => {
				this.subcategories = response.data
				$('#subModal').modal();
			});
		},
		addSub(){
			let formData = new FormData();
		   	formData.append('data', JSON.stringify(this.subcat) );

			axios.post(this.url+'/addSub',formData).then( r => {
				this.subcategories = r.data;
				this.subcat.name = '';
				toastr.success('Subcategoria Agregado Exitosamente');
			})
			.catch( error =>{
				console.log(error);
			});
		},
		deleteSub(id, category){
			axios.get(this.url+'/deleteSub/'+ id +'/'+category).then( r => {
				this.subcategories = r.data;
				this.subcat.name = '';
				toastr.success('SubCategoria Borrado');
			})
			.catch( error =>{
				console.log(error);
			});
		},
		submitImage(){
			//revisar
			let formData = new FormData();
		    formData.append('file', this.file2);

			axios.post('/categoryImgApp/'+this.element.id, formData, { headers: {'Content-Type': 'multipart/form-data'} }
		   ).then( r => {
			   this.getList();
			   $('#editPortadaModal').modal('hide');
			   this.file2 = '';
		   })
		   .catch( error =>{
			   console.log(error);
		   });
		},
		submit(){
		    let formData = new FormData();
			formData.append('file', this.file);
			formData.append('data',JSON.stringify(this.data));

		    axios.post(this.url, formData, { headers: {'Content-Type': 'multipart/form-data'} }
			).then( r => {
				this.getList();
				this.data = {};
				$("#createModal").modal('hide');
				toastr.success('Category Guardado Exitosamente');
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});

		},
		update(){
		    let formData = new FormData();
			formData.append('file', this.file2);
		    formData.append('data',JSON.stringify(this.element));

		    axios.put(this.url+'/'+this.element.id, this.element)
			.then( r => {
				this.getList();
				this.element = r.data;
				$("#editModal").modal('hide');
				toastr.success('Categoria Actualizado Exitosamente');
				this.errors2 = [];
			})
			.catch( error =>{
				this.errors2 = error.response.data.errors
			});

		},
		handleFileUpload(){
			this.file = this.$refs.file.files[0];
      	},
		handleFileUpload2(){
			this.file2 = this.$refs.file2.files[0];
      	},
		editElement(d){
			this.element = d;
			$("#editModal").modal();
		},
		showBox(d){
			this.catLabel = d.name
			this.box.category_id = d.id;
			this.boxes = d.boxes;
			$('#boxModal').modal()

		},
		addBox(){
			let formData = new FormData();
		   	formData.append('data', JSON.stringify(this.box) );

			axios.post(this.url+'/addBox',formData).then( r => {
				this.boxes = r.data;
				toastr.success('Box Agregado Exitosamente');
			})
			.catch( error =>{
				console.log(error);
			});
		},
		deleteBox(id,category){
			axios.get(this.url+'/deleteBox/'+ id +'/'+ category ).then( r => {
				this.boxes = r.data;
				toastr.success('Box Borrado');
			})
			.catch( error =>{
				console.log(error);
			});
		},
		deleteElement(id){
			if (confirm("Esta Seguro que desea Eliminar!")) {
				axios.delete(this.url+'/'+id).then(response => {
					this.getList();
				});
			}
		}

	}
});
