var SaleVue = new Vue({
	el: '#salesContent',

	data: {
		url:'/categories',
		lists: [],
		ventas:[],
        services:[],
		data:{},
		element:{},
		tratatime:0,
		endTime:0,
		tratamiento:{},
		errors:[],
		errors2:[],
		type:0
	},
	methods: {
		agendar:function(saleitem_id)
		{
			axios.get('/saleitem/'+saleitem_id).then( response => {
				let e = response.data;
				this.tratatime = e.time;
				console.log(e);
				$("#agendarModal input[name='user_id']").val(e.sale.user_id);
				$("#agendarModal input[name='saleitem_id']").val(saleitem_id);
				$('#agendarModal').modal()
			})
		},
		seguimiento:function(sale_id)
		{
			console.log('asdad');
			axios.get('/salesItems/'+sale_id).then( response => {
				this.element = response.data;
				console.log(this.element);
				$('#seguimientoTable').html(response.data)
				$('#seguimientoModal').modal()
			})
		},

		setTime(event){
			this.endTime = moment(event.target.value, 'HH:mm').add(this.tratatime, 'm').format("HH:mm");
		},


	}
});
