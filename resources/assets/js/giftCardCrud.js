var ProductsVue = new Vue({
	el: '#mainGiftcard',
	components: {
		   ckeditor: CKEditor.component
	},
	created: function() {
		this.getList();
	},
	data: {
		url:'/giftcardsApp',
		lists: [],
        editor: ClassicEditor,
        editorData: '<p>Descripcion.</p>',
        editorConfig: {},
		data:{},
		typeArray:['','Productos','Servicios'],
		element:{},
		errors:[],
		errors2:[],
	},
	methods: {
		getList: function(){
			axios.get(this.url).then(response => {
				this.lists = response.data.filter( d =>{
					d.typeName =  this.typeArray[d.type]
					if (d.service_id) {
						axios.get('/servicios/'+d.service_id).then( s => {
							d.service_id = s.data.name
						})
					}
					return d
				})
			});
		},
		submit(){
			this.data.store_id = $('input[name="store_id"]').val();
		    let formData = new FormData();
			formData.append('data',JSON.stringify(this.data));

		    axios.post(this.url, formData
			).then( r => {
				this.getList();
				this.data = {};
				$("#createModal").modal('hide');
				toastr.success('GiftCard Guardado Exitosamente');
				this.errors = [];
			})
			.catch( error =>{
				this.errors = error.response.data.errors
			});

		},
		update(){
		    let formData = new FormData();
		    formData.append('data',JSON.stringify(this.element));

		    axios.put(this.url+'/'+this.element.id, this.element)
			.then( r => {
				this.getList();
				this.element = r.data;
				$("#editModal").modal('hide');
				toastr.success('GiftCard Actualizado Exitosamente');
				this.errors2 = [];
			})
			.catch( error =>{
				this.errors2 = error.response.data.errors
			});

		},
		editElement(d){
			this.element = d;
			$("#editModal").modal();
		},
		deleteElement(id){
			if (confirm("Esta Seguro que desea Eliminar!")) {
				axios.delete(this.url+'/'+id).then(response => {
					this.getList();
				});
			}
		}

	}
});
