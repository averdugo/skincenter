var ScheduleVue = new Vue({
	el: '#scheduleContent',
	created: function() {
		this.getList();
	},
	data: {
		url:'/categories',
		lists: [],
		ventas:[],
        services:[],
		data:{},
		element:{},
		typeArray:['','Productos','Servicios'],
		tratatime:0,
		endTime:0,
		tratamiento:{},
		errors:[],
		errors2:[],
		type:0
	},
	methods: {
		getList: function(){
			axios.get(this.url).then(response => {
				this.lists = response.data.filter( d =>{
					d.typeName =  this.typeArray[d.type]
					return d
				})
			});
		},
		getSaleUser(user_id){
			axios.get('/getSalesbyUser/'+user_id).then( response => {
				this.ventas = response.data
			})
		},
		setTratamiento(event){
			if (event.target.options.selectedIndex > -1) {
	            let theTarget = event.target.options[event.target.options.selectedIndex].dataset;
	            this.tratatime = theTarget.time
	        }
		},
		typeSelect(event){
			this.type = event.target.value;
		},
		setTime(time){
			if(this.type != 2)
				this.endTime = moment(time, 'HH:mm').add(this.tratatime, 'm').format("HH:mm");
			else
				this.endTime = moment(time, 'HH:mm').add(10, 'm').format("HH:mm");
		},
		getCategory(event){
			let id = event.target.value;
			axios.get(`/web/getService2/${id}`).then(response => {
				this.services = response.data
				console.log(this.services);
			});
		}

	}
});
