require('./bootstrap');

window.Vue = require('vue');

window.VueRouter = require('vue-router').default;

window.VueAxios = require('vue-axios').default;

window.Axios = require('axios').default;

const AppLayout = Vue.component('AppLayout',require('./components/App.vue'));
const ProductLayout = Vue.component('ProductLayout',require('./components/Productos.vue'));

//import routes from './routes/routes'

const routes =[
	{
		name:"dashboard",
		path: '/administrador/',
		component: AppLayout
	},
	{
		name:"productos",
		path: '/administrador/productos',
		component: ProductLayout
	}
]


const router = new VueRouter({mode:'history',routes:routes})
Vue.use(VueRouter, VueAxios, Axios);
new Vue({
		router
}



).$mount('#app');
