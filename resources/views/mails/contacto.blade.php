<h3>{{$asunto}}</h3>
<table style="text-align: left;text-transform: uppercase;">
	<tr>
		<th>Nombre</th>
		<th style="font-weight:100">{{ $name }}</th>
	</tr>
	<tr>
		<th>Email</th>
		<th style="font-weight:100">{{ $email }}</th>
	</tr>
	<tr>
		<th>Mensaje</th>
		<th style="font-weight:100">{{ $mensaje }}</th>
	</tr>
</table>
