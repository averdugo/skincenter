<h3>Solicitud {{$category}} <br>Tratamiento: {{$service}} <br>Fecha: {{$date}} {{$time}}</h3>
<table style="text-align: left;text-transform: uppercase;">
	<tr>
		<th>Cliente</th>
		<th style="font-weight:100">{{ $name }}</th>
	</tr>
	<tr>
		<th>Rut</th>
		<th style="font-weight:100">{{ $rut }}</th>
	</tr>
	<tr>
		<th>Correo</th>
		<th style="font-weight:100">{{ $correo }}</th>
	</tr>
	<tr>
		<th>Telefono</th>
		<th style="font-weight:100">{{ $phone }}</th>
	</tr>

</table>
