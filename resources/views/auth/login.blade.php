<!DOCTYPE html>
<html lang="{{app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
	    <title>SkinCenter-Admin</title>
	    <link href="/admin/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	    <link href="/admin/css/style.css" rel="stylesheet">
	    <link href="/admin/css/pages/dashboard1.css" rel="stylesheet">
	    <link href="/admin/css/colors/default.css" id="theme" rel="stylesheet">
	</head>
	<body class="fix-header fix-sidebar card-no-border">

		<div id="main-wrapper">

				<div class="container-fluid">
					<div class="row justify-content-center">
					    <div class="col-md-8">
					        <div class="card">
					            <div class="card-header text-center">
									<img src="/images/logo_skincenter.png" class="img-fluid pull-xs-left" alt="..." style="max-width:400px;margin:40px auto">
								</div>

					            <div class="card-body">
					                <form class="form-horizontal form-material" method="POST" action="{{ route('login') }}">
					                    @csrf

					                    <div class="form-group ">
					                        <label for="email" class="col-md-12">{{ __('E-Mail
												') }}</label>

					                        <div class="col-md-12">
					                            <input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-line" name="email" value="{{ old('email') }}" required autofocus>

					                            @if ($errors->has('email'))
					                                <span class="invalid-feedback" role="alert">
					                                    <strong>{{ $errors->first('email') }}</strong>
					                                </span>
					                            @endif
					                        </div>
					                    </div>

					                    <div class="form-group ">
					                        <label for="password" class="col-md-12 ">{{ __('Password') }}</label>

					                        <div class="col-md-12">
					                            <input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-line" name="password" required>

					                            @if ($errors->has('password'))
					                                <span class="invalid-feedback" role="alert">
					                                    <strong>{{ $errors->first('password') }}</strong>
					                                </span>
					                            @endif
					                        </div>
					                    </div>

					                    <div class="form-group row">
					                        <div class="col-md-6 offset-md-4">
					                            <div class="form-check">
					                                <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>

					                                <label class="form-check-label" for="remember">
					                                    {{ __('Recuerdame') }}
					                                </label>
					                            </div>
					                        </div>
					                    </div>

					                    <div class="form-group row mb-0">
					                        <div class="col-md-8 offset-md-4">
					                            <button type="submit" class="btn btn-primary">
					                                {{ __('Login') }}
					                            </button>

					                            <a class="btn btn-link" href="{{ route('password.request') }}">
					                                {{ __('Olvidaste tu contraseña?') }}
					                            </a>
					                        </div>
					                    </div>
					                </form>
					            </div>
					        </div>
					    </div>
					</div>
				</div>


		</div>
		
	</body>
</html>
