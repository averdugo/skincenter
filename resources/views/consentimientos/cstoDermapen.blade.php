<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>CONSENTIMIENTO INFORMADO PARA TRATAMIENTO MEDIANTE DERMAPEN</title>
</head>
<body>
	<div class="container">
		<h2 align="center"> Consentimiento para tratamiento con Ultracavitación </h2>

        <p>El tratamiento consiste en realizar múltiples pequeñas punciones en la piel que alcanzan la dermis estimulando la respuesta inmune, la contracción y la creación de nuevo colágeno, normalizando diversas funciones de la piel. El objetivo de la técnica es  tratar signos de envejecimiento, arrugas, cicatrices, daño solar, manchas entre otros.  Comprendo que:</p>
        <p>•	Para obtener  mejores resultados, puede ser conveniente la repetición del tratamiento y que los tratamientos combinados suelen tener mayor efectividad.</p>
        <p>•	A pesar de la adecuada elección de la técnica y de su correcta realización pueden presentarse efectos secundarios, como ardor, escozor, escarchado, descamación, hipopigmentaciones, hiperpigmentación y costras en la zona tratada.</p>
        <p>•	El profesional me ha advertido, prohibiéndome expresamente, que no debo exponerme al sol después de cada sesión, así como el uso obligatorio de bloqueador solar FPS50  (3 veces) durante al menos un mes después del tratamiento.</p>
        <p>•	No debo realizar ejercicio físico ni nada que pueda generarme sudor en la zona tratada. Además debo asegurar la correcta hidratación de la piel posterior a la sesión, así también respetar los tiempos entre cada sesión.</p>
        <p>•	Es importante conocer mis antecedentes personales de posibles alergias a medicamentos y/o alimentarias, medicaciones actuales como anticoagulantes (heparina), antecedentes de herpes simple facial, antecedentes personales o familiares de queloides, aplicación reciente de botox y/o ácido hialurónico,  diabetes no controlada, embarazo o lactancia materna, quemaduras solares, acné activo, cáncer de piel, rosácea.</p>
        <p>•	 Me comprometo a no aplicarme maquillaje (incluyendo FPS con color)12 horas post tratamiento.</p>
        <p>•	 Autorizo la toma de una fotografía pre y post tratamiento.</p>
        
		<p>Confirmo que la información que he facilitado es verdadera y correcta y que he leído, comprendido y aceptado la información mencionada más arriba.</p>
		<p><b>He comprendido las explicaciones que se me han facilitado en un lenguaje claro y sencillo y que el Profesional que me ha atendido me ha permitido realizar todas las observaciones y me ha aclarado todas las dudas que le he planteado.</b></p>
		<h4 align="left"> Nombre  <h4 align="center"> RUT</h4></h4>		
		<h4 align="left"> Firma <h4 align="center"> Fecha</h4></h4>


	</div>
	
</body>
</html>