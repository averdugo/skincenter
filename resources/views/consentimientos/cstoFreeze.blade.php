<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>CONSENTIMIENTO PARA EL TRATAMIENTO CON RADIOFRECUENCIA VENUS FREEZE</title>
</head>
<body>
	<div class="container">
		<h2 align="center">CONSENTIMIENTO PARA EL TRATAMIENTO CON RADIOFRECUENCIA VENUS FREEZE</h2>

        <p>El equipo de radiofrecuencia Venus Freeze combina se basa en la combinación de radiofrecuencia multipolar con pulso magnético. Los pulsos electromagnéticos inducen la formación de pequeños vasos sanguíneos, lo que mejora la circulación y la radiofrecuencia, por su parte, estimula la producción de nuevas fibras de colágeno y elastina logrando un efecto tensor de la piel y mejorando notablemente el contorno corporal.  Comprendo que:</p>
        <p>•	Los resultados del tratamiento no se me han ofrecido como cambios permanentes y que son diferentes de una persona a otra.</p>
        <p>•	 Debo complementar el tratamiento para lograr un efecto más duradero, ya sea con cuidado personal u otros procedimientos que he tenido la oportunidad de conocer en la evaluación. </p>
        <p>•	Se tomarán fotografías clínicas y medidas antropométricas del área antes y después del tratamiento, las cuales serán de uso personal a modo de evaluar el resultado.</p>
        <p>•	Debo cumplir los plazos previamente establecidos entre sesiones, para asegurar el éxito de la radiofrecuencia.</p>
        <p>•	En caso de sospecha de embarazo o embarazo comprobado, el tratamiento no será realizado. Y retomare el tratamiento después del periodo de lactancia materna.</p>
        <p>•	En caso de reacción viral como herpes, no puedo realizar la sesión sin antes recibir tratamiento médico.</p>
        <p>•	Puedo presentar enrojecimiento de la piel tratada por algunas horas por el calor, y en casos excepcionales podrían generarse hematomas.</p>
        <p>•	Debo informar mi historial médico, pues es imposible realizar la sesión en caso de tener:</p>
        <p>1.	Implantes y otros metales (prótesis metálicas) ya que no son compatibles con la energía emitida.</p>
        <p>2.	Enfermedades como: queratosis actínica, cáncer, melanomas, marcapasos, problemas a la tiroides, epilepsia, fiebre, infección en el área de tratamiento, esclerosis múltiple, enfermedad mental, enfermedades autoinmunes específicas de la piel, enfermedades desmienilizantes.</p>
        <p>3.	Uso de anticoagulantes.</p>
        <p>4.	Trastornos vasculares.</p>
        <p>5.	Relleno sintético en la zona a tratar.</p>
		<p><b>•	Si por cualquier motivo no puedo asistir, debo avisar con 24 hrs de anticipación, de no ser así, la sesión se toma como realizada.</b></p>
        <p>Confirmo que la información que he facilitado es verdadera y correcta, que he leído, comprendido y aceptado la información mencionada en este consentimiento. He recibido las indicaciones Pre y Post Tratamiento, entiendo los beneficios del tratamiento, pero también los riesgos. Durante la sesión previa de evaluación he tenido la oportunidad de aclarar dudas y hacer todas las preguntas pertinentes. </p>
        <p>Por lo tanto, libremente consiento someterme al tratamiento de Radiofrecuencia Venus Freeze.</p>
		<h4 align="left"> Nombre  <h4 align="center"> RUT</h4></h4>		
		<h4 align="left"> Firma <h4 align="center"> Fecha</h4></h4>


	</div>
	
</body>
</html>