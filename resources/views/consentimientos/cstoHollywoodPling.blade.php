<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>CONSENTIMIENTO INFORMADO PARA TRATAMIENTO DE HOLLYWOOD PEELING</title>
</head>
<body>
	<div class="container">
		<h2 align="center"> CONSENTIMIENTO INFORMADO PARA TRATAMIENTO DE HOLLYWOOD PEELING </h2>

        <p>El Hollywood Peel es tratamiento cosmético que consiste en aplicar una loción de carbón en la piel por cerca de 10 a 15 minutos. Las micro partículas de carbón son absorbidas por la capa superficial de la piel. Cuando el láser se pasa por primera vez sobre la piel, el carbón absorbe un poco de calor lo que estimula la generación de colágeno y elastina, lo que produce un efecto de rejuvenecimiento facial leve. Los próximos pasos causan vaporización del carbón, con la eliminación de una capa superficial de la piel, es decir, produce una exfoliación mecánica inducida por láser. Finalmente se pasa el láser por la piel limpia, con más energía, lo que matiza la piel y genera el cierre de poros. El objetivo de la técnica es tratar signos de envejecimiento, mejorar tono general de la piel, daño solar, manchas epidérmicas, entre otros.   Comprendo que:</p>
        <p>•	Los resultados del tratamiento no se me han ofrecido como cambios permanentes y que son diferentes de una persona a otra.</p>
        <p>•	Debo complementar el tratamiento para lograr un efecto más duradero, ya sea con cuidado personal u otros procedimientos que he tenido la oportunidad de conocer en la evaluación. </p>
        <p>•	Debo cumplir los plazos previamente establecidos entre sesiones, para asegurar el éxito del tratamiento.</p>
        <p>•	Se tomarán fotografías clínicas del área antes y después del tratamiento, las cuales serán de uso personal a modo de evaluar el resultado.</p> 
        <p>•	A pesar de la adecuada elección de la técnica y de su correcta realización pueden presentarse efectos secundarios, como ardor, escozor, quemaduras, hipopigmentación o hiperpigmentación, eritema transitorio, edema peri folicular, prurito, hipersensibilidad, urticaria, reacción alérgica post sesión.</p>
        <p>•	En caso de sospecha de Embarazo o embarazo comprobado, el tratamiento no será realizado y retomaré el tratamiento sólo con orden  ginecológica autorizando,  posterior a 3 meses de gestación. </p>
        <p>•	En caso de reacción viral como herpes, no puede realizarse la sesión de láser sin antes recibir tratamiento médico.</p>
        <p>•	Es imposible realizar el tratamiento en caso de personas con marcapasos o desfibrilador interno, implante metálico, cáncer, enfermedades autoinmunes, epilepsia, relleno sintético en zona a tratar.</p>
        <p>•	Es importante dar a conocer mis antecedentes personales de posibles alergias a medicamentos y/o alimentarias, antecedentes de herpes simple facial, antecedentes personales o familiares de queloides, diabetes no controlada, embarazo o lactancia materna, quemaduras solares, acné activo, cáncer de piel.</p>
        <p>•	Antes de realizar la sesión se debe informar el uso de cremas despigmentantes y medicamentos fotosensibles (antibióticos, antivirales, antimicóticos, vitaminas C-D-E) y otras sustancias como la ingesta de betacaroteno. En el caso de que la paciente este en tratamiento con los medicamentos antes mencionados la sesión debe suspenderse, y retomar luego de 7 días post tratamiento.</p>


		<p>Confirmo que la información que he facilitado es verdadera y correcta, que he leído, comprendido y aceptado la información mencionada en este consentimiento.</p>
		<p>He recibido las indicaciones Pre y Post Tratamiento, entiendo los beneficios del tratamiento, pero también los riesgos. Durante la sesión previa de evaluación he tenido la oportunidad de aclarar dudas y hacer todas las preguntas pertinentes.  Por lo tanto, libremente consiento someterme al Tratamiento de Cavitación.</p>
		<p><b>Si por cualquier motivo no puedo asistir, debo avisar con 24 hrs de anticipación, de no ser así, la sesión se toma como realizada.</b></p>
		<h4 align="left"> Nombre  <h4 align="center"> RUT</h4></h4>		
		<h4 align="left"> Firma <h4 align="center"> Fecha</h4></h4>


	</div>
	
</body>
</html>