<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title> Consentimiento para el Tratamiento de Depilación Láser</title>
</head>
<body>
	<div class="container">
		<h2 align="center">  Consentimiento para el Tratamiento de Depilación Láser </h2>

		<p>El tratamiento en la mayoría de los pacientes ofrece entre un 70-90% de efectividad. Comprendo que el procedimiento no me es ofrecido “como depilación definitiva”, y que los resultados son diferentes entre una persona y otra. Estoy en   pleno conocimiento que debo cumplir los plazos previamente establecidos entre sesiones, para asegurar el éxito de la depilación.  Comprendo que:</p>
        <p>•	En caso de sospecha de Embarazo o embarazo comprobado, el tratamiento no será realizado y retomaré el tratamiento sólo con orden  ginecológica autorizando,  posterior a 3 meses de gestación.</p>
        <p>•	En caso de reacción viral como herpes, no puede realizarse la sesión de láser sin antes recibir tratamiento médico.</p>
        <p>•	La luz del láser puede provocar daño ocular, por lo que respetaré la norma de usar lentes de protección durante la sesión.</p>
        <p>•	El láser no toma vellos claros ni canas, ya que no poseen melanina.</p>
        <p>•	Antes de realizar la sesión se debe informar el uso de cremas despigmentantes y medicamentos fotosensibles (antibióticos, antivirales, antimicóticos, vitaminas C-D-E) y  otras sustancias como la ingesta de betacaroteno. En el caso de que la paciente esté en tratamiento con los medicamentos antes mencionados la sesión debe suspenderse, y retomar luego de 7 días post tratamiento.</p>
        <p>•	Debo asistir previamente rasurada a cada sesión.</p>
        <p>•	Debo asistir a la sesión sin desodorantes, crema, maquillaje ni perfume en la zona a tratar y evitar el uso de estas 3 horas post sesión.</p>
        <p>•	No debo exponerme al sol por lo menos 3 semanas previas al tratamiento, ni  venir bronceada por ningún método ya sea sol, solárium, autobronceantes o bronceados express. En este caso la sesión deberá ser suspendida hasta que la coloración de la piel vuelva a su tono natural.</p>
        <p>•	Debo evitar la exposición al sol a lo menos una  semana posterior a la sesión de depilación.</p>
        <p>•	Es obligatorio el uso de bloqueador solar FPS50 diaria (3 veces al menos) durante el período de tratamiento.</p>
        <p>•	Una vez realizado el tratamiento se prohíbe el uso del algún sistema que retire el vello de raíz (cera depilatoria, pinza, maquina eléctrica), solo se podrá utilizar crema depilatoria o rasuradora.</p>
        <p>•	Es imposible realizar el tratamiento en caso de personas con marcapasos o desfibrilador interno, implante metálico, cáncer, enfermedades autoinmunes, epilepsia, relleno sintético en zona a tratar.</p>
        <p>•	No debo realizar ejercicio físico ni nada que pueda generarme sudor en la zona tratada incluso 48 horas post sesión.</p>
        <p>•	Es posible que en casos excepcionales, si no se cumple con las indicaciones antes mencionadas, se puedan presentar quemaduras, hipopigmentación o hiperpigmentación.</p>
        <p>•	Es posible que algunos pacientes puedan presentar algunas reacciones post tratamiento tales como: Eritema transitorio, inflamación,  edema peri folicular, prurito, hipersensibilidad, urticaria, reacción alérgica.</p>
        <p>•	En casos excepcionales se podría generar el síndrome de hipertricosis paradojal.</p>
        <p>•	El tratamiento en personas con alteraciones hormonales es más prolongado, deberá realizar más de 6 sesiones.</p>
        <p>Confirmo que la información que he facilitado es verdadera y correcta, que he leído, comprendido y aceptado la información mencionada en este consentimiento. He recibido las indicaciones Pre y Post Tratamiento, entiendo los beneficios del tratamiento, pero también los riesgos. Durante la sesión previa de evaluación he tenido la oportunidad de aclarar dudas y hacer todas las preguntas pertinentes. </p>
		<p><b>Por lo tanto, libremente consiento someterme al Tratamiento del Laser.</b></p>
		<h4 align="left"> Nombre  <h4 align="center"> RUT</h4></h4>		
		<h4 align="left"> Firma <h4 align="center"> Fecha</h4></h4>


	</div>
	
</body>
</html>