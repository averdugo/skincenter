<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="X-UA-Compatible" content="ie=edge">
	<title>Consentimiento para tratamiento con Ultracavitación</title>
</head>
<body>
	<div class="container">
		<h2 align="center"> Consentimiento para tratamiento con Ultracavitación </h2>

		<p>La Ultracavitación es un tratamiento no invasivo, destinado para fines estéticos en tratamientos de reducción de grasas localizadas y  celulitis. Consiste en la utilización de ultrasonido para romper los adipocitos (células grasas). Esta acción consigue que los ácidos grasos se liberen en la sangre, recorriendo el cuerpo a través del sistema circulatorio. Dichas sustancias finalmente son eliminadas del organismo a través de la orina y el sistema linfático.  Comprendo que:</p>
		<p>•	Este tipo de tratamiento está contraindicado en personas que cursen obesidad generalizada, epilepsia, hígado graso, trastornos lipídicos, como elevaciones de triglicéridos ó colesterol, enfermedad renal, enfermedad hepática,  embarazo y lactancia.</p>
		<p>•	Debo mantener una buena hidratación consumiendo 2lt de agua diaria y mantener una dieta equilibrada para lograr un tratamiento efectivo.</p>
		<p>•	Estoy en pleno conocimiento  que debo cumplir con los plazos previamente establecidos entre sesiones para asegurar el éxito del tratamiento y que los resultados se verán afectados si no sigo el tratamiento de forma continua.</p>
		<p>•	Los resultados pueden ser diferentes entre una persona u otra y que no se puede asegurar un cambio exacto en medidas antropométricas.</p>
		<p>•	Estoy de acuerdo a que se me realicen mediciones antropométricas así como fotografías en la zona a tratar, las cuales serán de uso personal a modo de evaluar resultados.</p>
		<p>•	Debo informar en caso de implantes u otros metales (prótesis metálicas), ya que no son compatibles con la energía emitida. </p>
		<p>•	Entiendo que enfermedades tales como: queratosis actínica, cáncer, melanomas, marcapasos, problemas de tiroides, epilepsia, infección en el área a tratar, esclerosis múltiple, enfermedad mental, enfermedades autoinmunes específicas de la piel, enfermedades desmielinizantes, uso de anticoagulantes y trastornos vasculares están totalmente contraindicados con el tratamiento.</p>
		<p>Confirmo que la información que he facilitado es verdadera y correcta, que he leído, comprendido y aceptado la información mencionada en este consentimiento.</p>
		<p>He recibido las indicaciones Pre y Post Tratamiento, entiendo los beneficios del tratamiento, pero también los riesgos. Durante la sesión previa de evaluación he tenido la oportunidad de aclarar dudas y hacer todas las preguntas pertinentes.  Por lo tanto, libremente consiento someterme al Tratamiento de Cavitación.</p>
		<p><b>Si por cualquier motivo no puedo asistir, debo avisar con 24 hrs de anticipación, de no ser así, la sesión se toma como realizada.</b></p>
		<h4 align="left"> Nombre  <h4 align="center"> RUT</h4></h4>		
		<h4 align="left"> Firma <h4 align="center"> Fecha</h4></h4>


	</div>
	
</body>
</html>