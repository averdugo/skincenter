@extends('web.layout')
@section('css')
	<link rel="stylesheet" type="text/css" href="/styles/about.css">
	<link rel="stylesheet" type="text/css" href="/styles/about_responsive.css">
	<style>
		.imgSalient {
			max-height: 250px;
			width: auto;
		}
	</style>
@endsection
@section('content')
<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="bannar_area">
					<br>
					<h2>Producto</h2>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
<br>
<br>

<div class="container mt-5">
  	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="header-content-left">
				<img src="/uploads/products/{{$product->images->file}}" style="width: 95%;">
			</div>  
		</div>
		<div class="col-md-6 col-sm-6">
		  	<div class="header-content-right">
				<h1 class="display-4">{{$product->name}}</h1>
			  	<h4 class="mt-3">{!!$product->description!!}</h4>
				<br>
				<h3 class="mt-5">$ {{number_format( $product->price , 0 , ',' , '.' )}}</h3>
				<h5 class="mt-5">En stock</h5>
				<br>
				<br>
				<br>
				<p class="mt-5">{{$product->short_description}}</p>
				<br>
				<br>
				<a class="btn_df btn_dafault" href="#" data-toggle="modal" v-on:click="addCartList({{$product->id}},1)">Agregar al Carrito</a>
			  </div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
	<div id="myCarousel" class="carousel slide">
		<div class="carousel-inner">
			<div class="item active">
				<h1 class="h1">Productos Destacados</h1>
				<br>
				<br>
				<br>
				<br>
				<div class="row">
					@foreach ($products as $p)
						<div class="col-md-3">
							<div class="thumbnail">
								<a href="/productos/{{$p->id}}/details">
									<img src="/uploads/products/{{$p->images->file ?? ''}}" class="imgSalient">
								</a>
								<div class="caption">
									<h3>{{$p->name}}</h3>
									<span class="price">$ {{number_format( $p->price , 0 , ',' , '.' )}}</span>
									<div class="filters text-center">
										<ul class="filter-tabs filter-btns clearfix anim-3-all">
										</ul>
									</div>
								</div>
							</div>        
						</div>
					@endforeach
				</div>
			</div>
		</div>
	</div>
</div>
<br>
<br>

@endsection
@section('scripts')
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>

@endsection
