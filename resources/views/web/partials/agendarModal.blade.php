<div class="modal fade" id="agendarModal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="container">
			<div class="row" style="text-align:center">
				<div class="modal-content" style="text-align:left">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="section_title wow animated slideInUp">
								<h2>Agendar @{{agendaServiceName}}</h2>
								<small>@{{agendaStoreName}}</small>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12" style="margin-bottom:40px;">
							<table class="table">
								<thead>
									<tr>

										<th>Esteticista</th>
										<th style="width:30%;">Dia</th>
										<th>Hora Disponibles</th>
									</tr>
								</thead>
							  	<tbody>
								  	<tr>
										<td>
											<div class="form-group">
												<select class="form-control" name="" v-model="agenda.esteticistaSelect">
													<option value="">Seleccione</option>
													<option v-for="(item) in esteticistas" :value="item.id">@{{item.name}}</option>
												</select>
											</div>
										</td>
										<td>
											<div class="form-group">
												<input class="form-control" type="date" name="" v-model="agenda.date" @change="dateSelect()" style="line-height:20px;padding:0;    padding-left: 5px;">
											</div>
		  						      	</td>
		  						      	<td>
											<div class="form-group">
												<select class="form-control" name="" v-model="agenda.hour">
													<option v-for="(item) in hours" :value="item">@{{item}}:00</option>
												</select>
											</div>
		  						      	</td>
		  						    </tr>
							  	</tbody>
						    </table>
						</div>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn_df btn_dafault" @click="agendar(2)">Agendar</a>
			      </div>
				</div>
			</div>
		</div>
	</div>
</div>
