
<div id="header" class="menubar">
	<div class="menubar-content">
		<nav class="navbar navbar-default" style="">
			<div class="container">
				<div class="row">
					<div class="col-md-2 col-sm-2">
						<div class="site-title">
							<a class="navbar-brand call-section" href="#page-top">
								<img class="nav-logo" src="/images/letragruesa.png" alt="logo" style="margin-top:8px;width:250px">
							</a>
						</div><!-- end site-title -->
					</div><!-- end col-md-4 -->
					<div class="navbar-header">
						<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
							<span class="sr-only">Toggle navigation</span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button><!-- end button -->
					</div><!-- end navbar-header -->
					<div class="col-md-10 col-sm-10 navbar-style">
						<div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
						  <ul class="nav navbar-nav navbar-right">
							<li><a href="/#page_top" data-scroll="page_top">Inicio</a></li>

							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle user-dropdown" data-scroll="services" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									Servicios

								</a>
								<div class="dropdown-menu">
									@foreach ($categories as $key => $c)
										<a class="dropdown-item" href="/tratamiento/{{$c->id}}">{{$c->name}}</a>
									@endforeach


								</div>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle user-dropdown" data-scroll="products" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									Productos

								</a>
								<div class="dropdown-menu">
									<a  class="dropdown-item" href="/productos/{0}" target="_blank" >Todos los Productos</a>
									@foreach ($categoryProducts as $key1 => $c1)
										<a class="dropdown-item" href="/productos/{{$c1->id}}">{{$c1->name}}</a>
									@endforeach
									<a  class="dropdown-item" href="https://skincenterlosangeles.mynuskin.com" target="_blank" >Nuskin</a>	
								</div>
							</li>
							<li><a href="/#our_price" data-scroll="our_price">Promociones</a></li>
							<li><a href="/#map_contact" data-scroll="map_contact">Contacto</a></li>
							@if ($store->id == 1)
								<li><a href="http://skincenter.agendapro.com/cl" target="_blank" >Agenda Enlinea</a></li>
							@else
								<li><a href="#"  data-toggle="modal" data-target="#reservationModal" >Evaluacion</a></li>
							@endif

							<li  class="nav-item dropdown" >
								<a class="nav-link dropdown-toggle user-dropdown" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-user-circle" aria-hidden="true" style="font-size: 22px;"></i>
								</a>
								<div class="dropdown-menu">
									@if (!$user=Auth::user())
										<a class="dropdown-item" href="" data-toggle="modal" data-target="#userRegistration" >Registrar</a>
										<a class="dropdown-item" href="" data-toggle="modal" data-target="#userLogin">Iniciar Sesion</a>
									@else
										<a class="dropdown-item" href="">{{$user->name}}</a>
										<div class="dropdown-divider"></div>
										<a class="dropdown-item" href="/perfil">Perfil</a>
										<a class="dropdown-item" href="/logOut">Cerrar Sesion</a>
									@endif
								</div>
							</li>
							<li>
								<a href="#" v-on:click="showCartList()">
									<i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 22px;"></i>
									<span id="cartBadger" class="badge badge-danger" v-if="cartBadger > 0">@{{cartBadger}}</span>
								</a>
							</li>
							<li class="nav-item dropdown">
								<a class="nav-link dropdown-toggle user-dropdown" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
									<i class="fa fa-map-marker" aria-hidden="true" style="font-size: 22px;"></i>

								</a>
								<div class="dropdown-menu">
									<a class="dropdown-item" href="/goStore/1">Skin Center Los Angeles</a>
									<a class="dropdown-item" href="/goStore/2">Skin Center Temuco</a>
									<a class="dropdown-item" href="/goStore/3">Skin Center Concepción</a>
								</div>
							</li>
						  </ul>
						</div>
					</div>
					<!-- end col-md-8 -->
				</div><!-- end row -->
			</div><!-- end container-fluid -->
		</nav><!-- navbar -->
	</div><!-- end menubar-content -->
</div><!-- end menubar -->
