<!-- Modal -->
<div class="modal fade" id="seguimientoModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="container">
            <div class="row" style="text-align:center">
                <div class="modal-content"  style="text-align:left;width: 50%;">
                    <div class="modal-header">
  
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                        <h4 style="margin-left:15px">Agendar Proxima Sesion</h4>
                    </div>
                    <div class="modal-body" style="padding: 20px;">
                        <form class="form-horizontal">
                            <div class="form-group">
                              <label  class="col-sm-2 control-label">Dia</label>
                              <div class="col-sm-10">
                                <input id="dateSelect" class="form-control datepicker" data-date-format="dd/mm/yyyy">
                              </div>
                            </div>
                            <div class="form-group">
                              <label for="inputPassword3" class="col-sm-2 control-label">Horaras Disponibles</label>
                              <div class="col-sm-10">
                                <select class="form-control"  name="time" ></select>
                              </div>
                            </div>
                           
                            <div class="form-group">
                              <div class="col-sm-offset-2 col-sm-10">
                                <button type="submit" class="btn btn-default">Agendar</button>
                              </div>
                            </div>
                          </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
  