<div class="modal fade" id="cartListModal" role="dialog">
	<div class="modal-dialog" role="document">
		<div class="container">
			<div class="row" style="text-align:center">
				<div class="modal-content" style="text-align:left">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="col-md-12 col-sm-12 col-xs-12">
							<div class="section_title wow animated slideInUp">
								<h2>Lista de Compra</h2>
							</div>
						</div>
						<div class="col-md-12 col-sm-12 col-xs-12" style="    margin-bottom: 40px;">
							<table class="table">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Tipo</th>
										<th>Cantidad</th>
										<th>Precio</th>
										<th>Total</th>
									</tr>
								</thead>
							  	<tbody>
								  	<tr v-for="c in cartList">
		  						      	<td>@{{c.name}}</td>
										<td>@{{c.attributes.n_type}}</td>
		  						      	<td>@{{c.quantity}}</td>
										<td>@{{c.price}}</td>
										<td>@{{c.price * c.quantity }}</td>
		  						    </tr>
							  	</tbody>
								<tfoot>
									<tr>
										<td></td>
										<td></td>
										<td></td>
										<td><strong>TOTAL</strong></td>
										<td>@{{cartTotal}}</td>
									</tr>
								</tfoot>
						    </table>
						</div>

					</div>
					<div class="modal-footer">
						<a href="/cartClean" class="btn_df btn_dafault" >Borrar Lista</a>
						@if ($store->id == 1)
							<a href="/goFlow" class="btn_df btn_dafault" >Comprar</a>
						@else
							<a href="/goFlow" class="btn_df btn_dafault" >Comprar</a>
						@endif
			      </div>
				</div>
			</div>
		</div>
	</div>
</div>
