<!-- Modal -->
<div class="modal fade" id="userRegistration" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
	  <div class="container">
		  <div class="row" style="text-align:center">
			  <div class="modal-content" style="text-align:left;min-width: 330px;">
  		      <div class="modal-header">

  		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
  		          <span aria-hidden="true">&times;</span>
  		        </button>
				<h2 style="margin-left:15px">Registrate</h2>
  		      </div>
  		      <div class="modal-body">
  		        {!! Form::open(['url' =>route('register'),'class'=>'form-material']) !!}
  				<div class="form-group">
  				  <label for="name" class="col-md-12">Nombre</label>
  				  <div class="col-md-12">
  				  	<input type="text" class="form-control"  placeholder="" name="name">
  				</div>
  				</div>
  				<div class="form-group ">
  					<label for="email" class="col-md-12">{{ __('E-Mail
  						') }}</label>

  					<div class="col-md-12">
  						<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-line" name="email" value="{{ old('email') }}" required autofocus>

  						@if ($errors->has('email'))
  							<span class="invalid-feedback" role="alert">
  								<strong>{{ $errors->first('email') }}</strong>
  							</span>
  						@endif
  					</div>
  				</div>
  				<div class="form-group">
  				  <label for="name" class="col-md-12">Telefono</label>
  				  <div class="col-md-12">
  				  <input type="text" class="form-control"  placeholder="" name="phone">
  			  </div>
  				</div>
  				<div class="form-group">
  				  <label for="name" class="col-md-12">Rut</label>
  				  <div class="col-md-12">
  				  <input type="text" class="form-control"  placeholder="" name="rut">
  			  </div>
  				</div>
  				<div class="form-group ">
  					<label for="password" class="col-md-12 ">{{ __('Password') }}</label>

  					<div class="col-md-12">
  						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }} form-control-line" name="password" required>

  						@if ($errors->has('password'))
  							<span class="invalid-feedback" role="alert">
  								<strong>{{ $errors->first('password') }}</strong>
  							</span>
  						@endif
  					</div>
  				</div>
  				<div class="form-group ">
  					<label for="password-confirm" class="col-md-12">{{ __('Confirmar Password') }}</label>

  					<div class="col-md-12">
  						<input id="password-confirm" type="password" class="form-control" name="password_confirmation" required>
  					</div>
  				</div>
				<br>
				<div class="form-group ">
  					<div class="col-md-6 offset-md-4">
  						<div class="form-check">
  							<a href="#" data-toggle="modal" data-target="#userLogin" data-dismiss="modal">Ingresa</a>
  						</div>
  					</div>
  				</div>
				<div class="form-group">

  					<div class="col-md-12 ">

						<div class="btn-slid" style="    margin: 20px auto;float: right;">
						  <button class="btn_df btn_dafault" type="submit">Registrar</button>
						</div>

  					</div>
  				</div>

  		      </div>

  			  {!! Form::close() !!}
  		    </div>
		  </div>

	  </div>



  </div>
</div>
