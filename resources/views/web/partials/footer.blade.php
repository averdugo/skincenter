<footer class="footer">
	<div class="parallax_background parallax-window" data-parallax="scroll" data-image-src="/images/footer.jpg" data-speed="0.8"></div>
	<div class="footer_content">
		<div class="container">
			<div class="row">

				<!-- Footer About -->
				<div class="col-lg-3 footer_col">
					<div class="footer_about">
						<div class="logo">
							<a href="#"><img src="/images/logo_skincenter.png" class="img-fluid pull-xs-left" alt="..."></a>
						</div>
						<div class="footer_about_text">Dermoestética SkinCenter cuenta con una completa gama de tratamientos destinados en su totalidad a la belleza corporal y facial.

Los tratamientos son totalmente personalizados y adaptados a cada paciente de acuerdo a los objetivos que quiera alcanzar, no requieren internación y no son invasivos.</div>
						<div class="footer_social">
							<ul class="d-flex flex-row align-items-center justify-content-start">
								<li><a href="#"><i class="fa fa-pinterest" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-facebook" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-twitter" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-dribbble" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-behance" aria-hidden="true"></i></a></li>
								<li><a href="#"><i class="fa fa-linkedin" aria-hidden="true"></i></a></li>
							</ul>
						</div>
						<div class="copyright">
Copyright &copy;<script>document.write(new Date().getFullYear());</script> Todos los derechos Reservados | Desarrollado  por <a href="https://maadchile.cl" target="_blank">MadChile</a>
<!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
</div>
					</div>
				</div>

				<!-- Footer Contact -->
				<div class="col-lg-5 footer_col">
					<div class="footer_contact">
						<div class="footer_contact_title">Contacto</div>
						<div class="footer_contact_form_container">
							<form action="#" class="footer_contact_form" id="footer_contact_form">
								<div class="d-flex flex-xl-row flex-column align-items-center justify-content-between">
									<input type="text" class="footer_contact_input" placeholder="Nombre" required="required">
									<input type="email" class="footer_contact_input" placeholder="E-mail" required="required">
								</div>
								<textarea class="footer_contact_input footer_contact_textarea" placeholder="Mensaje" required="required"></textarea>
								<button class="footer_contact_button">Enviar</button>
							</form>
						</div>
					</div>
				</div>

				<!-- Footer Hours -->
				<div class="col-lg-4 footer_col">
					<div class="footer_hours">
						<div class="footer_hours_title">Horarios</div>
						<ul class="hours_list">
							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>Lunes – Viernes</div>
								<div class="ml-auto">9.30 – 20.00</div>
							</li>

							<li class="d-flex flex-row align-items-center justify-content-start">
								<div>Sabado</div>
								<div class="ml-auto">10.00 – 20.00</div>
							</li>

						</ul>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="footer_bar">
		<div class="container">
			<div class="row">
				<div class="col">
					<div class="footer_bar_content d-flex flex-sm-row flex-column align-items-lg-center align-items-start justify-content-start">
						<nav class="footer_nav">
							<ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
								<li class="active"><a href="index.html">Inicio</a></li>
								<li><a href="about.html">Productos</a></li>
								<li><a href="services.html">Servicios</a></li>
								<li><a href="news.html">Contacto</a></li>
								<li><a href="contact.html">Sedes</a></li>
							</ul>
						</nav>
						<div class="footer_links">
							<ul class="d-flex flex-lg-row flex-column align-items-lg-center align-items-start justify-content-start">
								<li><a href="#">Los Angeles</a></li>
								<li><a href="#">Temuco</a></li>
								<li><a href="#">Concepcion</a></li>
							</ul>
						</div>
						<div class="footer_phone ml-lg-auto">
							<i class="fa fa-phone" aria-hidden="true"></i>
							<span>+56 (41) 246 2695</span>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</footer>
