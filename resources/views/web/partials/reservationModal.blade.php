<div id="resarvation_from">
  <div class="modal fade" id="reservationModal" role="dialog">
	<div class="modal-dialog" role="document">
	  <div class="container">
		<div class="row">
			<div class="modal-content">
			<div class="modal-header">
			  <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
			</div>
			<div class="modal-body">
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
				  <h2>Solicitar Evaluacion Gratis</h2>
				</div>
			  </div>
			<div class="col-md-12 col-sm-12 col-xs-12">
			  <form class="reservation_form" action="#" method="post">
				<div class="row">
					<div class="col-md-6 col-sm-6 col-xs-12">
					  	<h3>Detalles Tratamiento</h3>
					  	<div class="row">
							<div class="form-group col-md-12 col-sm-12">
							  <label>Servicio</label>
							  <select class="form-control" v-model="reservation.category" @change="getCategory($event)">
								  <option value=""></option>
								@foreach ($categoriesServices as $k => $v)
									<option value="{{$k}}">{{$v}}</option>
								@endforeach
							  </select>
							</div>
							<div class="form-group col-md-12 col-sm-12">
							  <label>Tratamiento</label>
							  <select class="form-control" v-model="reservation.service" >
								<option v-for="(item, key) in services" :value="key">@{{item}}</option>
							  </select>
							</div>

							<div class="form-group col-md-12 col-sm-12">
							  	<label>Fecha Inicial</label>
							  	<input class="form-control" type="date" v-model="reservation.date" value="">
							</div>
							<div class="form-group col-md-12 col-sm-12">
							  <label>Hora Preferencial</label>
							  <input class="form-control" type="time" v-model="reservation.time" value="">
							</div>


						<div class="col-md-12 col-sm-12">
						  <strong>Para mayor informacion contacte : <span>{{$store->phone}}</span></strong>
						</div>
					  </div>
					</div>
					<div class="col-md-6 col-sm-6 col-xs-12">
					  <h3>Datos Personales</h3>
					  <div class="row">
						<div class="form-group col-md-12 col-sm-12">
						  <label>Nombre</label>
						  <input class="form-control no-border"  type="text" name="name" v-model="reservation.name" placeholder="Ingresa tu Nombre"/>
						</div>
						<div class="form-group col-md-12 col-sm-12">
						  <label>Correo</label>
						  <input class="form-control"  type="email" name="email" v-model="reservation.correo" placeholder="Ingresa tu Correo"/>
						</div>
						<div class="form-group col-md-12 col-sm-12">
						  <label>Rut</label>
						  <input class="form-control"  type="text" name="number" v-model="reservation.rut" placeholder="Ingresa tu Rut"/>
						</div>
						<div class="form-group col-md-12 col-sm-12">
						  <label>Telefono</label>
						  <input class="form-control"  type="text" name="address" v-model="reservation.phone" placeholder="Ingresa tu Direccion"/>
						</div>
						<div class="form-group col-md-12 col-sm-12">
						  <a class="btn_df btn_dafault" href="#" v-on:click="submit()">Envia tu Peticion</a>
						</div>
					  </div>
					</div>
				  </div>
				</form>
			  </div>
			</div>
		  </div>
		</div>
	  </div>
	</div>
  </div>
</div>
