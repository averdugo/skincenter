@extends('web.layout')
@section('content')
	<section id="Bannar" class="bgoverlay" style="background-image:url('/images/slides/1.jpg')">
	<div class="container">
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="bannar_area">
			<h2>single post</h2>
			<ul class="banner_nav">
				<li><a href="index.html">Home</a></li>
				<li><a href="#">/</a></li>
				<li class="active">Blog Details</li>
			</ul>
		  </div>
		</div>
	  </div>
	</div>
	</section>
	<!--========== End Bannar Section ============-->

	<!--========== Start Blog Post ===============-->
	<section id="blogpost">
	<div class="container">
	  <div class="row">
		<div class="col-md-8 col-sm-8 col-xs-12">
		  <div class="single_post wow animated slideInUp">
			<div class="blog_image">
			   <img src="images/blog/2.jpg" alt="Image">
			</div>
			<div class="blog_data">
			  <div class="blog_title"><a href="#">Senectus Accumsan Purus Tristique Sollicitudin Platea.</a></div>
			  <div class="blog_info">
				  <span class="blogdate">25 April 17</span>
				  <span class="admin"><i class="fa fa-user"></i><a href="#">Admin</a></span>
				  <span class="comment"><i class="fa fa-comment"></i><a href="#">29 Comments</a></span>
			  </div>
			  <div class="blog_textarea">
				<p>Donec est purus luctus aenean. Inceptos mattis felis ac taciti egestas nullam litora orci tellus curabitur tempus netus est. Imperdiet velit, fames. Nascetur nascetur blandit. Habitant tincidunt orci faucibus. Ante dictum suspendisse auctor mi congue. Nunc rhoncus blandit sagittis ad libero vel facilisi malesuada condimentum dictum condimentum. Dapibus pede volutpat. Quam sed dignissim eros curabitur nunc in dui magna pretium nisl dolor ornare id justo sem.</p>
				<p>Netus elit. Sollicitudin mattis cum sed placerat placerat aliquam potenti amet at sociis, cubilia ut aliquam. Integer justo.</p>
				<blockquote><p>Tempus nam cubilia tempor cubilia. Dignissim fames habitant velit viverra mi aliquam libero pellentesque sodales cursus bibendum. Consequat platea montes dis tempus dictumst eros commodo phasellus pretium cubilia suscipit viverra scelerisque risus. Platea cursus elit gravida, eget. A sed erat integer curabitur metus natoque. Enim metus ut nostra etiam.</p></blockquote>
				<p>Fringilla. Nisi. Penatibus sed integer imperdiet duis habitasse ipsum vulputate Enim velit aliquam, egestas. Quis ipsum gravida facilisis velit Consectetuer. Vitae dapibus vulputate interdum est sem vivamus aliquam fringilla sociosqu habitasse. Senectus. Dolor libero at ornare Donec vestibulum Vivamus adipiscing et hendrerit suspendisse parturient rutrum habitant dictum nibh felis, </p>
				<p>Sit morbi feugiat pellentesque mus lobortis nascetur sagittis iaculis penatibus massa dui fringilla fames ultricies vehicula ullamcorper potenti morbi donec tincidunt ultrices tempus iaculis lectus facilisis duis cubilia fringilla ac, nibh purus nisi. Sem urna lectus. Senectus cras. Felis gravida mi fermentum platea quam vestibulum rutrum tempor libero sagittis per non ac fermentum platea imperdiet porttitor metus ut massa accumsan posuere hac. Velit pede magna blandit risus ornare iaculis at senectus. Imperdiet. Ridiculus fusce metus gravida ac torquent imperdiet.</p>
			  </div>
			  <div class="share_icon" style="display:none">
				<ul>
				  <li>Share :</li>
				  <li><a href="#"><i class="fa fa-facebook"></i></a></li>
				  <li><a href="#"><i class="fa fa-twitter"></i></a></li>
				  <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
				  <li><a href="#"><i class="fa fa-linkedin"></i></a></li>
				  <li><a href="#"><i class="fa fa-rss"></i></a></li>
				</ul>
			  </div>
			</div>
		  </div>


		</div>
		<div class="col-md-4 col-sm-4 col-xs-12">
		  <div class="left_sidebar">
			<div class="sidebar_widget">
			  <div class="latestpt_title"><h2>Categories</h2></div>
			  <div class="Categories">
				<ul>
				  <li><a href="#">Full Body Massage</a></li>
				  <li><a href="#">Spa Treatment</a></li>
				  <li><a href="#">Skin Care Treatment</a></li>
				  <li><a href="#">Beauty</a></li>
				  <li><a href="#">Nail Treatment</a></li>
				  <li><a href="#">Hair Treatment</a></li>
				</ul>
			  </div>
			</div>
			<div class="sidebar_widget">
			  <div class="latestpt_title"><h2>latest post</h2></div>
			  <div class="latest_post">
				<img src="images/blog/4.jpg" alt="Image">
				<div class="latestpt_title"><a href="#">Senectus Accumsan Purus Tristique Sollicitudin Platea.</a></div>
				<div class="blog_info">
				  <span class="blogdate postdata">01 / 05 / 2017</span>
				  <span class="post_comment postdata"><i class="fa fa-comment"></i><a href="#">8 Comments</a></span>
				</div>
			  </div>
			  <div class="latest_post">
				<img src="images/blog/5.jpg" alt="Image">
				<div class="latestpt_title"><a href="#">Senectus Accumsan Purus Tristique Sollicitudin Platea.</a></div>
				<div class="blog_info">
				  <span class="blogdate postdata">03 / 05 / 2017</span>
				  <span class="post_comment postdata"><i class="fa fa-comment"></i><a href="#">2 Comments</a></span>
				</div>
			  </div>
			  <div class="latest_post">
				<img src="images/blog/6.jpg" alt="Image">
				<div class="latestpt_title"><a href="#">Senectus Accumsan Purus Tristique Sollicitudin Platea.</a></div>
				<div class="blog_info">
				  <span class="blogdate postdata">04 / 05 / 2017</span>
				  <span class="post_comment postdata"><i class="fa fa-comment"></i><a href="#">6 Comments</a></span>
				</div>
			  </div>
			</div>

		  </div>
		</div>
	  </div>
	</div>
	</section>
@endsection
