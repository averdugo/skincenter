@extends('web.layout')
@section('css')
	<link rel="stylesheet" type="text/css" href="/styles/contact.css">
	<link rel="stylesheet" type="text/css" href="/styles/contact_responsive.css">
@endsection
@section('content')
	<div class="contact" style="padding-top: 210px;">
		<div class="container">
			<div class="row">

				<!-- Contact form -->
				<div class="col-lg-8 contact_col">
					<div class="contact_form">
						<div class="contact_title">Mantente en Contacto</div>
						<div class="contact_form_container">
							<form action="#" id="contact_form" class="contact_form">
								<input type="text" id="contact_input" class="contact_input" placeholder="Tu Nombre" required="required">
								<input type="email" id="contact_email" class="contact_input" placeholder="Tu Correo" required="required">
								<input type="text" id="contact_subject" class="contact_input" placeholder="Asunto" required="required">
								<textarea class="contact_input contact_textarea" id="contact_textarea" placeholder="Mensaje" required="required"></textarea>
								<button class="contact_button" id="contact_button">Enviar Mensaje</button>
							</form>
						</div>
					</div>
				</div>

				<!-- Make an Appointment -->
				<div class="col-lg-4 contact_col">
					<div class="info_form_container">
						<div class="info_form_title">Agenda una Cita</div>
						<form action="#" class="info_form" id="info_form">
							<select name="info_form_dep" id="info_form_dep" class="info_form_dep info_input info_select">
								@foreach ($categories as $key => $value)
									<option value="{{$key}}">{{$value}}</option>
								@endforeach
							</select>

							<input type="text" class="info_input" placeholder="Nombre" required="required">
							<input type="text" class="info_input" placeholder="Telefono">
							<button class="info_form_button">Agendar</button>
						</form>
					</div>
				</div>

				<!-- contact info -->
				<div class="contact_info">
					<div class="row">
						<div class="col-lg-4 offset-lg-1">
							<div class="contact_info_list">

								<ul>
									<li><strong>Skin Center Los Angeles</strong></li>
									<li><span>Direccion: </span>Av. Alemania 831, local 25.</li>
									<li><span>Telefono: </span>43-2326622</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="contact_info_list contact_info_list_2">
								<ul>
									<li><strong>Skin Center Temuco</strong></li>
									<li><span>Direccion: </span>Hochstetter #1069, local 1</li>
									<li><span>Telefono: </span>45-2427090</li>
								</ul>
							</div>
						</div>
						<div class="col-lg-3">
							<div class="contact_info_list contact_info_list_2">
								<ul>
									<li><strong>Skin Center Concepción</strong></li>
									<li><span>Direccion: </span>Paicavi #483</li>
									<li><span>Telefono: </span>41-2462695</li>
								</ul>
							</div>
						</div>

					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="contact_map">
		<!-- Google Map -->
		<div class="map">
			<div id="google_map" class="google_map">
				<div class="map_container">
					<div id="map"></div>
				</div>
			</div>
		</div>
	</div>

@endsection
@section('scripts')
	<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=AIzaSyCIwF204lFZg1y4kPSIhKaHEXMLYxxuMhA"></script>
	<script src="/js/contact.js"></script>
	<script type="text/javascript">

	</script>
@endsection
