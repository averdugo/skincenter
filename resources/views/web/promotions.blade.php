@extends('web.layout')
@section('css')
	<link rel="stylesheet" type="text/css" href="/styles/about.css">
	<link rel="stylesheet" type="text/css" href="/styles/about_responsive.css">
@endsection
@section('content')
	<div class="services" style="padding-top:230px">
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">Nuestras Promociones</div>
				</div>
			</div>
			<div class="row doctors_row">
				@foreach ($promotions as $key => $value)
					<div class="col-xl-3 col-md-6">
						<div class="doctor">
							<div class="doctor_image"><img src="/uploads/promotions/{{$value->images->file}}" alt=""></div>
							<div class="doctor_content">
								<div class="doctor_name"><a href="/promociones/{{$value->id}}">{{$value->name}}</a></div>
								<div class="doctor_title">{{$value->price}}</div>
								<div class="doctor_link"><a href="#" class="shoppingAdd" data-id="{{$value->id}}" data-type="3">+</a></div>
							</div>
						</div>
					</div>
				@endforeach
			</div>
		</div>
	</div>
@endsection
@section('scripts')
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/js/servicesWeb.js"></script>
@endsection
