<!DOCTYPE html>
<html lang="en">
	<head>
		@php
			switch (\Session::get('store_id')) {
				case 1:
					$title = 'SkinCenter Los Angeles';
					$pixel = '<!-- Facebook Pixel Code -->
								<script>
								!function(f,b,e,v,n,t,s)
								{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
								n.callMethod.apply(n,arguments):n.queue.push(arguments)};
								if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
								n.queue=[];t=b.createElement(e);t.async=!0;
								t.src=v;s=b.getElementsByTagName(e)[0];
								s.parentNode.insertBefore(t,s)}(window,document,\'script\',
								\'https://connect.facebook.net/en_US/fbevents.js\');
								fbq(\'init\', \'552875932105359\'); 
								fbq(\'track\', \'PageView\');
								</script>
								<noscript>
								<img height="1" width="1" 
								src="https://www.facebook.com/tr?id=552875932105359&ev=PageView
								&noscript=1"/>
								</noscript>
								<!-- End Facebook Pixel Code -->';
					break;
				case 2:
					$title = 'SkinCenter Temuco';
					$pixel = '<!-- Facebook Pixel Code -->
							<script>
							!function(f,b,e,v,n,t,s)
							{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
							n.callMethod.apply(n,arguments):n.queue.push(arguments)};
							if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
							n.queue=[];t=b.createElement(e);t.async=!0;
							t.src=v;s=b.getElementsByTagName(e)[0];
							s.parentNode.insertBefore(t,s)}(window,document,\'script\',
							\'https://connect.facebook.net/en_US/fbevents.js\');
							fbq(\'init\', \'407871356658403\'); 
							fbq(\'track\', \'PageView\');
							</script>
							<noscript>
							<img height="1" width="1" 
							src="https://www.facebook.com/tr?id=407871356658403&ev=PageView
							&noscript=1"/>
							</noscript>
							<!-- End Facebook Pixel Code -->';
					break;
				case 3:
					$title = 'SkinCenter Concepción';
					$pixel = '<!-- Facebook Pixel Code -->
								<script>
								!function(f,b,e,v,n,t,s)
								{if(f.fbq)return;n=f.fbq=function(){n.callMethod?
								n.callMethod.apply(n,arguments):n.queue.push(arguments)};
								if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version=\'2.0\';
								n.queue=[];t=b.createElement(e);t.async=!0;
								t.src=v;s=b.getElementsByTagName(e)[0];
								s.parentNode.insertBefore(t,s)}(window,document,\'script\',
								\'https://connect.facebook.net/en_US/fbevents.js\');
								fbq(\'init\', \'2590513847733138\'); 
								fbq(\'track\', \'PageView\');
								</script>
								<noscript>
								<img height="1" width="1" 
								src="https://www.facebook.com/tr?id=2590513847733138&ev=PageView
								&noscript=1"/>
								</noscript>
								<!-- End Facebook Pixel Code -->';
					break;
				default:
					$title = 'SkinCenter';
					$pixel = '';
					break;
			}
		@endphp
		<title>{{$title}}</title>
		<meta charset="utf-8">
		<meta http-equiv="X-UA-Compatible" content="IE=edge">
		<meta name="description" content="En {{$title}} se conjugan tratamientos, tecnología y profesionales de primera línea, que brindan un servicio especializado para cada tratamiento.">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		<meta name="csrf-token" content="{{csrf_token()}}">

		<link rel="stylesheet" href="/web/css/bootstrap.min.css">
		<link rel="stylesheet" href="/web/css/settings.css">
	    <link rel="stylesheet" href="/web/css/style.css">
		<link rel="stylesheet" href="/web/css/responsive.css">
	    <link rel="stylesheet" href="/web/css/colors/color_3.css" id="color-change">
	    <link rel="stylesheet" href="/web/css/default-animate.css">
	    <link rel="stylesheet" href="/web/css/font-awesome.css">
	    <link rel="stylesheet" href="/web/fonts/flaticon.css">
	    <link rel="stylesheet" href="/web/css/bootstrap-formhelpers.min.css">
		<link rel="stylesheet" href="/web/css/loader.css">
		<link href="/css/toastr.css" rel="stylesheet">

		@yield('css')
		{!!$pixel!!}	
	</head>
	<style media="screen">
	.dropdown-item {
		display: block;
		width: 100%;
		padding: .25rem 1.5rem;
		clear: both;
		font-weight: 400;
		color: #212529;
		text-align: inherit;
		white-space: nowrap;
		background: 0 0;
		border: 0;
	}
	.dropdown-item:hover{
		color:black!important;
		opacity:0.7
	}
	</style>
	<body id="page_top" class="page-wrapper page-load" data-anchor="page_top">
		<a href="#" id="scroll" style="display: none;"><span></span></a>
		<div class="preloader">
			<div id="loader-wrap" class="loader-wrap">
				<div class="cssload-loader">
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
					<div class="cssload-side"></div>
				</div>
			</div>
		</div>
		@if (count($errors) > 0)
			<div class="alert alert-danger">
				Error en Contraseña
			</div>
		@endif


		<div id="webApp">
			@include('web.partials.header')
			@yield('content')
			@include('web.partials.cartListModal')
		</div>
		@include('web.partials.reservationModal')


		<script src="/web/js/jquery-1.12.4.min.js"></script>
	    <script src="/web/js/bootstrap.min.js"></script>
	    <script src="/web/js/owl.js"></script>
	    <script src="/web/js/jquery.prettyPhoto.js"></script>
	    <script src="/web/js/jquery.isotope.js"></script>
	    <script src="/web/js/jquery.fancybox.min.js"></script>
	    <script src="/web/js/wow.js"></script>
		<script src="/web/js/mixitup.js"></script>
	    <script src="/web/js/bootstrap-formhelpers.min.js"></script>
		<script src="/web/js/YouTubePopUp.jquery.js"></script>
		<script src="/web/js/jquery.cookie.js"></script>

		<script src="/web/js/validate.js"></script>
		<script src="/web/js/custom.js"></script>

		<script src="/js/web.js"></script>

		@include('web.partials.userRegistrationModal')
		@include('web.partials.userLoginModal')

		@yield('scripts')
		<script>
		    window.auth_user = {!! json_encode($user); !!};
		</script>
		<!-- Global site tag (gtag.js) - Google Analytics -->
		<script async src="https://www.googletagmanager.com/gtag/js?id=UA-62141431-49"></script>
		<script>
		 window.dataLayer = window.dataLayer || [];
		 function gtag(){dataLayer.push(arguments);}
		 gtag('js', new Date());

		 gtag('config', 'UA-62141431-49');
		</script>

	</body>
</html>
