@extends('web.layout')
@section('css')
<style>
	.gallery-section .filters {
		margin-top: 50px;
		margin-bottom: 5px
	}
	.well {
		background: none;
    	border: none;
	}
	.imgSalient {
		max-height: 230px;
    	width: auto;
	}
</style>

@endsection
@section('content')
	<section id="slider">
		<div class="container-fluid">
		   <div class="row">
			  <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
				<!-- Wrapper for slides -->
				<div class="carousel-inner" role="listbox">
				  <div class="item active">
					<div class="overlay"></div>
					<img src="/images/slides/grande-1.png" alt="Slider Image 1">
					  <div class="carousel-caption">
						<div class="slider-title">
						  <h1>Bienvenidos a <span>{{$store->name}}</span></h1>
						</div>
						<p>¡Cuidarse más y mejor! El autocuidado parte por sí mismos, mejorando tu imagen personal y calidad de vida.</p>
					  </div>
				  </div>
				  <div class="item">
					<div class="overlay"></div>
					<img src="/images/slides/grande-2.png" alt="Slider Image 2">
					<div class="carousel-caption">
						<div class="slider-title">
						  <h1>Equipo e  <span>Infraestructura</span></h1>
						</div>
						<p>SkinCenter cuenta con la mejor tecnología, infraestructura y equipo de profesionales para la práctica innovadores tratamientos. </p>
				  	</div>
				  </div>
				</div>
				<!-- Controls -->
				<a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev"> <span class="bdr bg-hvr txt fa fa-angle-left" aria-hidden="true"></span> <span class="sr-only">Anterior</span> </a>
				<a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next"> <span class="bdr bg-hvr txt fa fa-angle-right" aria-hidden="true"></span> <span class="sr-only">Siguiente</span> </a>
			  </div>
		  </div>
		</div>
	</section>
	<section id="about_us" class="py_80" data-scroll="about_us">
		<div class="container">
		  	<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="section_title wow animated slideInUp">
					  <h2>Bienvenidos a {{$store->name}}</h2>

				  </div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="about_img wow animated fadeInLeft">
						@if ($store->id == 1)
							<img src="/images/angeleshome.jpg" alt="About us Image">
						@else
							 <img src="/images/about.jpeg" alt="About us Image">
						@endif

					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="about_content wow animated fadeInRight">
					  	<p style="text-align: justify;">
						  La medicina estética avanza a pasos agigantados, ofreciendo una serie de tratamientos y procedimientos que en conjunto forman una amplia gama de alternativas tanto para mujeres como hombres que buscan una solución a “problemas” estéticos o simplemente la reducción de algunas tallas. <br>
						  Asimismo el paso de los años es enemigo de todos pero su efecto se puede aplacar con la ayuda de las innovadoras técnicas y tecnologías que ofrece la medicina estética. <br>
						  En SkinCenter se conjugan tratamientos, tecnología y profesionales de primera línea, que brindan un servicio especializado para cada tratamiento. <br>
						  Recupera confianza y autoestima gracias a nuestros tratamientos corporales y faciales, que cubren de forma satisfactoria las necesidades más específicas de cada paciente.  <br>
						  Hoy es posible una renovación celular, hacer una limpieza de manchas en la piel o tener mayor luminosidad mejorando la calidad de vida de la persona. <br>
						  En su mayoría son tratamientos efectivos y poco invasivos donde el paciente podrá retomar su rutina diaria de forma normal y sin contra indicaciones.<br>
						</p>
					  <a class="btn_df btn_dafault" href="#" data-toggle="modal" data-target="#reservationModal">Evaluacion Gratis</a>
					</div>
				</div>
		  	</div>
		</div>
	</section>
	<section id="services" class="py_80" data-anchor="services" >
		<div class="container">
			<div class="row">
				<div class="col-md-12 col-sm-12 col-xs-12">
				  <div class="section_title wow animated slideInUp">
					  <h2>Nuestros Tratamientos</h2>
					   <p>SkinCenter tiene los mejores tratamientos para moldear la figura y disimular el paso de los años, tales como rejuvenecimiento facial o “peeling”, entregando una mayor suavidad y luminosidad a tu piel.</p>
				  </div>
				</div>
				@foreach ($categories as $key => $s)
					<a href="tratamiento/{{$s->id}}">
						<div class="col-md-4 col-sm-6 col-xs-12">
						  <div class="service_widget wow animated slideInUp">
							<div class="service_img">
								<img src="/uploads/{{$s['img']}}" alt="">
							  	<div class="serimg_overlay"></div>
							</div>

							<div class="service_content">
							  <h3>{{$s['name']}}</h3>

							</div>
						  </div>
						</div>
					</a>
				@endforeach
			</div>
		</div>
	</section>

	<section id="our_gallery" class="py_80" data-anchor="our_gallery">
		<div class="container-fluid">
			<div class="row">
				<div class="gallery-section">
				   <div class ="col-md-12 col-sm-12 col-xs-12">
					  <div class="section_title">
						  <h2>Productos Destacados</h2>
						  <p>Agasaja tu cuerpo con productos cosméticos de alta calidad. ¡Dale un mejor trato a tu cuerpo y rostro en SkinCenter!</p>
					  </div>
				   </div>
				   <div class="container">
						<div class="row">
							<div class="well">
								@foreach ($products as $p)
									<div class="col-md-3">
										<div class="thumbnail">
											<a href="/productos/{{$p->id}}/details"><img src="/uploads/products/{{$p->images->file ?? ''}}"  class="imgSalient"></a>
										<div class="caption">
										<h3><a href="/productos/{{$p->id}}/details">{{$p->name}}</a></h3>
										<h4>${{$p->price}}</h4>
											<p>{{$p->short_description}}</p>
											<div class="filters text-center">
												<ul class="filter-tabs filter-btns clearfix anim-3-all">
													<p>
														<li href="#" class="active filter" data-role="button" v-on:click="addCartList({{$p->id}},1)" >
															Agregar <i class="fa fa-shopping-cart"></i>
														</li>
														
													</p>
												</ul>
											</div>
										</div>
										</div>        
									</div>
								@endforeach
						</div><!-- End Well -->
					</div>
				</div>
			</div>
		</div>
	</section>
	<!--============== End Our Gallery Section ================-->
	<!--============== Start Our Team Section =================-->
	<section id="our_team" class="py_80" data-anchor="our_team" style="">
		<div class="container">
			<div class="row-fluid">
			  <div class="col-md-12 col-sm-12 col-xs-12">
			    <div class="section_title wow animated slideInUp">
				   <h2>Nuestro Equipo</h2>

			    </div>
			  </div>
			  @foreach ($teams as $key => $t)
				  <div class="col-md-3 col-sm-6 col-xs-12">
    				<div class="team_widget wow animated slideInLeft">
    				  <div class="team_image">
    					<img src="/uploads/team/{{$t->image}}" alt="{{$t->name}}">
    					<div class="teamimg_overlay"></div>
    					<div class="member_data">
    					  <h4>{{$t->name}}</h4>
    					  <span>{{$t->profession}}</span>

    					</div>
    				  </div>
    				  <div class="member_profile">

    					<div class="member_content">
    					  <p>{{$t->description}}</p>
    					</div>
    				  </div>
    				</div>
    			  </div>

			  @endforeach

			</div>
		</div>
	</section>
	<!--============== End Our Team Section =================-->
	<!--============== Start Join us Section=================-->
	 <div id="join_us" class="py_80 bgoverlay">
		<div class="container">
			<div class="row" style="display:none">
			  <div class="col-md-8 col-sm-8 col-xs-12">
				<div class="join_content wow animated slideInLeft">
				  <h3>Join Us Now And Get <span>50% Off</span> Your Next Booking ! </h3>
				  <p>Doesn't said was man creepeth he divide. Land good called. Gathering upon female. Shall saw god. Were fruitful face. Isn't. Gathered firmament you grass bring. Likeness meat divide you're was.</p>
				</div>
			  </div>
			  <div class="col-md-4 col-sm-4 col-xs-12">
				<div class="join_btn wow animated slideInRight">
				  <a class="btn_df btn_dafault" href="about_us.html">read more</a>
				</div>
			  </div>
			</div>
		</div>
	</div>
	<!--============== End Join us Section=================-->
	<!--============== Start Price Section ================-->
	<section id="our_price" class="py_80" data-anchor="our_price">
		<div class="container">
			<div class="row">
			  <div class="col-md-12 col-sm-12 col-xs-12">
				<div class="section_title wow animated slideInUp">
					<h2>Promociones / GiftCards</h2>

				</div>
			  </div>
			  	@foreach ($promotions as $key => $pp)
					<div class="col-md-3 col-sm-6 col-xs-12">
					   	<div class="price_table wow animated slideInLeft" style="height: 370px;"> 
						 	<div class="pricing_img">
								<a href="/promociones/{{$pp->id}}/details" data-placement="top" title="Ver Mas">
						   			<img src="/uploads/promotions/{{$pp->images ? $pp->images->file : '' }}" alt="">
								</a>
						 	</div>

						
							<div class="Price_category">{{substr($pp->name, 0 ,50) }}</div>
						
							<div class="price" style="padding:0px;">Valor Normal: $ {{number_format( $pp->before_price , 0 , ',' , '.' )}}</span></div>
							<div class="price" style="padding:0px;"><strong>Pagando Online: $ {{number_format( $pp->price , 0 , ',' , '.' )}}</strong></span></div>
							<a class="btn_df btn_info" style="bottom: 1px;
							left: 0;
						 	position: absolute;" v-on:click="addCartList({{$pp->id}},3)" href="#">Comprar</a>
					   	</div>
					</div>
			  	@endforeach
				@foreach ($giftCards as $key => $g)
					<div class="col-md-3 col-sm-6 col-xs-12">
					   <div class="price_table wow animated slideInLeft">
						 <div class="pricing_img">
						   <img src="/web/images/price/1.jpg" alt="Price Image 1">
						 </div>

						
						 <div class="Price_category">{{substr($g->name, 0 ,100)}}</div>
						 <div class="price_list" style="padding-left: 10px;">
						  {!!$g->description!!}
						 </div>
						 <div class="price"><strong> {{$g->amount }}</strong></span></div>
						 <a class="btn_df btn_info" href="#" v-on:click="addCartList({{$g->id}},4)" >Comprar</a>
					   </div>
					 </div>
			  	@endforeach
			</div>
		</div>
	</section>
	<!--=============== End Our Price Section =============-->
	 <!--============== Start Our Video Section ===========-->
	 <div id="join_us2" class="py_80 bgoverlay">
 	   <div class="container">
 		   <div class="row" style="display:none">
 			 <div class="col-md-8 col-sm-8 col-xs-12">
 			   <div class="join_content wow animated slideInLeft">
 				 <h3>Join Us Now And Get <span>50% Off</span> Your Next Booking ! </h3>
 				 <p>Doesn't said was man creepeth he divide. Land good called. Gathering upon female. Shall saw god. Were fruitful face. Isn't. Gathered firmament you grass bring. Likeness meat divide you're was.</p>
 			   </div>
 			 </div>
 			 <div class="col-md-4 col-sm-4 col-xs-12">
 			   <div class="join_btn wow animated slideInRight">
 				 <a class="btn_df btn_dafault" href="about_us.html">read more</a>
 			   </div>
 			 </div>
 		   </div>
 	   </div>
    </div>
	<!--============ End Our Video Section ================-->
	<!--============ Start Our Blog Section ===============-->

	<!--================= End Blog Section ============-->
	<!--================= Start Our Client Section ====-->

	<!--============== End Our Client Section ================-->
	<!--============== Start Map and Contact Form Section ====-->
	<section id="map_contact" data-anchor="map_contact">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-6 col-sm-6 col-xs-12">
					<div class="row">
					  <div id="map" data-lat="{{$store->lat}}" data-lng="{{$store->lng}}"class="map-canvas wow animated slideInUp"></div>
					</div>
				</div>
				<div class="col-md-6 col-sm-6 col-xs-12">
				  	<div class="section_title wow animated slideInUp">
						<h2>Contacto</h2>
						<p style="display:none">Third and made a saying stars two. Image stars. All let made subdue, stars fruit blessed were. Lights fowl fruitful.</p>
				  	</div>
					<form class="contact_message" id="contact-form" action="" method="post">
					  	<div class="form-group col-md-6 col-sm-6">
							<input class="form-control no-border" type="text" name="name" placeholder="Nombre" v-model="contact.name"/>
					  	</div>
					  	<div class="form-group col-md-6 col-sm-6">
							<input class="form-control no-border" type="text" name="email" placeholder="Email*" v-model="contact.email"/>
					  	</div>
					  	<div class="form-group col-md-12 col-sm-12">
							<input class="form-control no-border" id="subject" type="text" name="subject" placeholder="Asunto*" v-model="contact.asunto"/>
					  	</div>
					  	<div class="form-group col-md-12 col-sm-12">
							<textarea class="form-control no-border" id="message" name="message" placeholder="Mensaje*" v-model="contact.mensaje"></textarea>
					  	</div>
					  	<div class="form-group col-md-12 col-sm-6">
							<input class="btn_df btn_dafault" id="send"  value="Enviar" @click='contactMail()'/>
					  	</div>
						<div class="col-md-12">
							<div class="error-handel">
								<div id="success">Your email sent Successfully, Thank you.</div>
								<div id="error"> Error occurred while sending email. Please try again later.</div>
							</div>
						</div>
					</form>
			  	</div>
			</div>
		</div>
	</section>
	<!--=========== Map and Contact Form Section End ============-->
	<!--=========== Start Contact Info Section ==================-->
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="contact_info wow animated slideInUp">
					  <div class="contact_icon"><i class="fa fa-map-marker"></i></div>
					  <div class="info">
						<h4>Direccion :</h4>
						<p>{{$store->address}}</p>
					  </div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="contact_info wow animated slideInUp">
					  <div class="contact_icon"><i class="fa fa-phone"></i></div>
					  <div class="info">
						<h4>Telefono :</h4>
						<p>+ {{$store->phone}}</p>
					  </div>
					</div>
				</div>
				<div class="col-md-4 col-sm-4 col-xs-12">
					<div class="contact_info wow animated slideInUp">
					  <div class="contact_icon"><i class="fa fa-envelope-o"></i></div>
					  <div class="info">
						<h4>Email :</h4>
						<p>contact@skincenter.cl</p>
					  </div>
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection
@section('scripts')
	<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyCLA5EDWYTjLr9U_l2mhSF0NXSHkmStDU0"></script>
	<script src="/web/js/map.scripts.js"></script>
@endsection
