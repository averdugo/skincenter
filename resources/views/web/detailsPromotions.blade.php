@extends('web.layout')
@section('css')
	<link rel="stylesheet" type="text/css" href="/styles/about.css">
	<link rel="stylesheet" type="text/css" href="/styles/about_responsive.css">
	<style>
		.imgSalient {
			max-height: 250px;
			width: auto;
		}
	</style>
@endsection
@section('content')
<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
		<div class="row">
			<div class="col-md-12 col-sm-12 col-xs-12">
				<div class="bannar_area">
					<br>
					<h2>Promocion</h2>
				</div>
			</div>
		</div>
	</div>
</section>
<br>
<br>
<br>

<div class="container mt-5">
  	<div class="row">
		<div class="col-md-6 col-sm-6">
			<div class="header-content-left">
				<img src="/uploads/promotions/{{$promotion->images->file}}" style="width: 95%;">
			</div>  
		</div>
		<div class="col-md-6 col-sm-6">
		  	<div class="header-content-right">
				<h1 class="display-4">{{$promotion->name}}</h1>
			  	<h4 class="mt-3">{!!$promotion->description!!}</h4>
				<br>
				<h3 class="mt-5">$ {{number_format( $promotion->price , 0 , ',' , '.' )}}</h3>
				<h5 class="mt-5">En stock</h5>
				<br>
				<br>
				<br>
				<br>
				<br>
				<a class="btn_df btn_dafault" href="#" data-toggle="modal" v-on:click="addCartList({{$promotion->id}},1)">Agregar al Carrito</a>
			  </div>
		</div>
	</div>
	<br>
	<br>
	<br>
	<br>
	<br>
	<br>
</div>
<br>
<br>

@endsection
@section('scripts')
	<script src="/plugins/scrollmagic/ScrollMagic.min.js"></script>

@endsection
