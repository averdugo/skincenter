
@extends('web.layout')
@section('css')
<link rel="stylesheet" href="/css/productsShop.css">
<style>
	.imgSalient {
		max-height: 250px;
    	width: auto;
	}
</style>
@endsection
@section('content')
<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="bannar_area">
			  <br>
			  <h2>Todos los productos</h2>
		  </div>
		</div>
	  </div>
	</div>
</section>
	<div class="container">
		<h3 class="h3">Productos</h3>
		<div class="row">
			@foreach ($products as $p)
				<div class="col-md-3 col-sm-6">
					<div class="product-grid2">
						<div class="product-image2">
							<a href="/productos/{{$p->id}}/details" data-placement="top" title="Ver Mas">
								<img src="/uploads/products/{{$p->images->file ?? ''}}" class="imgSalient">
							</a>
							<ul class="social">
								<li role="button" v-on:click="addCartList({{$p->id}},1)"><i class="fa fa-shopping-cart"></i></li>
							</ul>
							<li class="add-to-cart" href="" v-on:click="addCartList({{$p->id}},1)">Agregar a Carrito</li>
						</div>
						<div class="product-content">
								<a href="/productos/{{$p->id}}/details">
									<h3 class="title">{{$p->name}}</h3>
								</a>
								<span class="price">$ {{number_format( $p->price , 0 , ',' , '.' )}}</span>
						</div>
					</div>
				</div>
			@endforeach
		</div>
	</div>
@endsection
@section('scripts')
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/js/servicesWeb.js"></script>
@endsection
