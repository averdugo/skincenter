@extends('web.layout')
@section('content')

	<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="bannar_area">
			  <br>
			  <h2>{{$store->name}}</h2>

		  </div>
		</div>
	  </div>
	</div>
	</section>
	<!--========== End Bannar Section ============-->

	<!--========== Start Blog Post ===============-->
	<section id="">
	<div class="container">
	  <div class="row">
		<div class="col-md-6 col-sm-6 col-xs-12">
		  <div class="single_post wow animated slideInUp">
			<div class="blog_data">
			<div class="latestpt_title"><h2>Compra Realizada</h2></div>

			  <div class="Categories">
				<ul>
  				  <li>Número de orden de Pedido: <strong class="pull-right">{{$transbankResponse['buyOrder']}}</strong></li>
  				  <li>Nombre del comercio: <strong class="pull-right">SkinCenter</strong></li>
  				  <li>Monto: <strong class="pull-right">{{$transbankResponse['detailOutput']->amount}}</strong></li>
  				  <li>Código de autorización: <strong class="pull-right">{{$transbankResponse['detailOutput']->authorizationCode}}</strong></li>
  				  <li>Fecha de la transacción: <strong class="pull-right">{{$sale->created_at}}</strong></li>
  				  <li>Tipo de pago: <strong class="pull-right">{{$transbankResponse['detailOutput']->paymentTypeCode}}</strong></li>
				  <li>Tarjeta bancaria: <strong class="pull-right">{{$transbankResponse['cardDetail']->cardNumber}}</strong></li>
  				</ul>
			  </div>
			</div>
		  </div>
		</div>
		<div class="col-md-6 col-sm-6 col-xs-12">

		  <div class="left_sidebar" style="    margin-top: 30px;">
			<div class="sidebar_widget">
			  <div class="latestpt_title"><h2>Compras</h2></div>
			  	@foreach ($items as $key => $v)
				  	@php
				  		$path = $forImg[$v->type];
				  	@endphp
				  	@if ($v->type == 2)
					  	<div class="latest_post" style="    display: block;    clear: both;">
						  	<img src="/uploads/{{$path}}/{{$v->item->images->file}}" alt="">
						  	<div class="latestpt_title">
							  	<a href="#">
									{{$v->item->name}}<br>
								  	{{$v->item->sessions}} Sesiones
							  	</a>
						  	</div>
						  	<div class="blog_info" style="float:left;padding: 0;">
							  	<small id="fechaAgendar{{$v->id}}" class=""></small>
							  	<a class="btn_link" href="#" @click="openAgenda({{$v->f_id}},{{$v->id}})"><i class="fa fa-calendar"></i>&nbsp;&nbsp;Agendar</a>
						  	</div>
					  	</div>
					@elseif ($v->type == 4)
						<div class="latest_post" style="    display: block;    clear: both;">
						  	<img src="" alt="">
						  	<div class="latestpt_title">
							  	<a href="#">
									{{$v->item->name}}<br>
							  	</a>
						  	</div>
						  	<div class="blog_info" style="float:left;padding: 0;">
							  	<a class="btn_link" href="#"><i class="fa fa-user"></i>Mandar Email</a>
						  	</div>
					  	</div>
					@else
						<div class="latest_post" style="display: block;    clear: both;">
						  	<img src="/uploads/{{$path}}/{{$v->item->images->file}}" alt="">
						  	<div class="latestpt_title">
							  	<a href="#">
									{{$v->item->name}}<br>
							  	</a>
						  	</div>
						  	<div class="blog_info" style="float:left;padding: 0;">

						  	</div>
					  	</div>
				  @endif

			  @endforeach

			  @include('web.partials.agendarModal')


			</div>


		  </div>
		</div>
	  </div>
	</div>
	</section>
@endsection
@section('scripts')

@endsection
