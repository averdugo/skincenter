@extends('web.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/css/bootstrap-datepicker.min.css">
	<style>.dataTables_wrapper .dataTables_filter{margin-top: 30px}</style>
@endsection
@section('content')
	
	<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="bannar_area">
			  <br>
			  <h2>{{$store->name}}</h2>
		  </div>
		</div>
	  </div>
	</div>
	</section>
	<!--========== End Bannar Section ============-->

	<!--========== Start Blog Post ===============-->
	<section id="">
		<div class="container">
			<div class="row">
				<div class="col-md-12">
				
					
				
				<!-- Nav tabs -->
				<ul class="nav nav-tabs " role="tablist" style="padding-top: 30px;">
					<li role="presentation" class="active">
						<a href="#home" aria-controls="home" role="tab" data-toggle="tab" class="themeText" style="font-size: 18px;text-transform: uppercase;color: #11ad92;    padding: 10px 60px;">
							Mis Datos
						</a>
					</li>
					<li role="presentation">
						<a href="#profile" aria-controls="profile" role="tab" data-toggle="tab" class="themeText" style="font-size: 18px;text-transform: uppercase;color: #11ad92;    padding: 10px 60px;">
							Mis Tratamientos
						</a>
					</li>
					<li role="presentation">
						<a href="#buys" aria-controls="profile" role="tab" data-toggle="tab" class="themeText" style="font-size: 18px;text-transform: uppercase;color: #11ad92;    padding: 10px 60px;">
							Mis Productos
						</a>
					</li>
					<li role="presentation">
						<a href="#messages" aria-controls="messages" role="tab" data-toggle="tab" class="themeText" style="font-size: 18px;text-transform: uppercase;color: #11ad92;    padding: 10px 60px;">
							Mi Agenda
						</a>
					</li>
					
				</ul>
				  
					<!-- Tab panes -->
					<div class="tab-content">
					  	<div role="tabpanel" class="tab-pane active row" id="home">
							<br>
							
							<form method="POST" action="/usersUpdate/{{$user->id}}" class="form-material">
								@csrf
								@method('put')
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Nombre</label>
									<div class="col-md-12">
										<input type="text" class="form-control"  placeholder="" name="name" value="{{$user->name}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="email" class="col-md-12">{{ __('E-Mail') }}</label>
	 								<div class="col-md-12">
										<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }} form-control-line" name="email" readonly value="{{$user->email}}" required autofocus>
	  									@if ($errors->has('email'))
											<span class="invalid-feedback" role="alert">
												<strong>{{ $errors->first('email') }}</strong>
											</span>
										@endif
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Telefono</label>
									<div class="col-md-12">
										<input type="text" class="form-control"  placeholder="" value="{{$user->phone}}" name="phone">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Rut</label>
									<div class="col-md-12">
										<input type="text" class="form-control"  placeholder="" name="rut" value="{{$user->rut}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Direccion</label>
									<div class="col-md-12">
										<input type="text" class="form-control"  placeholder="" name="address" value="{{$user->address}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Comuna</label>
									<div class="col-md-12">
										<input type="text" class="form-control"  placeholder="" name="comuna" value="{{$user->comuna}}">
									</div>
								</div>
								<div class="form-group col-md-6">
									<label for="name" class="col-md-12">Fecha Nacimiento</label>
									<div class="col-md-12">
										<input type="date" class="form-control"  placeholder="" name="birthday_at" value="{{$user->birthday_at}}">
									</div>
								</div>
								<div class="form-group">
									<div class="col-md-12 ">
			  						  <div class="btn-slid" style="    margin: 20px auto;float: right;">
										<button class="btn btn-default" type="submit">Actualizar</button>
									  </div>
			  						</div>
								</div>
							</form>
						</div>
						<div role="tabpanel" class="tab-pane" id="profile">
							<br>
							<div class="table-responsive m-t-20 no-wrap">
								<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
									<thead>
										<tr>
											<th>Tipo</th>
											<th>Nombre</th>
											<th>Cantidad</th>
											<th>Precio</th>
											<th>Fecha</th>
											<th>Estado</th>
											<th>Consentimiento</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($buyItems as $key => $s)
											<tr >
												<td>{{$s['type']}}</td>
												<td>{{$s['name']}}</td>
												<td>{{$s['qty']}}</td>
												
												<td> $ {{ number_format( $s['price'] , 0 , ',' , '.' ) }}</td>
												<td>{{ date("d-m-Y", strtotime($s['fecha']))}}</td>
												<td>{{$s['status']}}</td>
												<td>
													<button class="btn btn-default ">
														<a href="/consentimientoDownload/1">Descargar PDF</a>
													</button>
												</td>
												<td class="text-right">
													@if ($s['type'] == 'Servicio')
														{{$s['sessions']}}&nbsp;
														<button data-id="{{$s['id']}}" class="btn btn-default seguimiento">
															Agendar
														</button>
													@endif
												</td>
											</tr>
										@endforeach
	
	
									</tbody>
								</table>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="buys">
							<br>
							<div class="table-responsive m-t-20 no-wrap">
								<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
									<thead>
										<tr>
											<th>Tipo</th>
											<th>Nombre</th>
											<th>Cantidad</th>
											<th>Preciooo</th>
											<th>Fecha</th>
											<th>Estado</th>
											<th></th>
										</tr>
									</thead>
									<tbody>
										@foreach ($buyProducts as $key => $s)
											<tr >
												<td>{{$s['type']}}</td>
												<td>{{$s['name']}}</td>
												<td>{{$s['qty']}}</td>
												
												<td> $ {{ number_format( $s['price'] , 0 , ',' , '.' ) }}</td>
												<td>{{ date("d-m-Y", strtotime($s['fecha']))}}</td>
												<td>{{$s['status']}}</td>
												<td class="text-right">
													@if ($s['type'] == 'Servicio')
														{{$s['sessions']}}&nbsp;
														<button data-id="{{$s['id']}}" class="btn btn-default seguimiento">
															Agendar
														</button>
													@endif
												</td>
											</tr>
										@endforeach
	
	
									</tbody>
								</table>
							</div>
						</div>
						<div role="tabpanel" class="tab-pane" id="messages">

						</div>
					 
					</div>
				  
				  </div>
			</div>
		</div>
	</div>
	</section>
@endsection
@section('scripts')
@include('web.partials.detailSaleModal')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/js/bootstrap-datepicker.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.9.0/locales/bootstrap-datepicker.es.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.24.0/moment.min.js"></script>
<script>
	
	
	$(function(){
		var itemId
		function getHorarios(date, itemId) {
			console.log(itemId)
			$.get('/getHoursByDate/'+date+'/'+itemId, function(e){
				console.log(e)
			})
		}

		$('#myTable').DataTable({
				'language': {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscador:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    }
		});
		$('.datepicker').datepicker({
			'language':'es'
		}).on('changeDate', function(e) {
			var formatedDate = moment(e.date).format('YYYY-MM-DD');
			getHorarios(formatedDate,itemId)
		});

		$('body').on('click', '.seguimiento', function(){
			itemId = $(this).data('id');
			console.log(itemId)
			$('#seguimientoModal').modal()

			/*$.get('/salesItems/'+id, function(r){
				$('#seguimientoTable').html(r)

			})*/
			
		})

		

		
	})
</script>
@endsection
