@extends('web.layout')
@section('css')
	<link rel="stylesheet" type="text/css" href="/styles/about.css">
	<link rel="stylesheet" type="text/css" href="/styles/about_responsive.css">
	<link rel="stylesheet" type="text/css" href="/styles/services.css">
	<link rel="stylesheet" type="text/css" href="/styles/services_responsive.css">
	<style media="screen">
	.doctor_image img {
		max-width: 100%;
		height: 100%;
		max-height: 250px;
		width: 100%;
	}
	.services{
		    padding-bottom: 0;
	}
	</style>
@endsection

@section('content')

	<div class="doctors" style="padding-top:230px">
		<div class="container">
			<div class="row" >
				<div class="col text-left">
					<h2 class="section_title" style="color: #838383;">Servicios</h2>
					<br>
					<br>
				</div>
			</div>
			<div class="row">
				<div class="col-md-4">
					<div class="tabs d-flex flex-row align-items-center justify-content-start flex-wrap">
						@foreach ($categories as $key => $v)
							<div class="tab {{$key==0?'active':''}}">
								<div class="tab_title">{{$v->name}}</div>

							</div>
						@endforeach
					</div>
				</div>
				<div class="col-md-8">
					<div class="tab_panels">
						@foreach ($categories as $key => $v)
							<div class="tab_panel {{$key==0?'active':''}}">
								<div class="tab_panel_content">
									<div class="row">
										@for ($i=0; $i < 10; $i++)
											@foreach ($services as $k => $p)
												@if ($p->category_id == $v->id)
													<div class="col-xl-4 col-md-6">
														<div class="doctor">
															<div class="doctor_image"><img src="/uploads/services/{{$p->images?$p->images->file:''}}" alt=""></div>
															<div class="doctor_content">
																<div class="doctor_name"><a href="/servicios/{{$name}}/{{$p->id}}">{{$p->name}}</a></div>
																<div class="doctor_title">{{$p->price}}</div>
																<div class="doctor_link"><a href="#" class="shoppingAdd" data-id="{{$p->id}}" data-type="2">+</a></div>
															</div>
														</div>
													</div>
												@endif
											@endforeach
										@endfor

									</div>
								</div>
							</div>
						@endforeach
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="services" >
		<div class="container">
			<div class="row">
				<div class="col text-center">
					<div class="section_title">Nuestros Servicios</div>
					<div class="section_subtitle">Para escoger</div>
				</div>
			</div>
			<div class="row icon_boxes_row">
				@foreach ($categories as $key => $value)
					<div class="col-xl-4 col-lg-6">
						<div class="icon_box">
							<div class="icon_box_title_container d-flex flex-row align-items-center justify-content-start">
								<div class="icon_box_icon"><img src="images/icon_{{$key+1}}.svg" alt=""></div>
								<div class="icon_box_title">{{$value->name}}</div>
							</div>
							<div class="icon_box_text">{!!$value->description!!}</div>
						</div>
					</div>
				@endforeach


			</div>

		</div>
	</div>
@endsection
@section('scripts')
	<script src="plugins/scrollmagic/ScrollMagic.min.js"></script>
	<script src="/js/servicesWeb.js"></script>
@endsection
