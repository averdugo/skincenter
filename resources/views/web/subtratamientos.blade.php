@extends('web.layout')
@section('content')

	<section id="Bannar" class="bgoverlay"  style="background-image:url('/images/slides/2.jpg')">
	<div class="container">
	  <div class="row">
		<div class="col-md-12 col-sm-12 col-xs-12">
		  <div class="bannar_area">
			  <br>
			  <h2>{{$store->name}}</h2>

		  </div>
		</div>
	  </div>
	</div>
	</section>
	<!--========== End Bannar Section ============-->

	<!--========== Start Blog Post ===============-->
	<section id="">
	<div class="container">
	  <div class="row">
		<div class="col-md-10 col-sm-6 col-xs-12">
		  <div class="single_post wow animated slideInUp">
			<div class="blog_data">
				<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
			  <div class="panel panel-default">
				<div class="panel-heading" role="tab" id="headingOne">
				  <h4 class="panel-title">
					<a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne">
					  <div class="latestpt_title">
						  <h2 style="padding-bottom: 0px;">
							  Tratamientos de  {{$c->name}} - {{$subC->name}}
							  <i class="fa fa-angle-down" style="float: right;"></i>
						  </h2>
					  </div>
					</a>
				  </h4>
				</div>
				<div id="collapseOne" class="panel-collapse collapse " role="tabpanel" aria-labelledby="headingOne">
				  <div class="panel-body">
					{!!$c->description!!}
				  </div>
				</div>
			  </div>


			</div>

			@foreach ($services as $key => $s)
				<div class="col-md-4 col-sm-6 col-xs-12">
				  <div class="service_widget wow animated slideInUp">
					<div class="service_img">
					  <img src="/uploads/services/{{$s['images']['file']}}" alt="bodymassage">
					  <div class="serimg_overlay"></div>
					</div>
					<div class="service_iconprice">
						<div class="ser_icon serviceModal" data-service="{{$s}}"  style="cursor:pointer">
						  <i class="fa fa-info" aria-hidden="true" style="font-size: 22px;"></i>
						</div>
						<div class="ser_price" v-on:click="addCartList({{$s['id']}},2)"  style="cursor:pointer">
						   <i class="fa fa-shopping-cart" aria-hidden="true" style="font-size: 22px;"></i>
						</div>
					</div>
					<div class="service_content">
					  <h3>{{$s['name']}}</h3>
					  <ul>
						<li><strong>{{$s['price']}}</strong></li>
					  	<li><strong>{{$s['sessions']}} </strong>Sesiones</li>
						<li><strong>{{$s['time']}}m.</strong> cada Sesion</li>
					  </ul>
					</div>
				  </div>
				</div>
			@endforeach
			</div>
		  </div>
		</div>
		<div class="col-md-2 col-sm-6 col-xs-12">

		  <div class="left_sidebar" style="    margin-top: 30px;">
			<div class="sidebar_widget">
			  <div class="latestpt_title"><h2>SubCategorias</h2></div>
			  	@foreach ($subCategories as $key => $v)


					  	<div class="latest_post" style="    display: block;    clear: both;">
						  	<div class="latestpt_title">
							  	<a href="/tratamiento/{{$v->category_id}}/{{$v->id}}">
									{{$v->name}}<br>
							  	</a>
						  	</div>

					  	</div>


			  @endforeach




			</div>


		  </div>
		</div>
	  </div>
	</div>
	</section>
	<div class="modal fade" id="serviceModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	  <div class="modal-dialog" role="document">

		  <div class="container">
			  <div class="row " style="text-align:center">
				  <div class="modal-content col-md-offset-2 col-md-8 "  style="text-align:left">
	  		      <div class="modal-header">

	  		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	  		          <span aria-hidden="true">&times;</span>
	  		        </button>

	  		      </div>
	  		      <div class="modal-body">
					  <div class="col-md-12">
						<div class="section_title wow animated slideInUp">
						  <h2 >Solicitar Evaluacion Gratis</h2>
						</div>
						<div class="sectionModal">

  					  </div>
					  </div>

				  </div>
	  		    </div>
			  </div>

		  </div>


	  </div>
	</div>
@endsection
@section('scripts')
	<script type="text/javascript">
		$('body').on('click', '.serviceModal', function(){
			var service = $(this).data('service');
			$('#serviceModal .section_title h2').text(service.name)
			$('#serviceModal .sectionModal').html(service.description)
			$('#serviceModal').modal()
		})
	</script>
@endsection
