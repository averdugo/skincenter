<!DOCTYPE html>
<html lang="{{app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon"   href="/favicon.ico" type="image/x-icon" >
	    <title>SkinCenter-Admin</title>
	    <link href="/admin/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
	    <link href="/admin/css/style.css" rel="stylesheet">
	    <link href="/admin/css/pages/dashboard1.css" rel="stylesheet">
	    <link href="/admin/css/colors/default.css" id="theme" rel="stylesheet">
		<link href="/css/toastr.css" rel="stylesheet">
		<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.5.0/css/all.css" integrity="sha384-B4dIYHKNBt8Bc12p+WXckhzcICo0wtJAoU8YZTY5qE0Id1GSseTk6S+L3BlXeVIU" crossorigin="anonymous">
		<link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.css">

		@yield('css')
	</head>
	<body class="fix-header fix-sidebar card-no-border">
		<div class="preloader" style="">
	        <div class="loader">
	            <div class="loader__figure"></div>
	            <p class="loader__label">SkinCenter Admin</p>
	        </div>
	    </div>
		<div id="main-wrapper">
			@include('admin.header')
			@include('admin.sidebar')
			<div class="page-wrapper">
	            <div class="container-fluid">
					@yield('content')
				</div>
			</div>
		</div>
		<script type="text/javascript">
			window.Laravel = <?php echo json_encode(['csrfToken'=> csrf_token()]); ?>
		</script>
		<script src="/admin/assets/node_modules/jquery/jquery.min.js"></script>
	    <!-- Bootstrap popper Core JavaScript -->
	    <script src="/admin/assets/node_modules/bootstrap/js/popper.min.js"></script>
	    <script src="/admin/assets/node_modules/bootstrap/js/bootstrap.min.js"></script>
	    <!-- slimscrollbar scrollbar JavaScript -->
	    <script src="/admin/js/perfect-scrollbar.jquery.min.js"></script>
	    <!--Wave Effects -->
	    <script src="/admin/js/waves.js"></script>
	    <!--Menu sidebar -->
	    <script src="/admin/js/sidebarmenu.js"></script>
	    <!--Custom JavaScript -->
	    <script src="/admin/js/custom.min.js"></script>
	    <!-- ============================================================== -->
	    <!-- This page plugins -->
	    <!-- ============================================================== -->
	    <!--morris JavaScript -->
	    <script src="/admin/assets/node_modules/raphael/raphael-min.js"></script>
	    <script src="/admin/assets/node_modules/morrisjs/morris.min.js"></script>
	    <!--c3 JavaScript -->
	    <script src="/admin/assets/node_modules/d3/d3.min.js"></script>
	    <script src="/admin/assets/node_modules/c3-master/c3.min.js"></script>
	    <!-- Chart JS -->

		<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
		<script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.js"></script>
		@yield('scripts')

	</body>
</html>
