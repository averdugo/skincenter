@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
		.easy-autocomplete-container{
			z-index: 2000;
			    position: relative;
		}
		.easy-autocomplete {
			width: 100%!important
		}
		label.col-md-12 {
		    text-align: left;
		}
	</style>
@endsection
@section('content')
	@if (session('status'))
    <div class="alert alert-success">
        {{ session('status') }}
    </div>
@endif
	<div id="salesContent" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Ventas</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createSaleModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Venta</a>
			</div>
		</div>
		@include('admin.sales.crearModal')
		@include('admin.sales.seguimientoModal')
		@include('admin.sales.agendarModal')
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>
							<div class="ml-auto sr-only">
								<select class="custom-select b-0">
									<option selected="">January</option>
									<option value="1">February</option>
									<option value="2">March</option>
									<option value="3">April</option>
								</select>
							</div>
						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Cliente</th>
										<th>Fecha</th>
										<th>Items</th>
										<th>Total</th>
										<th>Estado</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($sales as $key => $s)
										<tr >
											<td style="width:50px;"><span class="round">{{$s->id}}</span></td>
											<td>{{$s->user->name}}</td>
											<td>{{$s->created_at}}</td>
											<td>{{count($s->items)}}</td>
											<td>{{$s->total}}</td>
											<td>{{$status[$s->status]}}</td>
											<td>
												<a data-id="{{$s->id}}" href="#" class="link m-r-10 seguimiento" >
													<i data-toggle="tooltip" data-placement="top" title="Seguimiento" class="fa fa-clipboard-list" style="font-size: 18px;"></i>
												</a>

											</td>
										</tr>
									@endforeach


								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


@endsection
@section('scripts')

	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
	<script src="/js/sales.js"></script>
	<script type="text/javascript">
		$(function(){


			$('body').on('click', '.seguimiento', function(){
				var id = $(this).data('id');
				SaleVue.seguimiento(id);
			})

			$('body').on('click', '.btnAgendaItem', function(){
				var id = $(this).data('id');
				SaleVue.agendar(id);
			})

			$('body').on('click', '.statusBtn', function(){
				var data = {
					id:$(this).data('id'),
					status:$(this).data('status')
				}
				$.post('/saleItemStatus', data, function(r){
					if (r == 1) {
						alert('Estado de Item Actualizado');
						Swal.fire('Estado de Item Actualizado')
						setTimeout( function(){
						   location.reload()
					   }  , 3000 );

					}
				})
			})




			$('#myTable').DataTable({
				'language': {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscador:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    }
			});

			var type = 0;
			var price = 0;
			$('#typeSelect').on('change', function() {
				 type = this.value
				if (type == 2 || type == 1) {
					$('#categoryRow').show();
					$('#categorySelect').html('<option value="">Seleccione</option>');
					$.get('/getCategoryByType/'+type, function(r){
						$.each(r, function (key, val) {

							$('#categorySelect').append('<option value="'+val.id+'">'+val.name+'</option>');
						});
					})
				}else if (type == 3) {
					$.get('/promotions', function(r){
						$('#itemSelect').html('<option value="">Seleccione</option>');
						$.each(r, function (key, val) {

							$('#itemSelect').append('<option data-ob="'+val.price+'" value="'+val.id+'">'+val.name+'</option>');
						});
					})
				}else if (type == 4) {

					$.get('/giftcardsApp', function(r){
						$('#itemSelect').html('<option value="">Seleccione</option>');
						$.each(r, function (key, val) {

							$('#itemSelect').append('<option data-ob="'+val.price+'" value="'+val.id+'">'+val.name+'</option>');
						});
					})
				}

			});

			$('#categorySelect').on('change', function(){
				var cat = this.value
				$.get('/getItemsByType/'+type+'/'+cat, function(r){
					$('#itemSelect').html('<option value="">Seleccione</option>');
					$.each(r, function (key, val) {

						$('#itemSelect').append('<option data-ob="'+val.price+'" value="'+val.id+'">'+val.name+'</option>');
					});
				})

			})

			$('#itemSelect').on('change', function(){
				price = $(this).find(':selected').data('ob');
				var p = price.replace('$', '')
				price = p.replace('.', '')
				$('#priceItem').val(price)

			})

			$('#qtyItem').on('change', function(){
				price = price * this.value
				$('#priceItem').val(price)
			})

			var dataClients = <?php echo json_encode($arrayClients); ?>;
			var options =  {
				getValue: "name",
				data:dataClients,
				list: {
					onClickEvent: function() {
						var id = $("#basics").getSelectedItemData().id;
						$("input[name='user_id']").val(id);
					},
					showAnimation: {
				      type: "slide"
				    },
				    hideAnimation: {
				      type: "slide"
				  	},
					match: {
						enabled: true
					}
				}
			}

			$("#basics").easyAutocomplete(options);

			var dataClients = <?php echo json_encode($arrayEst); ?>;
			var options2 =  {
				getValue: "name",
				data:dataClients,
				list: {
					onClickEvent: function() {
						var id = $("#basics2").getSelectedItemData().id;
						$("input[name='est_id']").val(id);
					},
					showAnimation: {
				      type: "slide"
				    },
				    hideAnimation: {
				      type: "slide"
				  	},
					match: {
						enabled: true
					}
				}
			}

			$("#basics2").easyAutocomplete(options2);
		})
	</script>

@endsection
