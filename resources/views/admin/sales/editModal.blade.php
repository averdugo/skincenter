<div class="modal fade" id="editProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Producto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'productos','files'=>true,'class'=>'form-material']) !!}
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('intern_id', 'ID')}}

							{{Form::text('intern_id', null,['class' => 'form-control form-control-line','v-model'=>'element.intern_id'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('brand', 'Marca')}}
								{{Form::text('brand', null,['class' => 'form-control form-control-line','v-model'=>'element.brand'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('name', 'Nombre')}}
							{{Form::text('name', null,['class' => 'form-control form-control-line','v-model'=>'element.name'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('format','Formato')}}
								{{Form::text('format', null,['class' => 'form-control form-control-line', 'v-model'=>'element.format'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('price','Precio')}}
  						  	{{Form::text('price', null,['class' => 'form-control form-control-line','v-model'=>'element.price'])}}
						</div>
						<div class="col-md-6">
							{{Form::label('stock','Stock')}}
							{{Form::number('stock', null,['class' => 'form-control form-control-line','v-model'=>'element.stock'])}}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('category_id','Categoria')}}
    						{{ Form::select('category_id', $categories, null , array('placeholder'=>'','class' => 'form-control form-control-line ', 'v-model'=>'element.category_id')) }}
						</div>
						<div class="col-md-6">
							{{Form::label('store_id','Tienda')}}
    						{{ Form::select('store_id', $stores, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'element.store_id')) }}
						</div>
					</div>
					<div class="form-group ">
						{{Form::label('description','Descripccion')}}
						<ckeditor :editor="editor" v-model="element.description" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors2" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="update()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
