<div class="modal fade" id="createSaleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Venta</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::open(['url' => 'sales','class'=>'form-material']) !!}
			  	<div class="modal-body">
					<input type="hidden" name="store_id" value="{{$store->id}}">
					<input type="hidden" name="user_id" value="">
					<input type="hidden" name="createdBy" value="{{\Auth::user()->id}}">
					<div class="form-group row">
						<div class="col-md-6">
							<label for="">Tipo</label>
							<select id="typeSelect" class="form-control" name="type">
								<option value="">Seleccione</option>
								<option value="1">Producto</option>
								<option value="2">Servicio</option>
								<option value="3">Promocion</option>
								<option value="4">GiftCard</option>
							</select>
						</div>
						<div class="col-md-6" id="categoryRow" style="display:none">
							<div class="form-group">
								<label for="">Categoria</label>
								<select id="categorySelect" class="form-control" name="category_id">
									<option value="">Seleccione</option>
								</select>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-6">
							<label for="">Item</label>
							<select id="itemSelect" class="form-control" name="f_id">
								<option value="">Seleccione</option>
							</select>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Cantidad</label>
								<input id="qtyItem" type="number" class="form-control" name="qty" value="">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('client','Cliente')}}
								{{Form::text('client', null,['id'=>'basics','class' => 'form-control form-control-line','style'=>'width:100%'])}}
							</div>
						</div>
						<div class="col-md-6">
							{{Form::label('total','Precio')}}
  						  	{{Form::text('total', null,['class' => 'form-control form-control-line', 'id'=>'priceItem'])}}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('type','Tipo')}}
								<select class="form-control" name="salestype">
									<option value="">Seleccione</option>
									@foreach ($types as $key => $s)
										<option value="{{$key}}">{{$s}}</option>
									@endforeach
								</select>

							</div>
						</div>
						<div class="col-md-6">
							{{Form::label('status','Estado')}}
							<select class="form-control" name="status">
								<option value="">Seleccione</option>
								@foreach ($status as $key => $s)
									<option value="{{$key}}">{{$s}}</option>
								@endforeach
							</select>
						</div>
					</div>
			  	</div>
			  	<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Guardar</button>
			  	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
