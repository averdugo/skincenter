@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
	<link rel="stylesheet" type="text/css" href="/admin/css/daterangepicker.css" />

@endsection
@section('content')
	@if (\Session::has('status'))
    <div class="alert alert-info">
        <span>{!! \Session::get('status') !!}</span>
    </div>
@endif
	<h4>Egresos {{$type == 1 ? $item->flow->name.'-'.$item->name : $item->item->flow->name.'-'.$item->item->name.'-'.$item->name }}</h4><br>
	<div class="" style="position:relative">
		<form action="/administrador/cashFlowEgresos/{{$type}}/{{$f_id}}" method="POST" class="form-inline">
			@csrf
			<div class="form-group mb-2">
				<input type="text" name="daterange" class="form-control" id="daterange" placeholder="Select value">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary mb-2" style="margin-left: 20px;">Filtrar</button>
			</div>
		</form>
		<button type="submit" class="btn btn-info mb-2 float-right" style="position: absolute; right: 0;top: 0;" data-toggle="modal" data-target="#createExpense">Crear</button>
	</div>
	<table id="flowTable" class="table">
		<thead>
			<tr>
				<th>Imagen</th>
				<th>Observacion</th>
				<th>Fecha de Gasto</th>
				<th>Fecha de Creado</th>
				<th>Total</th>
				<th>#</th>
			</tr>
		</thead>
		<tbody>
			@php
				$total = 0;
			@endphp
			@foreach ($egresos as $key => $f)
				@php
					$total = $f->amount + $total;
				@endphp
				<tr>
					<td><img src="{{ asset("storage/$f->img")}}" alt=""></td>
					<td>{{$f->observation}}</td>
					<td>{{date('d-m-Y', strtotime($f->make_at))}}</td>
					<td>{{date('d-m-Y', strtotime($f->created_at))}}</td>
					<td>$ <?=number_format( $f->amount , 0 , ',' , '.' );?></td>
					<td>
						<form action="{{ url('expenses' , $f->id ) }}" method="POST">
						    {{ csrf_field() }}
						    {{ method_field('DELETE') }}
						    <button type="submit" class="btn btn-danger btn-sm">x</button>
						</form>
					</td>
				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td>Total</td>
				<td style="padding: 10px 13px 6px 10px;">$ <?=number_format( $total , 0 , ',' , '.' );?></td>
				<td></td>
			</tr>
		</tfoot>
	</table>

	<div class="modal fade" id="createExpense" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
	  <div class="modal-dialog" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title" id="exampleModalLabel">Crear Gasto</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body">
			  <form action="/expenses" method="POST"  enctype="multipart/form-data">
				  @csrf
				  <input type="hidden" name="type" value="{{$type}}">
				  <input type="hidden" name="f_id" value="{{$f_id}}">
				  <input type="hidden" name="store_id" value="{{$store->id}}">
			    <div class="form-group">
			      <label for="">Imagen de Boleta o Factura</label>
			      <input type="file" name="file" value="">
			    </div>
			    <div class="form-group">
			      <label for="">Cantidad</label>
			      <input type="number" class="form-control" name="amount" placeholder="50000">
			    </div>
				<div class="form-group">
			      <label for="">Fecha de Gasto</label>
			      <input type="date" name="make_at" class="form-control">
			    </div>
				<div class="form-group">
			      <label for="">Observacion</label>
			      <textarea name="observation" class="form-control"></textarea>
			    </div>

			    <button type="submit" class="btn btn-primary">Crear</button>
			  </form>
	      </div>

	    </div>
	  </div>
	</div>
@endsection

@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
	<script src="/admin/js/moment.min.js" charset="utf-8"></script>
	<script src="/admin/js/daterangepicker.js" charset="utf-8"></script>
	<script type="text/javascript">
		$(function(){
			$('#flowTable').DataTable({
				"pageLength": 25,
			    'language': {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscador:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    }
			});
			$('#daterange').daterangepicker({
		        autoApply:true,
				locale: {
		            format: 'YYYY-MM-DD'
		        }
		    });

		})
	</script>
@endsection
