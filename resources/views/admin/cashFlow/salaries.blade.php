<div id="" class="panel panel-primary">
	<div class="panel-body" >
		<div class="row">
			<table class="table">
				<thead>
					<tr>
						<th>Nombre</th>
						<th>Rut</th>
						<th>Sueldo</th>
						<th>Leyes Sociales</th>
						<th>Total</th>
					</tr>
				</thead>
				<tbody>
					@foreach ($workers as $key => $w)
						<tr>
							<td>{{$w->name}}</td>
							<td>{{$w->rut}}</td>
							<td>{{$w->salary}}</td>
							<td>{{$w->social_law}}</td>
							<td>{{$w->salary - $w->social_law}}</td>
						</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>
</div>
