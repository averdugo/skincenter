@extends('admin.layout')
@section('css')
<style media="screen">
	.list-group-item.active{
		background: none;
		color: #495057;
		border-color: rgba(0,0,0,0.4);
	}
	.list-group-item:first-child {
		border-top-left-radius: 0;
		border-top-right-radius:0
	}
	.list-group-item:last-child{
		border-bottom-left-radius: 0;
		border-bottom-right-radius:0
	}
</style>
<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">

@endsection
@section('content')
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
	<div id="" class="panel panel-primary">
    	<div class="panel-body" >
			<div class="col-md-12 text-left">
				<ul class="nav nav-tabs" id="myTab" role="tablist">
				  	<li class="nav-item">
				   		<a class="nav-link active" id="flujo-tab" data-toggle="tab" href="#flujo" role="tab" aria-controls="flujo" aria-selected="true">Flujo</a>
				 	</li>
				  	<li class="nav-item">
				    	<a class="nav-link " id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="false">Ingresos</a>
				  	</li>
				  	<li class="nav-item">
				    	<a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="true">Egresos</a>
				  	</li>
				  	<li class="nav-item">
				    	<a class="nav-link" id="contact-tab" data-toggle="tab" href="#contact" role="tab" aria-controls="contact" aria-selected="false">Remuneraciones</a>
				  	</li>

				</ul>
				<div class="tab-content" id="myTabContent" style="padding: 20px 0;">
					<div class="tab-pane fade show active" id="flujo" role="tabpanel" aria-labelledby="flujo-tab">
						@include('admin.cashFlow.flow')
					</div>
				  	<div class="tab-pane fade " id="home" role="tabpanel" aria-labelledby="home-tab">
				  		@include('admin.cashFlow.ingresos')
				  	</div>
				  	<div class="tab-pane fade " id="profile" role="tabpanel" aria-labelledby="profile-tab">
				  		@include('admin.cashFlow.egresos')
				  	</div>
				  	<div class="tab-pane fade" id="contact" role="tabpanel" aria-labelledby="contact-tab">
				  		@include('admin.cashFlow.salaries')
				  	</div>
				</div>
			</div>
        </div>
    </div>
@endsection
@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
<script type="text/javascript">
	$(function() {
		$('#flowTable').DataTable({

			"pageLength": 25,
		    'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});

		$('body').on('click', '.selectEgreso', function(){
			console.log('asda');
			$('.selectEgreso').removeClass('active');
			$(this).addClass('active');
			var id = $(this).data('id');
			$('.sectionSet').hide();
			$('.sectionSub').hide();
			$('#sectionFlow'+id).fadeIn();
		})
		$('body').on('click', '.selectItemEgreso', function(){
			$('.selectItemEgreso').removeClass('active');
			$(this).addClass('active');
			var id = $(this).data('id');
			$('.sectionSub').hide();
			$('#sectionSub'+id).fadeIn();
		})
		$('body').on('click', '.goModal', function(){
			location.href="/administrador/cashFlowEgresos/"+$(this).data('type')+"/"+$(this).data('id')
		})
		$('body').on('click', '.selectIngreso', function(){
			$('.selectIngreso').removeClass('active');
			$(this).addClass('active');
			var id = $(this).data('id');
			$('.sectionSet').hide();
			$('.sectionSub').hide();
			$('#sectionFlowIngresos'+id).fadeIn();
		})
		$('body').on('click', '.selectItemIngreso', function(){
			$('.selectItemIngreso').removeClass('active');
			$(this).addClass('active');
			var id = $(this).data('id');
			$('.sectionSub').hide();
			if (id == 1) {
				$('#productSection').fadeIn();
			}else if (id == 2) {
				$('#catSection').fadeIn();
			}

		})
		$('body').on('click', '.goLink', function(){
			var type = 0;
			if ($(this).data('type')) {
				type = $(this).data('type')
			}
			location.href="/administrador/cashFlowIngresos/"+type+"/"+$(this).data('id')
		})

	});
</script>

@endsection
