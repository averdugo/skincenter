@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css" />
	<link rel="stylesheet" type="text/css" href="/admin/css/daterangepicker.css" />

@endsection
@section('content')
	@if (\Session::has('status'))
    <div class="alert alert-info">
        <span>{!! \Session::get('status') !!}</span>
    </div>
@endif
	<h4>{{$title}}</h4><br>
	<div class="" style="position:relative">
		<form action="/administrador/cashFlowIngresosP/{{$type}}/{{$f_id}}" method="POST" class="form-inline">
			@csrf
			<div class="form-group mb-2">
				<input type="text" name="daterange" class="form-control" id="daterange" placeholder="Select value">
			</div>
			<div class="form-group">
				<button type="submit" class="btn btn-primary mb-2" style="margin-left: 20px;">Filtrar</button>
			</div>
		</form>

	</div>
	<table id="flowTable" class="table">
		<thead>
			<tr>
				<th>Titulo</th>
				<th>Usuario</th>
				<th>Fecha</th>
				<th>Total</th>

			</tr>
		</thead>
		<tbody>
			@php
				$total = 0;
			@endphp
			@foreach ($items as $key => $f)
				@php

					$total = $f->price + $total;
				@endphp
				<tr>
					<td>{{$f->item->name}}</td>
					<td>{{$f->user->name}}</td>
					<td>{{date('d-m-Y', strtotime($f->created_at))}}</td>
					<td>$ <?=number_format( $f->price , 0 , ',' , '.' );?></td>

				</tr>
			@endforeach
		</tbody>
		<tfoot>
			<tr>
				<td></td>
				<td></td>
				<td>Total</td>
				<td style="padding: 10px 13px 6px 10px;">$ <?=number_format( $total , 0 , ',' , '.' );?></td>
			</tr>
		</tfoot>
	</table>


@endsection

@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" charset="utf-8"></script>
	<script src="/admin/js/moment.min.js" charset="utf-8"></script>
	<script src="/admin/js/daterangepicker.js" charset="utf-8"></script>
	<script type="text/javascript">
		$(function(){
			$('#flowTable').DataTable({
				"pageLength": 25,
			    'language': {
			        "decimal": "",
			        "emptyTable": "No hay información",
			        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
			        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
			        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
			        "infoPostFix": "",
			        "thousands": ",",
			        "lengthMenu": "Mostrar _MENU_ Entradas",
			        "loadingRecords": "Cargando...",
			        "processing": "Procesando...",
			        "search": "Buscador:",
			        "zeroRecords": "Sin resultados encontrados",
			        "paginate": {
			            "first": "Primero",
			            "last": "Ultimo",
			            "next": "Siguiente",
			            "previous": "Anterior"
			        }
			    }
			});
			$('#daterange').daterangepicker({
		        autoApply:true,
				locale: {
		            format: 'YYYY-MM-DD'
		        }
		    });

		})
	</script>
@endsection
