<div id="" class="panel panel-primary">
	<div class="panel-body" >
		<div class="row">
			<div class="col-md-3">
				<div class="list-group">
					@foreach ($itemsEgresos as $key => $i)
						<a href="#" data-id="{{$i->id}}" class="list-group-item list-group-item-action  d-flex justify-content-between align-items-center selectEgreso">
							{{$i->name}}
							<i class=" fa fa-angle-right"></i>
						</a>
					@endforeach
				</div>
			</div>
			<div class="col-md-4">
				@foreach ($itemsEgresos as $key => $i)
					<section class="sectionSet" id="sectionFlow{{$i->id}}" style="display:none">
								<div class="list-group">
									@foreach ($i->items as $k=> $v)
										<a href="#" data-id="{{$v->id}}" data-type="1" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center {{count($v->subItems) != 0?'selectItemEgreso':'goModal'}} ">
											{{$v->name}}
											@if (count($v->subItems) != 0)
												<i class=" fa fa-angle-right"></i>
											@endif
										</a>
									@endforeach
								</div>
					</section>
				@endforeach
			</div>
			<div class="col-md-4">
				@foreach ($itemsEgresos as $key => $i)
					@foreach ($i->items as $k=> $v)
						@if (count($v->subItems) != 0)
							<section class="sectionSub" id="sectionSub{{$v->id}}" style="display:none">
								<div class="list-group">
									@foreach ($v->subItems as $k=> $s)
										<a href="#" data-id="{{$s->id}}" data-type="2" class="list-group-item list-group-item-action  d-flex justify-content-between align-items-center goModal">
											{{$s->name}}
										</a>
									@endforeach
								</div>
							</section>
						@endif
					@endforeach
				@endforeach
			</div>
		</div>
	</div>
</div>
