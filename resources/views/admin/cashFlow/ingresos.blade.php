<div id="" class="panel panel-primary">
	<div class="panel-body" >
		<div class="row">

			<div class="col-md-4">
				@foreach ($itemsIngresos as $key => $i)
					<section class="sectionSet" id="sectionFlowIngresos{{$i->id}}" >
						<div class="list-group">
							@foreach ($i->items as $k=> $v)
								<a href="#" data-id="{{$v->id}}" data-type="{{$v->id}}" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center {{$v->id == 1 || $v->id == 2?'selectItemIngreso':'goLink'}} ">
									{{$v->name}}
									@if ($v->id == 1 || $v->id == 2)
										<i class=" fa fa-angle-right"></i>
									@endif
								</a>
							@endforeach
						</div>
					</section>
				@endforeach
			</div>
			<div class="col-md-4">
				<section class="sectionSub" id="productSection" style="display:none">
					<div class="list-group">
						@foreach ($catProducts as $k=> $v)
							<a href="#" data-id="{{$v->id}}" data-type="1" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center goLink ">
								{{$v->name}}
							</a>
						@endforeach
					</div>
				</section>
				<section class="sectionSub" id="catSection" style="display:none">
					<div class="list-group">
						@foreach ($catServices as $k=> $v)
							<a href="#" data-id="{{$v->id}}" data-type="2" class="list-group-item list-group-item-action d-flex justify-content-between align-items-center goLink">
								{{$v->name}}
							</a>
						@endforeach
					</div>
				</section>
			</div>
		</div>
	</div>
</div>
