<h2>Flujo Mes {{$mes}} - {{$dt->year}}</h2>

<table id="flowTable" class="table">
	<thead>
		<tr>
			<th>#</th>
			<th>Flujos</th>
			<th>Items</th>
			<th>Total</th>
		</tr>
	</thead>
	<tbody>
		@php
			$total = 0;
		@endphp
		@foreach ($flowItems as $key => $f)
			@php
				if ($f->type == 1) {
					$total = $total + $f->totalAmount;
				}else{
					$total = $total - $f->totalAmount;
				}

			@endphp
			<tr>
				<td>{{$f->id}}</td>
				<td>{{$f->flow->name}}</td>
				<td>{{$f->name}}</td>
				<td>$ {{number_format( $f->totalAmount , 0 , ',' , '.' )}}</td>
			</tr>
		@endforeach
			<tr>
				<td>70</td>
				<td>Remuneraciones</td>
				<td>Total</td>
				<td>$ {{number_format( $remuTotal , 0 , ',' , '.' )}}</td>
			</tr>
	</tbody>
	<tfoot>
		@php
			$total = $total - $remuTotal;
		@endphp
		<tr>
			<td></td>
			<td></td>
			<td>Balance Total</td>
			<td class="alert {{$total > 0 ? 'alert-success':'alert-danger'}}">$ {{number_format( $total , 0 , ',' , '.' )}}</td>
		</tr>
	</tfoot>
</table>
