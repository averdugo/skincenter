<div class="modal fade" id="createModalC" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Usuarios</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('users.store')}}" method="POST" enctype="multipart/form-data">

				@csrf
				<input type="hidden" id="token" value="{{csrf_token()}}">
				<div class="modal-body">
					<input type="hidden" name="type" value="{{$clients->type=4}}">
					<input type="hidden" name="store_id" value="{{$store->id}}">

					<div class="form-group row">
						<div class="col-md-12">
							<label for="file">Portada</label>
							<div class="input-group">
									<input type="file" name="file" class="form-control">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="name">Nombre</label>
							<div class="input-group">
									<input type="text" name="name" id="name" class="form-control" placeholder="Nombre" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="email">Email</label>
							<div class="input-group">
									<input type="email" name="email" id="email" class="form-control" placeholder="Precio" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="password">Password</label>
							<div class="input-group">
									<input type="password" name="password" id="password" class="form-control" placeholder="Password" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="phone">Telefono</label>
							<div class="input-group">
									<input type="text" name="phone" id="phone" class="form-control" placeholder="Telefono" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="rut">Rut</label>
							<div class="input-group">
									<input type="text" name="rut" id="rut" class="form-control" placeholder="Rut sin guiones" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="address">Direccion</label>
							<div class="input-group">
									<input type="text" name="address" id="address" class="form-control" placeholder="Direccion" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="comuna">Comuna</label>
							<div class="input-group">
									<input type="text" name="comuna" id="comuna" class="form-control" placeholder="Comuna" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="birthday_at">Fecha Nacimiento</label>
							<div class="input-group">
									<input type="date" name="birthday_at" id="birthday_at" class="form-control" placeholder="Fecha Nacimiento" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="weight">Peso</label>
							<div class="input-group">
									<input type="text" name="weight" id="weight" class="form-control" placeholder="Peso" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="height">Estatura</label>
							<div class="input-group">
									<input type="text" name="height" id="height" class="form-control" placeholder="Estatura" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="refer_by">Referido por</label>
							<div class="input-group">
								<select class="form-control" name="refer_by" id="refer_by"  placeholder="Referido por" required>
									<option>Tipo</option>
									<option value="">Seleccione</option>
									<option value="Revista NOS">Revista NOS</option>
									<option value="TVU">TVU</option>
									<option value="Medico">Médico</option>
									<option value="Facebook">Facebook</option>
									<option value="Instagram">Instagram</option>
									<option value="Google">Google</option>
									<option value="Web">Pagina Web</option>
									<option value="Fachada">Fachada</option>
									<option value="Recomendacion">Recomendación</option>
									<option value="Otro">Otro</option>					
								</select>
							</div>
						</div> 
					</div>
					<div class="form-group row">         
						<div class="col-md-12">
							<label for="">Observacion - Nota</label>
							<textarea name="observation" id="observation" class="form-control" required></textarea>
						</div>
					</div> 
		  		</div>
				<div class=" row col-md-12 text-right">
					<input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
				</div>
			</form>
		</div>
	</div>
</div>
