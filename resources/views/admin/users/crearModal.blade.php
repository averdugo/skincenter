<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Usuarios</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'usuarios','files'=>true,'class'=>'form-material']) !!}


					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('imagenes', 'Foto')}}<br>
							<input type="file" id="file" ref="file" v-on:change="handleFileUpload()"/>
						</div>

					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('name', 'Nombre')}}
							{{Form::text('name', null,['class' => 'form-control form-control-line','v-model'=>'data.name'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('email', 'Email')}}
								{{Form::text('email', null,['class' => 'form-control form-control-line','v-model'=>'data.email'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('rut', 'Rut')}}
								{{Form::text('rut', null,['class' => 'form-control form-control-line','v-model'=>'data.rut'])}}
							</div>
						</div>
						<div class="col-md-6">
							{{Form::label('password', 'Password')}}
							<input type="password" name="password" class="form-control form-control-line" v-model="data.password">

						</div>
						<div class="col-md-6">
							{{Form::label('phone', 'Telefono')}}
							{{Form::text('phone', null,['class' => 'form-control form-control-line','v-model'=>'data.phone'])}}
						</div>
						<div class="col-md-6">
							{{Form::label('address','Direccion')}}
    						{{Form::text('address', null,['class' => 'form-control form-control-line','v-model'=>'data.address'])}}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('type','Tipo')}}
							@if (\Auth::user()->type == 1)
								{{ Form::select('type', [3=>'Esteticista',2=>'Administrador',1=>'Super Administrador'], null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.type')) }}
							@else
								{{ Form::select('type', [3=>'Esteticista',2=>'Administrador'], null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.type')) }}
							@endif

						</div>
						<div class="col-md-6">
							{{Form::label('rol','Rol-Especialidad')}}
    						{{Form::text('rol', null,['class' => 'form-control form-control-line','v-model'=>'data.rol'])}}
						</div>
					</div>
					<div class="form-group row">

						<div class="col-md-6">
							{{Form::label('store_id','Tienda')}}
    						{{ Form::select('store_id', $stores, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.store_id')) }}
						</div>
						<div class="col-md-6">
							{{Form::label('salary','Sueldo Base')}}
    						<input type="number" v-model="data.salary" class="form-control form-control-line" value="">
						</div>
					</div>
					<div class="form-group row" >
						<div class="col-md-6">
							{{Form::label('social_law','Imposiciones')}}
    						<input type="number" v-model="data.social_law" class="form-control form-control-line" value="">
						</div>

					</div>

					<div class="form-group ">
						{{Form::label('description','Observacion - Nota')}}
						<ckeditor :editor="editor" v-model="data.observation" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="submit()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
