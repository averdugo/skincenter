<div class="modal fade" id="evaluationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Agendar Evaluacion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::open(['url' => 'schedules','class'=>'form-material']) !!}
		  		<div class="modal-body">

					<input type="hidden" name="store_id" value="{{$store->id}}">
					<input type="hidden" name="user_id" value="">
					<input type="hidden" name="est_id" value="">
					<input type="hidden" name="type" value="2">

					<div class="form-group row">
						<div class="col-md-6 text-left">
							{{Form::label('category_id','Categoria')}}
    						<select class="form-control form-control-line" name="category_id" @change="getCategory($event)"> >
								<option value="">Seleccione</option>
								@foreach ($categories as $key => $value)
									<option value="{{$key}}">{{$value}}</option>
								@endforeach
    						</select>
						</div>
						<div class="col-md-6 text-left">
							{{Form::label('service_id','Tratamiento')}}
    						<select class="form-control form-control-line" name="service_id" >
								<option v-for="(item, key) in services" :value="key">@{{item}}</option>
    						</select>
						</div>
					</div>
					<div class="form-group row">

						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('este','Profesional')}}
								{{Form::text('este', null,['id'=>'basics2','class' => 'form-control form-control-line','style'=>'width:100%'])}}
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Fecha')}}
								<input type="date" name="start_at" value="" class="form-control form-control-line">
							</div>
						</div>
					</div>
					<div class="form-group row">

						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Hora Inicio')}}
								<input type="time" min="09:00:00" max="22:00:00"  name="start_at_time" value="" class="form-control form-control-line">
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Hora Fin')}}
								<input type="time" min="09:00:00" max="22:00:00"  name="end_at_time" value="" class="form-control form-control-line">
							</div>
						</div>

					</div>
					<div class="form-group row">

						<div class="col-md-12 text-left">
							<div class="form-group">
								{{Form::label('este','Observaciones')}}
								<textarea name="observation" class="form-control "> </textarea>
							</div>
						</div>
					</div>


		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger"></span>
				</div>
				<button type="submit" class="btn btn-primary" >Guardar</button>
		  	</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>
