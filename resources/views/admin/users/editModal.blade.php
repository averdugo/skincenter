<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Usuario</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'productos','files'=>true,'class'=>'form-material']) !!}
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('name', 'Nombre')}}
							{{Form::text('name', null,['id'=>'n','class' => 'form-control form-control-line','v-model'=>'element.name'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('email', 'Email')}}
								{{Form::text('email', null,['id'=>'e','class' => 'form-control form-control-line','v-model'=>'element.email'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('phone', 'Telefono')}}
							{{Form::text('phone', null,['id'=>'t','class' => 'form-control form-control-line','v-model'=>'element.phone'])}}
						</div>
						<div class="col-md-6">
							{{Form::label('address','Direccion')}}
							{{Form::text('address', null,['id'=>'d','class' => 'form-control form-control-line','v-model'=>'element.address'])}}
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('rol','Rol-Especialidad')}}
    						{{Form::text('rol', null,['id'=>'r','class' => 'form-control form-control-line','v-model'=>'element.address'])}}
						</div>
						<div class="col-md-6">
							{{Form::label('store_id','Tienda')}}
    						{{ Form::select('store_id', $stores, null , array('placeholder'=>'','id'=>'s','class' => 'form-control form-control-line ','v-model'=>'element.store_id')) }}
						</div>
					</div>
					<div class="form-group ">
						{{Form::label('description','Observacion - Nota')}}
						<ckeditor :editor="editor" v-model="element.observation" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors2" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="update()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
