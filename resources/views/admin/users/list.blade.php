@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
	.easy-autocomplete-container{
		z-index: 2000;
			position: relative;
	}
	.easy-autocomplete {
		width: 100%!important
	}
	</style>
@endsection
@section('content')
	<div id="mainClients" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Clientes</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createModalC" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Cliente</a>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>

						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>E-mail</th>
										<th>Telefono</th>
										<th>Direccion</th>
										<th>Acciones</th>

									</tr>
								</thead>
								<tbody>
									@foreach ($clients as $c)
										
									<tr>
										<td>{{$c->id}}</td>
										<td>{{$c->name}}</td>
										<td>{{$c->email}}</td>
										<td>{{$c->phone}}</td>
										<td>{{$c->address}}</td>
										<td>		
											<a href="#" class="link m-r-10" @click.prevent="evaluationModal(element.id)">
												<i data-toggle="tooltip" data-placement="top" title="Evaluacion" class="fa fa-clipboard-list" style="font-size: 18px;"></i>
											</a>
											<a href="/administrador/cliente/{{$c->id}}/tratamientos" class="link m-r-10">
												<i data-toggle="tooltip" data-placement="top" title="Tratamientos" class="fas fa-notes-medical" style="font-size: 18px;"></i>
											</a>
											<a href="#" class="link m-r-10 edit" data-userid="{{$c->id}}">
												<i data-toggle="tooltip" data-placement="top" title="Editar" class="fa fa-pencil-alt" 
												style="font-size: 18px;"></i>
											</a>
											<form action="/users/{{ $c->id }}" method="post" style="display:inline">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default" type="submit"style="background:transparent;padding: 0 12px" >
                                                    <i title="Eliminar" class="fa fa-trash-alt" style="color:#67757c;"></i>
                                                </button>
											</form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('admin.users.crearModalClientes')
	@include('admin.users.editModalClientes')
	@include('admin.users.editPhoto')
	@include('admin.users.evaluationModal')

@endsection
@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>
	<script src="/js/clients.js"></script>
	<script>
		$(function(){
          
		  $('body').on('click','.edit',function(){
			var id = $(this).data('userid')
			$.get('/users/'+id, function(r){
				console.log(r);
				$('#editModal form').attr('action', "/users/"+r.id)
                    $('#editModal input[name=name]').val(r.name)
					$('#editModal input[name=email]').val(r.email)
					$('#editModal input[name=phone]').val(r.phone)
					$('#editModal input[name=rut]').val(r.rut)
                    $('#editModal input[name=address]').val(r.address)
					$('#editModal input[name=comuna]').val(r.comuna)
					$('#editModal input[name=birthday_at]').val(r.birthday_at)
					$('#editModal input[name=weight]').val(r.weight)
					$('#editModal input[name=height]').val(r.height)
					$('#editModal select[name=refer_by]').val(r.refer_by)
                    $('#editModal textarea[name=observation]').text(r.observation)
                    $('#editModal').modal()
			})
		  })
		  $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
				location.reload()
			})
        })
	</script>
	<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable({
			'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});

		var dataEst = <?php echo json_encode($arrayEst); ?>;

		var options2 =  {
			getValue: "name",
			data:dataEst,
			list: {
				onClickEvent: function() {
					var id = $("#basics2").getSelectedItemData().id;
					$("input[name='est_id']").val(id);
				},
				showAnimation: {
			      type: "slide"
			    },
			    hideAnimation: {
			      type: "slide"
			  	},
				match: {
					enabled: true
				}
			}
		}
		$("#basics2").easyAutocomplete(options2);

	} );

	</script>
@endsection
