@extends('admin.layout')
@section('content')
	<div id="mainUsers" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Usuarios</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Usuarios</a>
			</div>
			@include('admin.users.crearModal')
			@include('admin.users.editModal')
			@include('admin.users.editPhoto')
		</div>
		<div class="row">
			<div class="col-lg-8">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>

						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>E-mail</th>
										<th>Tipo</th>
										<th>Rol</th>

									</tr>
								</thead>
								<tbody>
									<tr v-for="l in lists" @click="showElement(l.id)" style="cursor:pointer">
										<td style="width:50px;"><span class="round">@{{l.id}}</span></td>
										<td style="width:50px;">@{{l.name}}</td>
										<td>@{{l.email}}</td>
										<td>@{{l.type}}</td>
										<td>@{{l.rol}}</td>

									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
			<div class="col-lg-4">
				<div class="card">
					<div class="up-img" @click="editPhoto()" style="cursor:pointer" :style="{ 'background-image': 'url(' + element.portada + ')' }"></div>
					<div class="card-body">
						<h5 class=" card-title">@{{element.name}} </h5>
						<span class="label label-default label-rounded" style="color:black">@{{element.type}}</span>
						<span class="label label-primary label-rounded" style="color:black">@{{element.rol}}</span>
						<p class="m-b-0 m-t-20" >@{{element.email}}</p>
						<p class="m-b-0 m-t-20" >@{{element.address}}</p>
						<p class="m-b-0 m-t-20" v-html="element.observation"></p>

						<div class="d-flex m-t-20">

							<div class="ml-auto align-self-center">
								<a href="#" class="link m-r-10" data-toggle="modal" data-target="#editModal">
									<i class="fa fa-pencil-alt"></i>
								</a>
								<a href="#" class="link m-r-10" @click.prevent="deleteElement()">
									<i class="fa fa-trash-alt"></i>
								</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


@endsection
@section('scripts')
	<script src="/js/users.js"></script>
@endsection
