@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
	.easy-autocomplete-container{
		z-index: 2000;
			position: relative;
	}
	.easy-autocomplete {
		width: 100%!important
	}
	</style>
@endsection
@section('content')
	<div id="mainClients" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Tratamientos</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						@if (session('status'))
						<div class="alert alert-success">
							{{ session('status') }}
						</div>
						@endif
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>
							<div class="col-md-7 text-right">
								<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Asignar Tratamientos</a>
							</div>
						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>Nombre</th>
										<th>Fecha de Inicio</th>
										<th>Numero de Sesiones</th>
										<th>Acciones</th>

									</tr>
								</thead>
								<tbody>
									@foreach ($tratamientos as $t)	
									<tr>
										<td>{{$t->name}}</td>
										<td>{{$t->appointment}}</td>
										<td>{{$t->sessions}}</td>
										<td>
											<a href="#" data-id="{{$t->clientservice_id}}" class="link m-r-10 consentimientosMod">
												<i data-toggle="tooltip" data-placement="top" title="Subir Consentimientos" class="fas fa-file-upload" style="font-size: 18px;"></i>
											</a>	
											<a href="#" data-toggle="modal" data-target="#ventaT"  class="link m-r-10 sr-only">
												<i data-toggle="tooltip" data-placement="top" title="Crear Venta" class="fas fa-shopping-bag" style="font-size: 18px;"></i>
											</a>
											<a href="/administrador/tratamientosFicha" class="link m-r-10">
												<i data-toggle="tooltip" data-placement="top" title="Ver Ficha" class="fas fa-notes-medical" style="font-size: 18px;"></i>
											</a>		
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('admin.users.serviciosClientes.crear')
	@include('admin.users.serviciosClientes.uploadFiles')
	@include('admin.users.serviciosClientes.saleT')

@endsection
@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>
	<script src="/js/clients.js"></script>
	<script type="text/javascript">
	$(document).ready( function () {

		$('body').on('click', '.consentimientosMod', function() {
			var id = $(this).data('id')
			
			
			$.get('/clientServices/'+id, function(r){

				if(r.consent_path != null) {
					$('.consentimientoFile a').attr('href', '/storage/public/'+r.consent_path);
					$('.consentimientoFile').show();
				}

				$('#consentimientosMod input[name=clientservice_id]').val(id)
				$('#consentimientosMod').modal();
			
			})
			
			
			
			
			

		})

	    $('#myTable').DataTable({
			'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});

		$('#categorySelect').on('change', function() {
			var id = this.value;

			$.get('/categories/getSubCategories/'+id, function(r){
				$('#subCategorySelect').html('');
				r.forEach(element => {
					$('#subCategorySelect').append('<option value="'+element.id+'">'+element.name+'</option>')
					$('#subcategoryRow').show()
				});
				
			})
		});

		$('#subCategorySelect').on('change', function() {
			var id = this.value;

			$.get('/serviceFindBySubcategory/'+id, function(r){
				$('#serviceSelect').html('');
				r.forEach(element => {
					$('#serviceSelect').append('<option value="'+element.id+'">'+element.name+'</option>')
					$('#serviceRow').show()
				});
				
			})
		});

	} );

	</script>
@endsection
