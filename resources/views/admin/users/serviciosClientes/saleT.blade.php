<div class="modal fade" id="ventaT" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Venta</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="/venta" method="POST" enctype="multipart/form-data">

				@csrf
				<input type="hidden" id="token" value="{{csrf_token()}}">
				<div class="modal-body">
					

					<div class="form-group row">
						<div class="col-md-6">
							<label for="">Tipo</label>
							<select id="typeSelect" class="form-control" name="type">
								<option value="">Seleccione</option>
								<option value="1">Producto</option>
								<option value="2">Servicio</option>
								<option value="3">Promocion</option>
								<option value="4">GiftCard</option>
							</select>
                        </div>
                        <div class="col-md-6">
							<label for="">Tipo Venta</label>
							<select id="itemSelect" class="form-control" name="salestype">
                                <option value="">Seleccione</option>
							</select>
                        </div>
                    </div>
                    <div class="form-group row">
						<div class="col-md-6">
							<label for="">Item</label>
							<select id="itemSelect" class="form-control" name="f_id">
								<option value="">Seleccione</option>
							</select>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Cantidad</label>
								<input id="qtyItem" type="number" class="form-control" name="qty" value="">
							</div>
						</div>
                    </div>
					<div class="form-group row">
                        <div class="col-md-6">
							<div class="form-group">
								<label for="">Categoria</label>
								<select id="categorySelect" class="form-control" name="category_id">
									<option value="">Seleccione</option>
								</select>
							</div>
						</div>
                        <div class="col-md-6">
							<label for="">Estado</label>
							<select id="itemSelect" class="form-control" name="status">
                                <option value="">Seleccione</option>
							</select>
						</div>
					</div>
		  		</div>
				<div class=" row col-md-12 text-right">
					<input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
				</div>
			</form>
		</div>
	</div>
</div>
