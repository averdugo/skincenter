<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Asignar Tratamiento</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::open(['url' => '/clientService','class'=>'form-material']) !!}
			  	<div class="modal-body">
                    <input type="hidden" name="store_id" value="{{$store->id}}">
                    <input type="hidden" name="user_id" value="{{$user->id}}">
					
					<div class="form-group row">
						<div class="col-md-6" id="categoryRow" >
							<div class="form-group">
								<label for="">Categoria</label>
								<select id="categorySelect" class="form-control" name="category_id">
                                    <option value="">Seleccione</option>
                                    @foreach ($categories as $c)
                                        <option value="{{$c->id}}"> {{$c->name}}</option>
                                    @endforeach
								</select>
							</div>
                        </div>
                        <div class="col-md-6" id="subcategoryRow" >
							<div class="form-group">
								<label for="">Sub - Categoria</label>
								<select  id="subCategorySelect" class="form-control" name="category_id">
									
								</select>
							</div>
						</div>
					</div>

					<div class="form-group row">
						<div class="col-md-6" id="serviceRow" >
							<div class="form-group">
								<label for="">Tratamiento</label>
								<select id="serviceSelect" class="form-control" name="service_id" required>
									<option value="">Seleccione</option>
								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								<label for="">Fecha inicio</label>
								<input id="qtyItem" type="date" class="form-control" name="appointment" value="">
							</div>
						</div>
					</div>
					
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('status','Estado')}}
							<select class="form-control" name="status">
								<option value="">Seleccione</option>
                                <option value="1">Pendiente</option>
                                <option value="2">Proceso</option>
                                <option value="3">Terminada</option>
							</select>
						</div>
						
					</div>
			  	</div>
			  	<div class="modal-footer">
					<button type="submit" class="btn btn-primary">Guardar</button>
			  	</div>
			{!! Form::close() !!}
		</div>
	</div>
</div>
