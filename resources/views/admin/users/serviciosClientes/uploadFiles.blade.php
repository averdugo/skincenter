<div class="modal fade" id="consentimientosMod" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Subir Consentimientos</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
            </div>
            <div class="container">
                <div class="row">
                  <div class="col-md-10 col-md-offset-1">
                    <div class="panel panel-default">
                        <div class="panel-body">

                          <form method="POST" action="/subirConsentimiento" accept-charset="UTF-8" enctype="multipart/form-data">
                            
                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                            <input type="hidden" name="clientservice_id" value>
							
							<br>
                            <div class="form-group row">
								<div class="col-md-6">
									<label for="date">Nuevo Archivo</label>
									<input type="file" name="consent_path" class="form-control">
								</div>
							</div> 
							<div class="form-group">
								<div class="col-md-6 consentimientoFile">
									<a href="" class="link m-r-10 consentimientoFile">
										<i data-toggle="tooltip" data-placement="top" title="Descargar Consentimiento" class="fas fa-download" style="font-size: 40px;"></i>
									</a>	
								</div> 
							</div> 
                            <div class="form-group">
                              <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                              </div>
                            </div>
                          </form>
                        </div>
                      </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>