@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
	.easy-autocomplete-container{
		z-index: 2000;
			position: relative;
	}
	.easy-autocomplete {
		width: 100%!important
	}
	</style>
@endsection
@section('content')
	<div id="mainClients" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Ficha</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createficha" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Sesion</a>
			</div>

		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>

						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>Fecha</th>
										<th>Nº Sesion</th>
										<th>Zona a tratar</th>
										<th>Longitud de onda</th>
                                        <th>Fluencia J/cm2</th>
                                        <th>Ancho Pulso</th>
                                        <th>S.Size mm</th>
                                        <th>Observaciones</th>
										<th>Acciones</th>

									</tr>
								</thead>
								<tbody>
									@foreach ($patientFile as $p)
									<tr>
										<td>{{$p->date}}</td>
                                        <td>{{$p->session}}</td>
										<td>{{$p->treatment_area}}</td>
										<td>{{$p->wavelength}}</td>
										<td>{{$p->j_cm2}}</td>
                                        <td>{{$p->wide_pulse}}</td>
                                        <td>{{$p->size_mm}}</td>
                                        <td>{{$p->observations}}</td>
										<td>		
											<a href="#" class="link m-r-10 edit" data-fichaid="{{$p->id}}">
												<i class="fa fa-pencil-alt"></i>
											</a>
											<form action="/administrador/tratamientosFicha/{{ $p->id }}" method="post" style="float:right">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default" type="submit"style="background:transparent;padding: 0 12px" >
                                                    <i title="Eliminar" class="fa fa-trash-alt"></i>
                                                </button>
											</form>
										</td>
									</tr>
									@endforeach
									
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
    </div>
    
	@include('admin.users.serviciosClientes.fichasClientes.crear')
	@include('admin.users.serviciosClientes.fichasClientes.edit')	
	

@endsection
@section('scripts')
	<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
	<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>
	<script src="/js/clients.js"></script>
	<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable({
			'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});
    } );

	</script>
	<script>
		$(function(){
          
		  $('body').on('click','.edit',function(){
			var id = $(this).data('fichaid')
			$.get('/administrador/tratamientosFicha/'+id, function(r){
				console.log(r);
				$('#editFicha form').attr('action', "/administrador/tratamientosFicha/"+r.id)
                    $('#editFicha input[name=date]').val(r.date)
                    $('#editFicha input[name=session]').val(r.session)
					$('#editFicha input[name=treatment_area]').val(r.treatment_area)
					$('#editFicha input[name=wavelength]').val(r.wavelength)
					$('#editFicha input[name=j_cm2]').val(r.j_cm2)
					$('#editFicha input[name=wide_pulse]').val(r.wide_pulse)
					$('#editFicha input[name=size_mm]').val(r.size_mm)
					$('#editFicha input[name=observations]').val(r.observations)
                    $('#editFicha').modal()
			})
		  })
		  $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
				location.reload()
			})
        })
	</script>
@endsection
