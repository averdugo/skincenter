<div class="modal fade" id="editFicha" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Ficha Tratamiento</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="/administrador/tratamientosFicha/$id" method="POST">
                @method('PUT')
				@csrf
				<input type="hidden" id="token" value="{{csrf_token()}}">
				<div class="modal-body">
					<div class="form-group row">
						<div class="col-md-6">
							<label for="date">Fecha</label>
							<div class="input-group">
                                <input type="date" name="date" id="date" required class="form-control">
							</div>
						</div>
						<div class="col-md-6">
							<label for="session">Nº Sesion</label>
							<div class="input-group">
									<input type="number" name="session" id="session" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="treatment_area">Zona a tratar</label>
							<div class="input-group">
									<input type="text" name="treatment_area" id="treatment_area" class="form-control" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="wavelength">Longitud de Onda</label>
							<div class="input-group">
									<input type="text" name="wavelength" id="wavelength" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="j_cm2">Fluencia J/cm2</label>
							<div class="input-group">
									<input type="text" name="j_cm2" id="j_cm2" class="form-control" required>
							</div>
						</div>
						<div class="col-md-6">
							<label for="wide_pulse">Ancho Pulso</label>
							<div class="input-group">
									<input type="text" name="wide_pulse" id="wide_pulse" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<label for="size_mm">S.Size mm</label>
							<div class="input-group">
									<input type="text" name="size_mm" id="size_mm" class="form-control" required>
							</div>
						</div>
					</div>
					<div class="form-group row">         
						<div class="col-md-12">
							<label for="">Observaciones</label>
							<textarea name="observations" id="observations" class="form-control" required></textarea>
						</div>
					</div> 
		  		</div>
				<div class=" row col-md-12 text-right">
					<input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
				</div>
			</form>
		</div>
	</div>
</div>
