<!DOCTYPE html>
<html lang="{{app()->getLocale() }}">
	<head>
		<meta charset="utf-8">
		<meta name="csrf-token" content="{{ csrf_token() }}">
		<title></title>
		<meta name="viewport" content="width=device-width, initial-scale=1">
	    <meta name="description" content="">
	    <meta name="author" content="">
	    <link rel="icon" type="image/png" sizes="16x16" href="../assets/images/favicon.png">
	    <title>SkinCenter-Admin</title>
	    <link href="/admin/assets/node_modules/bootstrap/css/bootstrap.min.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/morrisjs/morris.css" rel="stylesheet">
	    <link href="/admin/assets/node_modules/c3-master/c3.min.css" rel="stylesheet">
	    <link href="/admin/css/style.css" rel="stylesheet">
	    <link href="/admin/css/pages/dashboard1.css" rel="stylesheet">
	    <link href="/admin/css/colors/default.css" id="theme" rel="stylesheet">
		<link href="/css/animateBoxs.css" id="theme" rel="stylesheet">
		<script src="/js/modernizr.custom.js"></script>
	</head>
	<body class="fix-header fix-sidebar card-no-border">

		<div id="main-wrapper">

				<div class="container-fluid">
					<div class="row justify-content-center">
					    <div class="col-md-8">
					        <div class="card">
					            <div class="card-header text-center">
									<img src="/images/logo_skincenter.png" class="img-fluid pull-xs-left" alt="..." style="max-width:400px;margin:20px auto">
								</div>
					        </div>
					    </div>
						<style media="screen">
							img{
								height: 340px;
							}
						</style>
						<div class="col-md-12">
							<ul class="grid cs-style-6">
								<li>
									<figure>
										<img src="images/concepcion.jpg" >
										<figcaption>
											<h3>Skin Center Concepción</h3>
											<span>Paicavi #483</span>
											<a href="/goDahsboard/3">Administrar</a>
										</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="images/temuco.jpg" >
										<figcaption>
											<h3>Skin Center Temuco</h3>
											<span>Hochstetter #1069, local 1</span>
											<a href="/goDahsboard/2">Administrar</a>
										</figcaption>
									</figure>
								</li>
								<li>
									<figure>
										<img src="images/skiangeles.jpg" >
										<figcaption>
											<h3>Skin Center Los Angeles</h3>
											<span>Avenida Alemania #258, local 2</span>
											<a href="/goDahsboard/1">Administrar</a>
										</figcaption>
									</figure>
								</li>

							</ul>
						</div>
					</div>
				</div>
		</div>
		<script src="js/toucheffects.js"></script>
	</body>
</html>
