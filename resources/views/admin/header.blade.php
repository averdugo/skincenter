<header class="topbar">
	<nav class="navbar top-navbar navbar-expand-md navbar-light">
		<!-- ============================================================== -->
		<!-- Logo -->
		<!-- ============================================================== -->
		<div class="navbar-header">
			<a class="navbar-brand" href="/">
				<!-- Logo icon --><b>
					<!--You can put here icon as well // <i class="wi wi-sunset"></i> //-->

				</b>
				<!--End Logo icon -->
				<!-- Logo text --><span>
				 <!-- dark Logo text -->
				 <img src="/images/logo_skincenter.png" alt="homepage" class="dark-logo" style="width:220px" />
				 <!-- Light Logo text -->
				 <img src="/assets/images/logo-light-text.png" class="light-logo" alt="homepage" /></span> </a>
		</div>
		<!-- ============================================================== -->
		<!-- End Logo -->
		<!-- ============================================================== -->
		<div class="navbar-collapse" style="background:#569995">
			<!-- ============================================================== -->
			<!-- toggle and nav items -->
			<!-- ============================================================== -->
			<ul class="navbar-nav mr-auto" style="padding-left:0px">
				<li class="nav-item " >
					@if (\Auth::user()->type == 1)
						<a class="nav-link waves-effect waves-dark" href="/home" style="color:white!important;">
							{{$store->name}}
						</a>
					@else
						<a class="nav-link waves-effect waves-dark" href="#" style="color:white!important;">
							{{$store->name}}
						</a>
					@endif

				</li>
			</ul>
			<!-- ============================================================== -->
			<!-- User profile and search -->
			<!-- ============================================================== -->
			<ul class="navbar-nav my-lg-0">
				<!-- ============================================================== -->
				<!-- Profile -->
				<!-- ============================================================== -->
				<li class="nav-item dropdown u-pro">
					<a class="nav-link  waves-effect waves-dark profile-pic" href="/logOut" ><img src="/assets/images/users/1.jpg" alt="user" class="" /> <span class="hidden-md-down" style="color:white">Aldo Verdugo &nbsp;</span> </a>
				</li>
			</ul>
		</div>
	</nav>
</header>
