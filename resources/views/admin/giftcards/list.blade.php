@extends('admin.layout')
@section('content')
	<div id="mainGiftcard" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">GiftCards</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Giftcard</a>
			</div>
			@include('admin.giftcards.crearModal')
			@include('admin.giftcards.editModal')
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>

						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>Monto</th>
										<th>Tipo</th>
										<th>Servicio</th>
										<th>Descripction</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="l in lists"  style="cursor:pointer">
										<td style="width:50px;"><span class="round">@{{l.id}}</span></td>
										<td style="width:100px;">@{{l.name}}</td>
										<td>@{{l.amount}}</td>
										<td>@{{l.typeName}}</td>
										<td>@{{l.service_id}}</td>

										<td v-html="l.description"></td>
										<td><div class="ml-auto align-self-center">
											<a href="#" class="link m-r-10" @click.prenvent="editElement(l)">
												<i class="fa fa-pencil-alt"></i>
											</a>
											<a href="#" class="link m-r-10" @click.prevent="deleteElement(l.id)">
												<i class="fa fa-trash-alt"></i>
											</a>
										</div></td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
@endsection
@section('scripts')
	<script src="/js/giftcards.js"></script>
@endsection
