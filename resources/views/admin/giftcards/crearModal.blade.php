<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear GiftCard</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'giftcards','class'=>'form-material']) !!}
				<input type="hidden" name="store_id" value="{{$store->id}}">
					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('name','Nombre')}}
								{{Form::text('name', null,['class' => 'form-control form-control-line','v-model'=>'data.name'])}}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('type','Tipo')}}
								{{ Form::select('type', [1=>'Productos',2=>'Servicios'], null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.type')) }}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('amount','Cantidad')}}
								{{Form::text('amount', null,['class' => 'form-control form-control-line','v-model'=>'data.amount'])}}
							</div>
						</div>
						<div class="col-md-6" v-if="data.type == 2">
							<div class="form-group">
								{{Form::label('service_id','Servicio')}}
								{{ Form::select('service_id', $services, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.service_id')) }}
							</div>
						</div>
					</div>
					<div class="form-group ">
						{{Form::label('description','Descripccion')}}
						<ckeditor :editor="editor" v-model="data.description" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="submit()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
