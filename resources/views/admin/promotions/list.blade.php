@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
	.easy-autocomplete-container{
		z-index: 2000;
			position: relative;
	}
	.easy-autocomplete {
		width: 100%!important
	}
	</style>
@endsection
@section('content')
	<div id="mainPromotions" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Promociones</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Promocion</a>
			</div>
			
			@if (session('status'))
				<div class="alert alert-success">
					{{ session('status') }}
				</div>
			@endif
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>
						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>Portada</th>
										<th>Nombre</th>
										<th>Precio Antes</th>
										<th>Precio</th>
										<th>Tipo</th>
										<th>Categoria</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($promotions as $p)
									<tr>
										<td><img src="{{asset('/uploads/promotions/'.$p->images)}}" width="150" /></td>
										<td>{{$p->name}}</td>
										<td>{{$p->before_price}}</td>
										<td>{{$p->price}}</td>
										<td>{{$p->typeName}}</td>
										<td>{{$p->category->name ?? ''}}</td>
										<td>
											<a href="#" class="link m-r-10 edit" data-promotionid="{{$p->id}}">
												<i class="fa fa-pencil-alt"></i>
											</a>
											<form action="/promotions/{{ $p->id }}" method="post" style="float:right">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default" type="submit"style="background:transparent;padding: 0 12px" >
                                                    <i class="fa fa-trash-alt"></i>
                                                </button>
                                            </form>
										</td>
									</tr>
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('admin.promotions.crearModal')
	@include('admin.promotions.editModal')
	@include('admin.promotions.editPhoto')

@endsection
@section('scripts')

<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>

<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable({
			'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});
		$("#basics2").easyAutocomplete(options2);

	} );

</script>		
	<script>
		$(function(){
          
		  $('body').on('click','.edit',function(){
			var id = $(this).data('promotionid')
			$.get('/promotions/'+id, function(r){
				console.log(r);
				$('#editModal form').attr('action', "/promotions/"+r.id)
                    $('#editModal input[name=name]').val(r.name)
                    $('#editModal input[name=price]').val(r.price)
					$('#editModal input[name=before_price]').val(r.before_price)
					$('#editModal select[name=type]').val(r.type)
					$('#editModal select[name=store_id]').val(r.store_id)
					$('#editModal select[name=category_id]').val(r.category_id)
                    $('#editModal textarea[name=description]').text(r.description)
                    $('#editModal').modal()
			})
		  })
		  $("body").on('click', '.cancelEdit', function(e){
                e.preventDefault()
				location.reload()
			})
        })
	</script>
@endsection
