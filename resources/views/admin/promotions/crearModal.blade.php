<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Promocion</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('promotions.store')}}" method="POST" enctype="multipart/form-data">

				@csrf
				<input type="hidden" id="token" value="{{csrf_token()}}">
				<div class="modal-body">					
					<input type="hidden" name="store_id" value="{{$store->id}}">
					<input type="hidden" name="category_id" value="{{$categories->id=13}}">

						<div class="form-group row">
							<div class="col-md-6">
								<label for="file">Portada</label>
								<div class="input-group">
										<input type="file" name="file" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<label for="name">Nombre</label>
								<div class="input-group">
										<input type="text" name="name" id="name" class="form-control" placeholder="Nombre">
								</div>
							</div>
						</div>
						<div class="form-group row">
							
							<div class="col-md-6">
								<label for="price">Precio</label>
								<div class="input-group">
										<input type="number" name="price" id="price" class="form-control" placeholder="Precio">
								</div>
							</div>
							<div class="col-md-6">
								<label for="name">Precio Antes</label>
								<div class="input-group">
										<input type="text" name="before_price" id="name" class="form-control" placeholder="Precio Antes">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="category_id">Tipo</label>
								<div class="input-group">
									<select class="form-control" name="type" id="type"  placeholder="type">
										<option>Tipo</option>
											<option value="0">Productos</option>
											<option value="1">Servicios</option>											
									</select>
								</div>
							</div> 
							<div class="col-md-6">
								<label for="store_id">Tienda</label>
								<div class="input-group">
									<select class="form-control" name="store_id" id="store_id"  placeholder="type">
										<option>Tienda</option>
										@foreach ($stores as $key => $value)
											<option value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div> 
						</div>
						<div class="form-group row">         
							<div class="col-md-12">
								<label for="">Descripccion</label>
								<textarea name="description" id="description" class="form-control"></textarea>
							</div>
						</div> 
					<div class=" row col-md-12 text-right">
					  <input type="submit" value="Crear" class="btn btn-danger" style="width:200px">
				  </div>
				</div>
			</form>
		</div>
	</div>
</div>
