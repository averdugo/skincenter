@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<style media="screen">
	.easy-autocomplete-container{
		z-index: 2000;
			position: relative;
	}
	.easy-autocomplete {
		width: 100%!important
	}
	</style>
@endsection
@section('content')
	<div id="mainProducts" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Productos</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createProductModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Producto</a>
			</div>
			@if (session('status'))
			<div class="alert alert-success">
				{{ session('status') }}
			</div>
		@endif
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>
							<div class="ml-auto sr-only">
								<select class="custom-select b-0">
									<option selected="">January</option>
									<option value="1">February</option>
									<option value="2">March</option>
									<option value="3">April</option>
								</select>
							</div>
						</div>
						<div class="table-responsive m-t-20 no-wrap">
							<table id="myTable" class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>Portada</th>
										<th>ID</th>
										<th>Nombre</th>
										<th>Marca</th>
										<th>Formato</th>
										<th>Precio</th>
										<th>Stock</th>
										<th>Categoria</th>
										<th>Acciones</th>
									</tr>
								</thead>
								<tbody>
									@foreach ($products as $pro)
									<tr>
										<td><img src="{{asset('/uploads/products/'.$pro->foreign_id)}}" width="150" /></td>
										<td>{{$pro->intern_id}}</td>
										<td>{{$pro->name}}</td>
										<td>{{$pro->brand}}</td>
										<td>{{$pro->format}}</td>
										<td>{{$pro->price}}</td>
										<td>{{$pro->stock}}</td>
										<td>{{$pro->category->name ?? ''}}</td>
										<td>
											<a href="#" class="link m-r-10 edit" data-productid="{{$pro->id}}">
												<i class="fa fa-pencil-alt"></i>
											</a>
											<form action="/products/{{ $pro->id }}" method="post" style="float:right">
                                                @csrf
                                                {{ method_field('delete') }}
                                                <button class="btn btn-default" type="submit" style="background:transparent;padding: 0 12px">
                                                    <i class="fa fa-trash-alt"></i>
                                                </button>
                                            </form>
										</td>
									</tr>										
									@endforeach
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

	@include('admin.products.crearModal')
	@include('admin.products.editModal')
	@include('admin.products.editPhoto')

@endsection
@section('scripts')
<script src="//cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js" type="text/javascript"></script>
<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>

<script type="text/javascript">
	$(document).ready( function () {
	    $('#myTable').DataTable({
			'language': {
		        "decimal": "",
		        "emptyTable": "No hay información",
		        "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
		        "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
		        "infoFiltered": "(Filtrado de _MAX_ total entradas)",
		        "infoPostFix": "",
		        "thousands": ",",
		        "lengthMenu": "Mostrar _MENU_ Entradas",
		        "loadingRecords": "Cargando...",
		        "processing": "Procesando...",
		        "search": "Buscador:",
		        "zeroRecords": "Sin resultados encontrados",
		        "paginate": {
		            "first": "Primero",
		            "last": "Ultimo",
		            "next": "Siguiente",
		            "previous": "Anterior"
		        }
		    }
		});
		$("#basics2").easyAutocomplete(options2);

	} );

</script>		
<script>
	$(function(){
	  
	  $('body').on('click','.edit',function(){
		var id = $(this).data('productid')
		$.get('/products/'+id, function(re){
			console.log(re);
			$('#editProductModal form').attr('action', "/products/"+re.id)
				if(re.is_salient == 1) {
					$('#editProductModal input[name=is_salient]').prop('checked', true);
				}
				
				
				$('#editProductModal input[name=intern_id]').val(re.intern_id)
				$('#editProductModal input[name=brand]').val(re.brand)
				$('#editProductModal input[name=name]').val(re.name)
				$('#editProductModal input[name=format]').val(re.format)
				$('#editProductModal input[name=price]').val(re.price)
				$('#editProductModal input[name=stock]').val(re.stock)
				$('#editProductModal select[name=store_id]').val(re.store_id)
				$('#editProductModal select[name=category_id]').val(re.category_id)
				$('#editProductModal textarea[name=description]').text(re.description)
				$('#editProductModal').modal()
		})
	  })
	  $("body").on('click', '.cancelEdit', function(e){
			e.preventDefault()
			location.reload()
		})
	})
</script>
@endsection
