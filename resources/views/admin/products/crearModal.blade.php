<div class="modal fade" id="createProductModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Producto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<form action="{{ route('products.store')}}" method="POST" enctype="multipart/form-data">
				@csrf
				<input type="hidden" id="token" value="{{csrf_token()}}">
				<div class="modal-body">					
						<input type="hidden" name="store_id" value="{{$store->id}}">

						<div class="form-group row">
							<div class="col-md-6">
								<label for="file">Portada</label>
								<div class="input-group">
										<input type="file" name="file" class="form-control">
								</div>
							</div>
							<div class="col-md-6">
								<div class="checkbox">
									<label> <input type="checkbox" style="position:relative;left:0;opacity:1;" name="is_salient"> Destacado</label> 
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="intern_id">ID</label>
								<div class="input-group">
										<input type="text" name="intern_id" id="intern_id" class="form-control" >
								</div>
							</div>
							<div class="col-md-6">
								<label for="price">Marca</label>
								<div class="input-group">
										<input type="text" name="brand" id="brand" class="form-control">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="name">Nombre</label>
								<div class="input-group">
										<input type="text" name="name" id="name" class="form-control" >
								</div>
							</div>
							<div class="col-md-6">
								<label for="format">Formato</label>
								<div class="input-group">
										<input type="text" name="format" id="format" class="form-control" >
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="price">Precio</label>
								<div class="input-group">
										<input type="number" name="price" id="price" class="form-control" >
								</div>
							</div>
							<div class="col-md-6">
								<label for="stock">Stock</label>
								<div class="input-group">
										<input type="number" name="stock" id="stock" class="form-control" >
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<label for="category_id">Categoria</label>
								<div class="input-group">
									<select class="form-control" name="category_id" id="category_id"  placeholder="type">
										<option>Tipo</option>
										@foreach ($categories as $key => $value)
											<option  value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div> 
							<div class="col-md-6">
								<label for="store_id">Tienda</label>
								<div class="input-group">
									<select class="form-control" name="store_id" id="store_id"  placeholder="type">
										<option>Tienda</option>
										@foreach ($stores as $key => $value)
											<option  value="{{$key}}">{{$value}}</option>
										@endforeach
									</select>
								</div>
							</div> 
						</div>
						<div class="form-group row">         
							<div class="col-md-12">
								<label for="">Descripccion</label>
								<textarea name="description" id="description" class="form-control"></textarea>
							</div>
						</div> 
					<div class=" row col-md-12 text-right">
					  <input type="submit" value="Guardar" class="btn btn-danger" style="width:200px">
				  </div>
				</div>
			</form>
		</div>
	</div>
</div>
