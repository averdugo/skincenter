<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Crear Servicio</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'servicios','files'=>true,'class'=>'form-material']) !!}


					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('imagenes', 'Portada')}}<br>
							<input type="file" id="file" ref="file" v-on:change="handleFileUpload()"/>
						</div>

					</div>
					<div class="form-group row">
						<div class="col-md-12">
							{{Form::label('name', 'Nombre')}}
							{{Form::text('name', null,['class' => 'form-control form-control-line','v-model'=>'data.name'])}}
						</div>

					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('sessions', 'Sesiones')}}
							{{Form::text('sessions', null,['class' => 'form-control form-control-line','v-model'=>'data.sessions'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('category_id','Categoria')}}
								{{ Form::select('category_id', $categories, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.category_id', '@change'=>'getSubCategory($event)')) }}
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('subcategory_id','Subcategoria')}}
								<select class="form-control form-control-line"  v-model="data.subcategory_id" >
									<option value=""></option>
									<option v-for="s in subcategories" :value="s.id">@{{s.name}}</option>

								</select>
							</div>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('price', 'Precio')}}
								{{Form::number('price', null,['class' => 'form-control form-control-line','v-model'=>'data.price'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('store_id','Tienda')}}
    						{{ Form::select('store_id', $stores, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'data.store_id')) }}
						</div>
						<div class="col-md-6">
							{{Form::label('time', 'Tiempo en Minutos')}}
							{{Form::number('time', null,['class' => 'form-control form-control-line','v-model'=>'data.time'])}}
						</div>
					</div>
					<div class="form-group ">
						{{Form::label('description','Descripccion')}}
						<ckeditor :editor="editor" v-model="data.description" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="submit()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
