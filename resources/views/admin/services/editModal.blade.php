<div class="modal fade" id="editModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Servicio</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => 'productos','files'=>true,'class'=>'form-material']) !!}
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('name', 'Nombre')}}
							{{Form::text('name', null,['class' => 'form-control form-control-line','v-model'=>'element.name'])}}
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('price','Precio')}}
								{{Form::number('price', null,['class' => 'form-control form-control-line', 'v-model'=>'element.price'])}}
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('sessions','Sesiones')}}
  						  	{{Form::text('sessions', null,['class' => 'form-control form-control-line','v-model'=>'element.sessions'])}}
						</div>
						<div class="col-md-6">
							{{Form::label('category_id','Categoria')}}
							<select class="form-control form-control-line"  v-model="element.category_id" @change="onChangeCategorySelect($event)" >
								<option value=""></option>
								<option v-for="c in categories"  v-bind:value="c.id"   >@{{c.name}}</option>
							</select>
						</div>
						<div class="col-md-6">
							<div class="form-group">
								{{Form::label('subcategory_id','Subcategoria')}}
								<select class="form-control form-control-line"  v-model="element.subcategory_id" >
									<option value=""></option>
									<option v-for="s in subcategories" v-bind:value="s.id">@{{s.name}}</option>

								</select>
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('store_id','Tienda')}}
    						{{ Form::select('store_id', $stores, null , array('placeholder'=>'','class' => 'form-control form-control-line ','v-model'=>'element.store_id')) }}
						</div>
						<div class="col-md-6">
							{{Form::label('time', 'Tiempo en Minutos')}}
							{{Form::number('time', null,['class' => 'form-control form-control-line','v-model'=>'element.time'])}}
						</div>
					</div>
					<div class="form-group ">
						{{Form::label('description','Descripccion')}}
						<ckeditor :editor="editor" v-model="element.description" :config="editorConfig"></ckeditor>
					</div>
			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors2" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="update()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
