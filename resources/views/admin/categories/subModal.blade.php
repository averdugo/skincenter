<div class="modal fade" id="subModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Subcategorias de @{{categoryLabel}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		  	<div class="modal-body">
				{!! Form::open(['url' => 'categorias','class'=>'form-inline', 'style' => 'float:right']) !!}

				  	<div class="input-group mb-1 mr-sm-1" style="width: 150px;">
						<input type="text" class="form-control"  placeholder="Subcategoria" v-model="subcat.name">
				  	</div>
				  	<button type="button" class="btn btn-info  mb-1" @click.prenvent="addSub()"><i class="fa fa-plus"></i></button>
				{!! Form::close() !!}

				<h3>Lista </h3>
				<table class="table">
					<thead>
						<td><strong style="color: black;">Nombre</strong></td>
						<td><strong style="color: black;">#</strong></td>
					</thead>
					<tbody>
						<tr v-for="s of subcategories">
							<td>@{{s.name}}</td>
							<td>
								<a href="#" class="link m-r-10" @click.prenvent="deleteSub(s.id, s.category_id)" data-toggle="tooltip" data-placement="top" title="Borrar">
									<i class="fa fa-trash-alt"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>



		  	</div>

		</div>
	</div>
</div>
