<div class="modal fade" id="editPortadaModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Editar Foto</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>

		  	<div class="modal-body">
			  	{!! Form::open(['url' => '','files'=>true,'class'=>'form-material']) !!}

					<div class="">
						<img :src="element.portada" alt="" style="width:100%;max-width:400px">
					</div>
					<div class="form-group row">
						<div class="col-md-6">
							{{Form::label('imagenes', 'Portada')}}<br>
							<input type="file" id="file2" ref="file2" v-on:change="handleFileUpload2()"/>
						</div>

					</div>

			  	{!! Form::close() !!}
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger">@{{error}}</span>
				</div>
				<button type="button" class="btn btn-primary" v-on:click="submitImage()">Guardar</button>
		  	</div>
		</div>
	</div>
</div>
