<div class="modal fade" id="boxModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Box de @{{catLabel}}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		  	<div class="modal-body">
				{!! Form::open(['url' => 'categorias','class'=>'form-inline', 'style' => 'float:right']) !!}
					<select class="form-control mb-1 mr-sm-1" name="" v-model="box.store_id">
						<option value="0">Seleccione Tienda</option>
						@foreach ($stores as $key => $s)
							<option value="{{$s->id}}">{{$s->name}}</option>
						@endforeach
					</select>
				  	<div class="input-group mb-1 mr-sm-1" style="width: 90px;">
						<input type="number" class="form-control"  placeholder="Boxes" v-model="box.box">
				  	</div>
				  	<button type="button" class="btn btn-info  mb-1" @click.prenvent="addBox()"><i class="fa fa-plus"></i></button>
				{!! Form::close() !!}

				<h3>Lista </h3>
				<table class="table">
					<thead>
						<td><strong style="color: black;">Tienda</strong></td>
						<td><strong style="color: black;">Box</strong></td>
						<td><strong style="color: black;">#</strong></td>
					</thead>
					<tbody>
						<tr v-for="i of boxes">
							<td>@{{i.store.name}}</td>
							<td>@{{i.box}}</td>
							<td>
								<a href="#" class="link m-r-10" @click.prenvent="deleteBox(i.id,i.category_id)" data-toggle="tooltip" data-placement="top" title="Borrar">
									<i class="fa fa-trash-alt"></i>
								</a>
							</td>
						</tr>
					</tbody>
				</table>



		  	</div>

		</div>
	</div>
</div>
