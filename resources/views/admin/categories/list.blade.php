@extends('admin.layout')
@section('content')
	<div id="mainCategory" class="">
		<div class="row page-titles">
			<div class="col-md-5 align-self-center">
				<h3 class="text-themecolor">Categorias</h3>
			</div>
			<div class="col-md-7 text-right">
				<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Crear Categoria</a>
			</div>
			@include('admin.categories.crearModal')
			@include('admin.categories.editModal')
			@include('admin.categories.boxModal')
			@include('admin.categories.editPhoto')
			@include('admin.categories.subModal')
		</div>
		<div class="row">
			<div class="col-lg-12">
				<div class="card">
					<div class="card-body">
						<div class="d-flex">
							<div>
								<h5 class="card-title">Lista</h5>
							</div>

						</div>
						<div class="table-responsive m-t-20 ">
							<table class="table vm no-th-brd pro-of-month table-hover">
								<thead>
									<tr>
										<th>#</th>
										<th>Nombre</th>
										<th>Type</th>
										<th>Descripction</th>
										<th>#</th>
									</tr>
								</thead>
								<tbody>
									<tr v-for="l in lists"  style="cursor:pointer">
										<td style="width:100px;">
											<img  @click="editPhoto(l)"  v-bind:src="'/uploads/' + l.img"
											 class="img-fluid pull-xs-left" alt="...">
										</td>
										<td style="width:100px;">@{{l.name}}</td>
										<td>@{{l.typeName}}</td>
										<td v-html="l.description"></td>
										<td>
											<div class="ml-auto align-self-center">
												<a href="#" class="link m-r-10" @click.prenvent="subCategory(l)" data-toggle="tooltip" data-placement="top" title="Subcategorias">
													<i class="fa fa-clipboard-list"></i>
												</a>
												<a href="#" class="link m-r-10" @click.prenvent="showBox(l)" data-toggle="tooltip" data-placement="top" title="Box por Tienda">
													<i class="fa fa-briefcase"></i>
												</a>
												<a href="#" class="link m-r-10" @click.prenvent="editElement(l)" data-toggle="tooltip" data-placement="top" title="Editar">
													<i class="fa fa-pencil-alt"></i>
												</a>
												<a href="#" class="link m-r-10" @click.prevent="deleteElement(l.id)" data-toggle="tooltip" data-placement="top" title="Borrar" >
													<i class="fa fa-trash-alt"></i>
												</a>
											</div>
										</td>
									</tr>

								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>


@endsection
@section('scripts')
	<script src="/js/category.js"></script>

@endsection
