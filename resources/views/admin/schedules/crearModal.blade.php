<div class="modal fade" id="createModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Agendar Tratamiento</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::open(['url' => 'schedules','class'=>'form-material']) !!}
		  		<div class="modal-body">

					<input type="hidden" name="store_id" value="{{$store->id}}">
					<input type="hidden" name="user_id" value="">
					<input type="hidden" name="est_id" value="">
					<div class="form-group row">
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('type','Tipo')}}
								<select class="form-control form-control-line" name="type" @change="typeSelect($event)">
									<option value=""></option>
									<option value="1">Atencion Pagada</option>
									<option value="1">Atencion Por Pagar</option>
									<option value="2">Evaluacion</option>
									<option value="3">Evaluacion y Atencion</option>
								</select>
							</div>
						</div>

						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('client','Cliente')}}
								{{Form::text('client', null,['id'=>'basics','class' => 'form-control form-control-line','style'=>'width:100%'])}}
							</div>
						</div>
						<div class="col-md-6 text-left" v-if="type == 1">
							<div class="form-group">
								{{Form::label('type','Ventas')}}
								<select class="form-control form-control-line" name="saleitem_id" @change="setTratamiento($event)">
									<option value="">Seleccione</option>
									<option v-for="(item, key) in ventas" :value="item.id" :data-time="item.term.time">
										@{{item.term.name}} - Sesiones: @{{item.sessions}} de @{{item.term.sessions}}
									</option>
								</select>
							</div>
						</div>
						<div class="col-md-6 text-left" >
							{{Form::label('category_id','Categoria')}}
    						<select class="form-control form-control-line" name="category_id" @change="getCategory($event)"> >
								<option value="">Seleccione</option>
								@foreach ($categories as $key => $value)
									<option value="{{$key}}">{{$value}}</option>
								@endforeach
    						</select>
						</div>
						<div class="col-md-6 text-left" >
							{{Form::label('service_id','Tratamiento')}}
    						<select class="form-control form-control-line" name="service_id" @change="setTratamiento($event)" >
								<option value="">Seleccione</option>
								<option v-for="(item, key) in services" :value="item.id" :data-time="item.time">@{{item.name}}</option>
    						</select>
						</div>
						
					</div>

					<div class="form-group row">
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('este','Esteticista')}}
								{{Form::text('este', null,['id'=>'basics2','class' => 'form-control form-control-line','style'=>'width:100%'])}}
							</div>
						</div>
						
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Fecha')}}
								<input type="date" name="start_at" value="" class="form-control form-control-line">
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Hora Inicio')}}
								<input type="time" min="09:00" max="22:00"  name="start_at_time" value="" class="form-control form-control-line timepicker" @change="setTime($event)">
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('user_id','Hora Fin')}}
								<input type="time" min="09:00:00" max="22:00:00"  name="end_at_time" v-model="endTime" value="" class="form-control form-control-line">
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('box','Box')}}
								<input type="text" name="box" value="" class="form-control form-control-line">
							</div>
						</div>
					</div>
					<div class="form-group row">
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('status','Estado')}}
								<select name="status" class="form-control form-control-line">
									<option value="">Selecciona</option>
									@foreach ($scheduleStatus as $key => $s)
										<option value="{{$key}}">{{$s}}</option>
									@endforeach
								</select>
							</div>
						</div>
						<div class="col-md-6 text-left">
							<div class="form-group">
								{{Form::label('este','Observaciones')}}
								<textarea name="observation" class="form-control "> </textarea>
							</div>
						</div>
					</div>


		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger"></span>
				</div>
				<button type="submit" class="btn btn-primary" >Guardar</button>
		  	</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>
