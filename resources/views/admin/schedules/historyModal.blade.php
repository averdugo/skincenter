<div class="modal fade" id="historyModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-sm" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="exampleModalLabel">Historial Cliente</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			{!! Form::open(['url' => 'user_history','class'=>'form-material']) !!}
		  		<div class="modal-body">
					<input id="scheduleId" type="hidden" name="f_id" value="">
					<input id="historyClient" type="hidden" name="user_id" value="">
					<input type="hidden" name="type" value="1">
					<div class="form-group row">
						<div class="col-md-12 text-left">
							<label for="">Observacion</label>
							<textarea name="observation" class="form-control"></textarea>
						</div>
					</div>
		  	</div>
		  	<div class="modal-footer">
				<div class="">
					<span v-for="error in errors" class="text-danger"></span>
				</div>
				<button type="submit" class="btn btn-primary" >Guardar</button>
		  	</div>
				{!! Form::close() !!}
		</div>
	</div>
</div>
