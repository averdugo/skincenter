@extends('admin.layout')
@section('css')
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/2.2.7/fullcalendar.min.css"/>
	<link rel="stylesheet" href="/admin/css/easy-autocomplete.min.css"/>
	<link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.css">
	<style media="screen">
		.easy-autocomplete-container{
			z-index: 2000;
			    position: relative;
		}
		.easy-autocomplete {
			width: 100%!important
		}
		label.col-md-12 {
		    text-align: left;
		}
		.fc-time-grid-event>.fc-content{
			    text-align: left;
		}
		.ui-timepicker-standard{
			z-index: 1500!important;
		}
	</style>
@endsection
@section('content')
	@if (session('status'))
		<div class="alert alert-success">
			{{ session('status') }}
		</div>
	@endif
	@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
	<div id="scheduleContent" class="panel panel-primary">
    	<div class="panel-body" >
			<div class="col-md-12 text-right">
				<a href="#" data-toggle="modal" data-target="#createModal" class="btn waves-effect waves-light btn btn-info pull-right hidden-sm-down">Agendar</a>
				@include('admin.schedules.crearModal')
				@include('admin.schedules.verModal')
				@include('admin.schedules.historyModal')
			</div>
          	{!! $calendar_details->calendar() !!}
        </div>
    </div>
@endsection
@section('scripts')
	<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment.min.js"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/fullcalendar/3.9.0/fullcalendar.min.js"></script>
	<script src="/admin/js/jquery.easy-autocomplete.min.js"></script>
	<script src='/js/fullcalendar/es.js'></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/timepicker/1.3.5/jquery.timepicker.min.js"></script>
	<script src="/js/agenda.js"></script>


	{!! $calendar_details->script() !!}
	<script type="text/javascript">
	$('.timepicker').timepicker({
		timeFormat: 'hh:mm',
		interval: 10,
		minTime: '09',
		maxTime: '22:00',

		startTime: '09:00',
		dynamic: false,
		dropdown: true,
		scrollbar: true,
		change: function(time) {
			var element = $(this), text;
		   	var timepicker = element.timepicker();
			console.log(timepicker.format(time))
			ScheduleVue.setTime(timepicker.format(time));
        }
	});


		var dataClients = <?php echo json_encode($arrayClients); ?>;
		var options =  {
			getValue: "name",
			data:dataClients,
			list: {
				onClickEvent: function() {
					var id = $("#basics").getSelectedItemData().id;
					$("input[name='user_id']").val(id);

					if ($("select[name='type']").val() == 1) {
						ScheduleVue.getSaleUser(id);
					}

				},
				showAnimation: {
			      type: "slide"
			    },
			    hideAnimation: {
			      type: "slide"
			  	},
				match: {
					enabled: true
				}
			}
		}

		$("#basics").easyAutocomplete(options);

		var dataEst = <?php echo json_encode($arrayEst); ?>;

		var options2 =  {
			getValue: "name",
			data:dataEst,
			list: {
				onClickEvent: function() {
					var id = $("#basics2").getSelectedItemData().id;
					$("input[name='est_id']").val(id);
				},
				showAnimation: {
			      type: "slide"
			    },
			    hideAnimation: {
			      type: "slide"
			  	},
				match: {
					enabled: true
				}
			}
		}
		var options3 =  {
			getValue: "name",
			data:dataEst,
			list: {
				onClickEvent: function() {
					var id = $("#basics3").getSelectedItemData().id;
					$("#estId").val(id);
				},
				showAnimation: {
			      type: "slide"
			    },
			    hideAnimation: {
			      type: "slide"
			  	},
				match: {
					enabled: true
				}
			}
		}

		$("#basics2").easyAutocomplete(options2);
		$("#basics3").easyAutocomplete(options3);

	</script>
@endsection
