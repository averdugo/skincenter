<div class="modal fade" id="verModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog modal-lg" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title" id="verModalLabel"></h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
		  	<div class="modal-body">
				<div class="row">
					<div class="col-lg-4 col-xlg-4 col-md-5">
						<div class="card">
							<div class="card-body">
								<center class="m-t-10">

									<h4 class="card-title m-t-10" id="clientName"></h4>
									<h6 class="card-title" id="clientRut"></h6>
									<div class="row text-center justify-content-md-center">
										<div class="col-12"><a href="javascript:void(0)" class="link"> <font class="font-medium" id="clientEmail"></font></a></div>
										<div class="col-12"><a href="javascript:void(0)" class="link"> <font class="font-medium" id="clientPhone"></font></a></div>
									</div>
								</center>
							</div>
						</div>
						<h6 class="card-subtitle text-left" style="margin-bottom: 15px;">
							Historial
							<button type="button" class="btn btn-default " name="button" style="float:right" data-toggle="modal" data-target="#historyModal" >+</button>
						</h6>
						<div class="accordion" id="accordion"  style="text-align:left">
						</div>

					</div>

					<div class="col-lg-8 col-xlg-8 col-md-7">
						<div class="card">
							<div class="card-body">
								{!! Form::open(['url' => 'schedules/','id'=>"updateSchedu", 'class'=>'form-material']) !!}
									{{method_field('PUT')}}
									<input type="hidden" id="estId" name="est_id" value="">
									<div class="form-group row">
										<label class="col-md-12">Sessiones</label>
										<div class="col-md-5">
											<input type="text" id="session1" readonly class="form-control form-control-line">
										</div>
										<div class="col-md-2">
											<p>De</p>
										</div>
										<div class="col-md-5">
											<input type="number" id="session2" readonly class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-12">Tratamiento y Box</label>
										<div class="col-md-6">
											<input type="text" id="serviceName" readonly class="form-control form-control-line">
										</div>
										<div class="col-md-6">
											<input type="number" id="serviceBox" readonly class="form-control form-control-line">
										</div>
									</div>
									<div class="form-group row">
										<label for="example-email" class="col-md-12">Esteticista</label>
										<div class="col-md-12">
											{{Form::text('este', null,['id'=>'basics3','class' => 'form-control form-control-line','style'=>'width:100%'])}}
										</div>
									</div>
									<div class="form-group row">
										<label for="example-email" class="col-md-12">Fecha y Hora</label>
										<div class="col-md-6">
											<input type="date" id="fechaServices" readonly class="form-control form-control-line" >
										</div>
										<div class="col-md-3">
											<input type="time"  id="startServices" class="form-control form-control-line">
										</div>
										<div class="col-md-3">
											<input type="time" id="endServices" class="form-control form-control-line" >
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-12">Estado</label>
										<div class="col-sm-12">
											<select id="serviceStatus" class="form-control form-control-line" name="status">
												@foreach ($scheduleStatus as $key => $s)
													<option value="{{$key}}">{{$s}}</option>
												@endforeach
											</select>
										</div>
									</div>
									<div class="form-group row">
										<label class="col-md-12">Observaciones</label>
										<div class="col-md-12">
											<textarea id="serviceText" name="observation" class="form-control form-control-line"></textarea>
										</div>
									</div>
									<div class="form-group">
										<div class="col-sm-12">
											<button class="btn btn-success">Actualizar</button>
										</div>
									</div>
								{!! Form::close() !!}
							</div>
						</div>
					</div>
					<!-- Column -->
				</div>
		  	</div>
		</div>
	</div>
</div>
