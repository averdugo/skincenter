<aside class="left-sidebar">
	<!-- Sidebar scroll-->
	<div class="scroll-sidebar">
		<!-- Sidebar navigation-->
		<nav class="sidebar-nav">
			<ul id="sidebarnav">
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type != 1 ?'sr-only':''}}" href="/administrador/dashboard" aria-expanded="false">
						<i class="fas fa-tachometer-alt"></i>
						<span class="hide-menu">Dashboard</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type != 1 ?'sr-only':''}}" href="/administrador/flujo_caja" aria-expanded="false">
						<i class="fas fa-book"></i>
						<span class="hide-menu">Flujo de Caja</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark" href="/administrador/productos" aria-expanded="false">
						<i class="fas fa-shopping-bag {{\Auth::user()->type == 3 ?'sr-only':''}}"></i>
						<span class="hide-menu">Productos</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/categorias" aria-expanded="false">
						<i class="fas fa-tags"></i>
						<span class="hide-menu">Categorias</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/servicios" aria-expanded="false">
						<i class="fa fa-table"></i>
						<span class="hide-menu">Servicios</span></a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/promociones" aria-expanded="false">
						<i class="fas fa-bullhorn"></i>
						<span class="hide-menu">Promociones</span></a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/giftcards" aria-expanded="false">
						<i class="fas fa-gift"></i>
						<span class="hide-menu">GiftCards</span></a>
				</li>
				<li>
					<a class="waves-effect waves-dark " href="/administrador/agenda" aria-expanded="false">
						<i class="fas fa-calendar-alt"></i>
						<span class="hide-menu">Agenda</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/ventas" aria-expanded="false">
						<i class="fas fa-dollar-sign"></i>
						<span class="hide-menu">Ventas</span>
					</a>
				</li>
				<li>
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/clientes" aria-expanded="false">
						<i class="fas fa-user-friends"></i>
						<span class="hide-menu">Clientes</span>
					</a>
				</li>
				<li class="">
					<a class="waves-effect waves-dark {{\Auth::user()->type == 3 ?'sr-only':''}}" href="/administrador/usuarios" aria-expanded="false">
						<i class="fas fa-users"></i>
						<span class="hide-menu">Usuarios</span>
					</a>
				</li>
			</ul>
			<div class="text-center m-t-30 sr-only">
				<a href="https://wrappixel.com/templates/adminwrap/" class="btn waves-effect waves-light btn-info hidden-md-down"> Upgrade to Pro</a>
			</div>
		</nav>
		<!-- End Sidebar navigation -->
	</div>
	<!-- End Sidebar scroll-->
</aside>
