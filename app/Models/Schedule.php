<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use Calendar;

class Schedule extends Model
{
    protected $fillable = ['end_at','user_id', 'store_id', 'service_id',"start_at", 'category_id', 'box', 'est_id','status','observation', 'saleitem_id','type'];
	const TYPE = [1=>'Normal', 2 =>'Evaluacion'];

	const STATUS = [
		0=>'Por confirmar',
		1=>'Confirmada',
		2 =>'No se presento'
	];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}

	public function client()
	{
		return $this->belongsTo('App\User','user_id');
	}

	public function agent()
	{
		return $this->belongsTo('App\User','est_id');
	}
	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
	public function saleItem()
	{
		return $this->belongsTo('App\Models\SaleItems','saleitem_id');
	}
	public function service()
	{
		return $this->belongsTo('App\Models\Services','service_id');
	}

	public static function eventList($schedules)
	{
		$event_list = [];
		foreach ($schedules as $key => $s) {
			$color = 'red';
			if ($s->type == 1) {
				switch ($s->category_id) {
					case 4:
						$color = 'black';
						break;
					case 6:
						$color = 'blue';
						break;
					case 7:
						$color = 'red';
						break;
					case 8:
						$color = 'green';
						break;
					case 9:
						$color = 'brown';
						break;
				}
			}else{
				$color = 'red';
			}


			$start = new Carbon( $s->start_at,'America/Santiago');
			$end = new Carbon( $s->end_at,'America/Santiago');

			$event_list[$key] = Calendar::event(
	                $s->service->name.' '.$s->client->name,
	                false,
	                $start,
	                $end,
					$s->id,
					['color' => $color]
	            );
		}

		return $event_list;

	}

	public static function calendarDetails($event_list)
	{
		return Calendar::addEvents($event_list)
				->setOptions([
                    'eventLimit'     => 4,
                ])->setCallbacks([
			        'eventClick' => "function(event) {

						$.get('/schedules/'+event.id,function(r){
							console.log(r);
							$('#session1').val(r.sale_item.sessions)
							$('#session2').val(r.service.sessions)
							$('#scheduleId').val(r.id)
							$('#historyClient').val(r.client.id)
							$('#accordion').html('')
							$.each(r.client.histories, function (key, val) {
								$('#accordion').append(`<div class='card' style='    margin-bottom: 10px;'>
									<div class='card-header' id='headingOne' style='    padding: 0;'>
										<h5 class='mb-0'>
											<button class='btn btn-link' type='button' data-toggle='collapse' data-target='#collapseOne`+val.id+`' aria-expanded='false' aria-controls='collapseOne' style='color:black;font-size: 0.75rem;'>
												`+val.created_at+` `+val.schedule.service.name+`
											</button>
										</h5>
									</div>
									<div id='collapseOne`+val.id+`' class='collapse ' aria-labelledby='headingOne' data-parent='#accordionExample'>
										<div class='card-body'>`+val.observation+`</div>
									</div>
								</div>`);
							});
							var type = r.type == 1 ? 'Agenda':'Evaluacion'
							$('#verModalLabel').text(type)
							$('#estId').val(r.agent.id)
							$('#updateSchedu').attr('action','/schedules/'+r.id);
							$('#clientName').text(r.client.name)
							$('#clientRut').text(r.client.rut)
							$('#clientEmail').text(r.client.email)
							$('#clientPhone').text(r.client.phone)
							$('#serviceName').val(r.service.name)
							$('#serviceBox').val(r.box)
							$('#basics3').val(r.agent.name)
							var fecha = moment(r.start_at).format('YYYY-MM-DD');
							var start = moment(r.start_at).format('HH:mm')
							var end = moment(r.end_at).format('HH:mm')
							$('#fechaServices').val(fecha)
							$('#startServices').val(start)
							$('#endServices').val(end)
							$('#serviceText').html(r.observation)
							$('#serviceStatus').val(r.status)
							$('#verModal').modal();

						})

					}"
			    ]);
	}


}
