<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
	const PRODUCT_TYPE = 1;
    const SERVICE_TYPE = 2;

	public $typeArray = [
		1=>'Productos', 2=>'Servicios', 3=>'Usuarios', 4=>'Promociones'
	];

}
