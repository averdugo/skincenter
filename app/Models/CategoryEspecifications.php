<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CategoryEspecifications extends Model
{

	protected $fillable = ['category_id', 'store_id','box'];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}
}
