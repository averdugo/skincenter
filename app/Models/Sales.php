<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Sales extends Model
{

	protected $fillable = [
		'user_id',
		'store_id',
		'total',
		'status',
		'transbank_token',
		'createdBy',
		'type'
	];

	const STATUS= [
		9 =>'Pendiente',
		0 => 'Creado/Pendiente',
		1 =>'Pagado',
		2 =>'Rechazado'
	];

	const TYPES = [
		1 => 'WebPay',
		2 => 'Efectivo',
		3 => 'Transbank POS',
		4 => 'Cheque',
		5 => 'Flow'
	];

	public function user()
	{
		return $this->belongsTo('App\User','user_id');
	}

	public function items()
	{
		return $this->hasMany('App\Models\SaleItems','sale_id');
	}

	public function getStatuslabelAttribute()
	{
		return Self::STATUS[$this->status];
	}

	public function getTypelabelAttribute()
	{
		return Self::TYPES($this->type);
	}
}
