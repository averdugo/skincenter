<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\Models\{Store, Images};

class Products extends Model
{
     protected $fillable = ['name', 'description', 'price', 'stock', 'format', 'category_id', 'store_id', 'brand', 'intern_id', 'is_salient'];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}

	public function images()
	{
		return $this->hasOne('App\Models\Image','foreign_id')->where('type', 1);
	}

	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
}
