<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Timetable extends Model
{
    public static function getHoursArray($categoria, $day)
	{
		$times = Self::where('category_id', $categoria)->where('start_day','<=',$day)->where('end_day','>=',$day)->get();

		$data = [];

		foreach ($times as $key => $t) {
			$startTime = explode(':',$t->start_time);
			$endTime = explode(':',$t->end_time);
			$startTime[0] = intval($startTime[0]);
			$startTime[1] = intval($startTime[1]);
			$endTime[0] = intval($endTime[0]);
			$endTime[1] = intval($endTime[1]);

			for ($i=$startTime[0]; $i <= $endTime[0]; $i++) {
				array_push($data, $i);
			}
		}
		return $data;
	}
}
