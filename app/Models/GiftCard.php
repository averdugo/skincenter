<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class GiftCard extends Model
{
	protected $fillable = ['name','amount', 'type','service_id','store_id','description'];

	public $typeArray = [
		1=>'Productos',2=>'Servicios'
	];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}

	public function services()
	{
		return $this->belongsTo('App\Models\Services');
	}

}
