<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SaleItems extends Model
{
	protected $fillable = [
		'f_id',
		'type',
		'qty',
		'price',
		'sale_id',
		'sessions',
		'status'
	];

	const STATUS = [
		0 => 'Pendiente',
		1 => 'Terminado',
		2 => 'Entregado'
	];

	const TYPE = [
		1 => 'Producto',
		2 => 'Servicio',
		3 => 'Promocion',
		4 => 'GiftCard',
	];

	public function sale()
	{
		return $this->belongsTo('App\Models\Sales');
	}
	public function item()
	{
		switch ($this->type) {
        	case 1:
        		return $this->belongsTo('App\Models\Products','f_id');
        		break;
			case 2:
        		return $this->belongsTo('App\Models\Services','f_id');
        		break;
			case 3:
        		return $this->belongsTo('App\Models\Promotion','f_id');
        		break;
			case 4:
        		return $this->belongsTo('App\Models\GiftCard','f_id');
        		break;
        }
	}

	public function getStatuslabelAttribute() {
		return Self::STATUS[$this->status];
	}
	public function getTypelabelAttribute() {
		return Self::TYPE[$this->type];
	}


}
