<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Promotion extends Model
{
    protected $fillable = ['name', 'description', 'price', 'type', 'category_id','store_id', 'before_price' ];

	public $typeArray = [
		0=>'Productos',1=>'Servicios'
	];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}
	public function images()
	{
		return $this->hasOne('App\Models\Image','foreign_id')->where('type', 4);
	}
	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}
}
