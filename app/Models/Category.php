<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['name', 'description','type', 'img', 'store_id'];

	public $typeArray = [
		1=>'Productos',2=>'Servicios'
	];

	public function boxes()
	{
		return $this->hasMany('App\Models\CategoryEspecifications');
	}

	public function subCat()
	{
		return $this->hasMany('App\Models\SubCategory');
	}

}
