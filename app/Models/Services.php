<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Services extends Model
{
	protected $fillable = ['name', 'description', 'price', 'sessions', 'category_id','store_id', 'time', 'subcategory_id' ];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}
	public function images()
	{
		return $this->hasOne('App\Models\Image','foreign_id')->where('type', 2);
	}
	public function category()
	{
		return $this->belongsTo('App\Models\Category');
	}

	public function subcategory()
	{
		return $this->belongsTo('App\SubCategory', 'subcategory_id');
	}
}
