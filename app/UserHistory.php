<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserHistory extends Model
{
    protected $fillable = ['user_id', 'type', 'f_id', 'observation'];

	public function schedule()
	{
		return $this->hasOne('App\Models\Schedule', 'id', 'f_id');
	}
}
