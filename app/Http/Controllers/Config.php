<?php

namespace App\Http\Controllers;
use Illuminate\Database\Eloquent\ModelNotFoundException;



 class Config {

	static function get($name, $store) {
		
		switch ($store) {
			case '1':
				$COMMERCE_CONFIG = array(
					"APIKEY" => "6FEDFFF4-D827-46B7-9153-9A7A37284L6D", // Registre aquí su apiKey
					"SECRETKEY" => "697b899687d59380d7826d8e61f68cf3f2594d7e", // Registre aquí su secretKey
					"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
					"BASEURL" => "https://skincenter.cl/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
			 	);
				break;
			case '2':
				$COMMERCE_CONFIG = array(
					"APIKEY" => "5618AF9A-277A-450F-9F6E-3D8L2734AF88", // Registre aquí su apiKey
					"SECRETKEY" => "220f1bc40ae5316ba29bc8e21049622449b49925", // Registre aquí su secretKey
					"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
					"BASEURL" => "https://skincenter.cl/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
			 	);
				break;
			case '3':
				$COMMERCE_CONFIG = array(
					"APIKEY" => "2BFDDA11-37BC-431E-8159-603E59LA2B91", // Registre aquí su apiKey
					"SECRETKEY" => "ced63b6b2d319642e03d0407ed62b1ff31a44287", // Registre aquí su secretKey
					"APIURL" => "https://www.flow.cl/api", // Producción EndPoint o Sandbox EndPoint
					"BASEURL" => "https://skincenter.cl/apiFlow" //Registre aquí la URL base en su página donde instalará el cliente
			 	);
				break;

		}

		if(!isset($COMMERCE_CONFIG[$name])) {
			throw new ModelNotFoundException("The configuration element thas not exist", 1);
		}
		return $COMMERCE_CONFIG[$name];
	}
 }
