<?php

namespace App\Http\Controllers;

use App\PatientFile;
use Illuminate\Http\Request;
use App\{User, ClientService};
use App\Models\{Image, Category, Store, Services};

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return User::with('store')
        ->where('store_id', \Session::get('store_id'))
        ->orderBy('id','DESC')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $store = Store::find(\Session::get('store_id'));
        $stores = Store::pluck('name', 'id');
        $categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
        $arrayEst = User::getWorkersAutocomplete();
        return view('admin.users.crearModalClientes',['user'=> new User(),'arrayEst'=>$arrayEst,'categories'=>$categories,'store'=>$store,'stores'=>$stores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

    $user = User::create($request->all());
		$user->password = bcrypt($request->password);
    $user->save();
    
		if ($request->file('file')) {
			$file = $request->file('file');
			$destinationPath = 'uploads/users';
			$hashImage = str_random(30);
			$ext = $file->getClientOriginalExtension();
			$file->move($destinationPath,"$hashImage.$ext");
			$image = new Image;
			$image->file = "$hashImage.$ext";
			$image->type = 3;
			$image->foreign_id = $user->id;
			$image->save();

    }
    
    return back()->with('status', 'Usuario Creado');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return User::with('images','store')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        return User::with('images','store')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		$user = User::find($id)->update($request->all());
		return back()->with('status', 'Usuario Actualizado');
    }

    public function update2(Request $request, $id)
    {
		$user = User::find($id)->update($request->all());
    return back()->with('status', 'Usuario Actualizado');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		User::find($id)->delete();
    return back()->with('status', 'Usuario Eliminado');
    }

    public function estetic($store_id)
    {
      return User::where('type', 3)->where('store_id',$store_id)->get();
    }
    
    public function ventaT()
    {

      return back()->with('status', 'Venta Creada');
    }

}
