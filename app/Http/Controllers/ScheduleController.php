<?php

namespace App\Http\Controllers;

use App\Models\{Schedule, Services, SaleItems};
use Illuminate\Http\Request;
use Auth;
use Carbon\Carbon;

class ScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    public function getHoursByDate($date, $itemId){
        dd($date);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
			'user_id'=>'required',
			'store_id'=>'required',
			'start_at'=>'required',
			'service_id'=>'required',
			'category_id'=>'required',
			'est_id'=>'required',
			'type'=>'required'
		]);

		$data = $request->all();
		if ($request->saleitem_id) {
			$item = SaleItems::find($request->saleitem_id);
			$data['service_id'] = $item->f_id;
			$data['category_id'] = $item->item->category_id;
			$data['sale_id'] = $item->sale_id;
		}

		$schedule = Schedule::create($data);
		$schedule->start_at = $request->start_at .' '.$request->start_at_time;
		$service = Services::find($data['service_id']);
		$start = new Carbon( $schedule->start_at,'America/Santiago');
		$end = $start->addMinutes($service->time);
		$schedule->end_at = $end->format('Y-m-d H:i');
		$schedule->save();

		if ($data['type'] == 1 && $request->saleitem_id) {
			$item = SaleItems::find($request->saleitem_id);
			$item->sessions = $item->sessions + 1;
			$item->save();
		}

	 	return redirect('/administrador/agenda')->with('status', 'Agendamiento Creada!');
    }

	public function clientStore(Request $request)
	{
		$schedule = new Schedule;
		$schedule->user_id = Auth::user()->id;
		$schedule->store_id = $request->tratamiento['store_id'];
		$schedule->service_id = $request->tratamiento['id'];
		$schedule->est_id = $request->agendar['esteticistaSelect'];
		$schedule->start_at = $request->agendar['date'].' '.$request->agendar['hour'].':00';
		$endHour = $request->agendar['hour']+1;
		$schedule->end_at = $request->agendar['date'].' '.$endHour.':00';
		$schedule->category_id = $request->tratamiento['category_id'];
		$schedule->status = 1;
		$schedule->save();
		return $schedule;
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Schedule::with('store','client','client.histories','client.histories.schedule','client.histories.schedule.service','agent','category','service','saleItem')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Schedule $schedule)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
		Schedule::find($id)->update( $request->all() );
		return redirect('/administrador/agenda')->with('status', 'Agendamiento Actualizado!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Schedule  $schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Schedule $schedule)
    {
        //
    }
}
