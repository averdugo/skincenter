<?php

namespace App\Http\Controllers;

use App\CategoryEspecifications;
use Illuminate\Http\Request;

class CategoryEspecificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\CategoryEspecifications  $categoryEspecifications
     * @return \Illuminate\Http\Response
     */
    public function show(CategoryEspecifications $categoryEspecifications)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\CategoryEspecifications  $categoryEspecifications
     * @return \Illuminate\Http\Response
     */
    public function edit(CategoryEspecifications $categoryEspecifications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\CategoryEspecifications  $categoryEspecifications
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, CategoryEspecifications $categoryEspecifications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\CategoryEspecifications  $categoryEspecifications
     * @return \Illuminate\Http\Response
     */
    public function destroy(CategoryEspecifications $categoryEspecifications)
    {
        //
    }
}
