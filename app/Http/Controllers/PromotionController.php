<?php

namespace App\Http\Controllers;

use App\Models\{Promotion, Image};
use Illuminate\Http\Request;

class PromotionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return Promotion::with('store','category')
						->where('store_id', \Session::get('store_id'))
						->orderBy('id','DESC')
						->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $store = Store::find(\Session::get('store_id'));
      $stores = Store::pluck('name', 'id');
      $categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
      return view('admin.promotions.crearModal', ['promotion'=> new Promotion(), 'categories'=>$categories,'store'=>$store,'stores'=>$stores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    $promotion = Promotion::create($request->all());

		if ($request->file('file')) {
			$file = $request->file('file');
			$destinationPath = 'uploads/promotions';
			$hashImage = str_random(30);
			$ext = $file->getClientOriginalExtension();
			$file->move($destinationPath,"$hashImage.$ext");
			$image = new Image;
			$image->file = "$hashImage.$ext";
			$image->type = 4;
			$image->foreign_id = $promotion->id;
			$image->save();

		}

    
    return back()->with('status', 'Promocion Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Promotion::with('images','store','category')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Promotion::with('images','store','category')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
      $promotion = Promotion::find($id)->update($request->all());
      
      if ($request->file('file')) {
        $file = $request->file('file');
        $destinationPath = 'uploads/promotions';
        $hashImage = str_random(30);
        $ext = $file->getClientOriginalExtension();
        $file->move($destinationPath,"$hashImage.$ext");
        $image = new Image;
        $image->file = "$hashImage.$ext";
        $image->type = 4;
        $image->foreign_id = $id;
        $image->save();
      }
  
      return back()->with('status', 'Promocion Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Promotion  $promotion
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		  Promotion::find($id)->delete();
      return back()->with('status', 'Promocion Eliminada');
    }
}
