<?php

namespace App\Http\Controllers;

use App\Models\Image;
use Illuminate\Http\Request;

class ImagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        dd('adsas');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$file = $request->file('file');
		$hashImage = str_random(30);
		$ext = $file->getClientOriginalExtension();

		switch ($request->type) {
			case 1:
				$destinationPath = 'uploads/products';
				break;
			case 2:
				$destinationPath = 'uploads/services';
				break;
			case 3:
				$destinationPath = 'uploads/users';
				break;
			case 4:
				$destinationPath = 'uploads/promotions';
				break;
		}

		$file->move($destinationPath,"$hashImage.$ext");
		
		if ($request->id) {
			$image = Image::find( $request->id );
			$image->file = "$hashImage.$ext";
			$image->save();
		}else{
			$image = new Image;
			$image->file = "$hashImage.$ext";
			$image->type = $request->type;
			$image->foreign_id = $request->foreign_id;
			$image->save();
		}
		return;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function show(Store $store)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function edit(Store $store)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Store $store)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Store  $store
     * @return \Illuminate\Http\Response
     */
    public function destroy(Store $store)
    {
        //
    }
}
