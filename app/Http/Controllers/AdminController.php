<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\{Store, Category, Schedule, Services, Sales, Products , SalesItems, Promotion};
use Carbon\Carbon;
use Calendar;
use App\{User, CashFlow, FlowItems, FlowSubItems, Expenses};
use Auth;

class AdminController extends Controller
{
	public function __construct()
	{
	    $this->middleware('auth');
	}

	public function index()
	{
		$store = Store::find(\Session::get('store_id'));
		return view('admin.dashboard',['store'=>$store]);
	}

	public function products()
	{
		$products =  Products::where('store_id', \Session::get('store_id'))->orderBy('id','DESC')->get();
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type', 1)->pluck('name', 'id');
		
		return view('admin.products.list',['stores'=>$stores,'store'=>$store,'categories'=>$categories,'products'=>$products]);
	}

	public function categories()
	{
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::all();
		$storesList = Store::pluck('name', 'id');
		return view('admin.categories.list',['store'=>$store, 'storesList'=>$storesList,'stores'=>$stores]);
	}

	public function services()
	{
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		return view('admin.services.list',['categories'=>$categories,'store'=>$store,'stores'=>$stores]);
	}

	public function giftcards()
	{
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$services = Services::where('store_id',\Session::get('store_id'))->pluck('name', 'id');
		return view('admin.giftcards.list',['services'=>$services,'store'=>$store,'stores'=>$stores]);
	}

	public function promociones()
	{
		$promotions =  Promotion::where('store_id', \Session::get('store_id'))->orderBy('id','DESC')->get();
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		return view('admin.promotions.list',['categories'=>$categories,'store'=>$store,'stores'=>$stores,'promotions' => $promotions]);
	}


	public function users()
	{
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		return view('admin.users.listU',['store'=>$store,'stores'=>$stores,'categories'=>$categories]);
	}
	public function clients()
	{
		$clients = User::with('store')->where('store_id', \Session::get('store_id'))->orderBy('id','DESC')->get();
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$arrayEst = User::getWorkersAutocomplete();
		return view('admin.users.list',['arrayEst'=>$arrayEst,'categories'=>$categories,'store'=>$store,'stores'=>$stores,'clients'=>$clients]);
	}
	public function schedule()
	{
		if (Auth::user()->type == 3) {
			$schedules = Schedule::where('store_id',\Session::get('store_id'))->where('est_id',Auth::user()->id)->get();
		}else{
			$schedules = Schedule::where('store_id',\Session::get('store_id'))->get();
		}

		$scheduleStatus = Schedule::STATUS;

		$store = Store::find(\Session::get('store_id'));
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$arrayClients = User::getClientsAutocomplete();
		$arrayEst = User::getWorkersAutocomplete();

		$event_list = Schedule::eventList($schedules);

		$calendar_details = Schedule::calendarDetails($event_list);

		return view('admin.schedules.calendar',['scheduleStatus'=>$scheduleStatus,'arrayEst'=>$arrayEst,'arrayClients'=>$arrayClients,'categories'=>$categories, 'calendar_details'=>$calendar_details,'store'=>$store,]);
	}

	public function login()
	{
		return view('auth.login');
	}

	public function cashFlow()
	{
		$monthArray = ['','Enero','Febrero', 'Marzo','Abril', 'Mayo', 'Junio', 'Julio', 'Agosto', 'Septiembre', 'Octubre', 'Noviembre', 'Diciembre'];
		$store = Store::find(\Session::get('store_id'));
		$stores = Store::pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$itemsEgresos = CashFlow::where('type', 2)->get();
		$itemsIngresos = CashFlow::where('type', 1)->get();
		$catProducts = Category::where('type', 1)->get();
		$catServices = Category::where('type', 2)->get();

		$dt = Carbon::now();
		$month = $dt->month;
		$month = $dt->month < 10 ? '0'.$dt->month  : $dt->month ;
		$dateI = $dt->year .'-'.$month.'-01';
		$dateF = $dt->year .'-'.$month.'-'. $dt->daysInMonth;

		$flowItems = FlowItems::all();

		$workers = User::where('type', 3)->where('store_id', $store->id)->get();
		$remuTotal = 0;
		foreach ($workers as $key => $w) {
			$bonus = $r = 0;
			foreach ($w->sales as $k => $s) {
				$bonus = $bonus + $s->total;

			}
			$bonus = $bonus * 0.05;
			$workers[$key]['bonus'] = $bonus;
			$r = $bonus + $w->salary;
			$remuTotal = $remuTotal + $r;
		}

		foreach ($flowItems as $key => $f) {
			$totalAmount = 0;
			if ($f->cashflow_id == 1) {
				$sales = Sales::where('store_id', $store->id)->where('created_at','>=',$dateI)->where('created_at','<=',$dateF)->get();
				foreach ($sales as $p => $s) {
					foreach ($s->items as $c => $i) {
						if ($i->type == $f->id) {
							$totalAmount = $totalAmount + $i->price;
						}
					}
				}
			}else{
				if (count($f->subItems) > 0) {
					foreach ($f->subItems as $k => $sub) {
						$expenses = Expenses::where('store_id', $store->id)->where('f_id', $sub->id)->where('type', 2)->where('make_at','>=',$dateI)->where('make_at','<=',$dateF)->get();
						foreach ($expenses as $key => $e) {
							$totalAmount = $e->amount + $totalAmount;
						}
					}
				}else{
					$expenses = Expenses::where('store_id', $store->id)->where('f_id', $f->id)->where('type', 1)->where('make_at','>=',$dateI)->where('make_at','<=',$dateF)->get();
					foreach ($expenses as $k => $e) {
						$totalAmount = $e->amount + $totalAmount;
					}

				}
			}

			$flowItems[$key]['totalAmount'] = $totalAmount;
		}
		return view('admin.cashFlow.index',['remuTotal'=>$remuTotal,'workers'=>$workers,'catProducts'=>$catProducts, 'catServices'=>$catServices,'flowItems'=>$flowItems, 'mes'=>$monthArray[$dt->month], 'dt'=>$dt,'itemsIngresos'=>$itemsIngresos,'itemsEgresos'=>$itemsEgresos,'store'=>$store,'stores'=>$stores,'categories'=>$categories]);
	}
	public function cashFlowEgresosP(Request $request, $type, $id)
	{
		$store = Store::find(\Session::get('store_id'));
		$dates = explode(' - ', $request->daterange);
		$egresos = Expenses::where('type', $type)->where('f_id',$id)->where('store_id',$store->id)->where('make_at','>=',$dates[0])->where('make_at','<=',$dates[1])->get();

		if ($type == 1) {
			$item = FlowItems::find($id);
		}else{
			$item = FlowSubItems::find($id);
		}

		return view('admin.cashFlow.cashFlowEgresos', ['item'=>$item,'type'=>$type, 'f_id'=> $id, 'egresos'=>$egresos,'store'=>$store]);

	}

	public function cashFlowEgresos($type, $id)
	{
		$store = Store::find(\Session::get('store_id'));
		$egresos = Expenses::where('type', $type)->where('f_id',$id)->where('store_id',$store->id)->get();

		if ($type == 1) {
			$item = FlowItems::find($id);
		}else{
			$item = FlowSubItems::find($id);
		}

		return view('admin.cashFlow.cashFlowEgresos', ['item'=>$item,'type'=>$type, 'f_id'=> $id, 'egresos'=>$egresos,'store'=>$store]);

	}

	public function cashFlowIngresosP(Request $request, $type, $id)
	{
		$store = Store::find(\Session::get('store_id'));
		$dates = explode(' - ', $request->daterange);

		$sales = Sales::where('store_id', $store->id)->where('created_at','>=',$dates[0])->where('created_at','<=',$dates[1])->get();
		$items = $data = [];
		$title = '';
		foreach ($sales as $key => $s) {
			foreach ($s->items as $k => $i) {
				if ($i->type == $type) {
					$i->user = $s->user;
					array_push($items, $i);
				}
			}
		}
		if ($type == 1) {
			$catName = Category::find($id)->name;
			$title = 'Producto ' .$catName;
			foreach ($items as $key => $v) {
				if ($v->item->category_id != $id) {
					unset($items[$key]);
				}
			}
		}elseif ($type == 2) {
			$catName = Category::find($id)->name;
			$title = 'Tratamiento '.$catName;
			foreach ($items as $key => $v) {
				if ($v->item->category_id != $id) {
					unset($items[$key]);
				}
			}
		}elseif ($type == 3) {
			$title = 'Promociones';
		}elseif ($type == 4) {
			$title = 'GiftCard';
		}
		return view('admin.cashFlow.cashFlowIncomes', ['type'=>$type,'title'=>$title, 'items'=>$items, 'f_id'=> $id,'store'=>$store]);

	}

	public function cashFlowIngresos($type, $id)
	{
		$store = Store::find(\Session::get('store_id'));
		$sales = Sales::where('store_id', $store->id)->get();
		$items = $data = [];
		$title = '';
		foreach ($sales as $key => $s) {
			foreach ($s->items as $k => $i) {
				if ($i->type == $type) {
					$i->user = $s->user;
					array_push($items, $i);
				}
			}
		}
		if ($type == 1) {
			$catName = Category::find($id)->name;
			$title = 'Producto ' .$catName;
			foreach ($items as $key => $v) {
				if ($v->item->category_id != $id) {
					unset($items[$key]);
				}
			}
		}elseif ($type == 2) {
			$catName = Category::find($id)->name;
			$title = 'Tratamiento '.$catName;
			foreach ($items as $key => $v) {
				if ($v->item->category_id != $id) {
					unset($items[$key]);
				}
			}
		}elseif ($type == 3) {
			$title = 'Promociones';
		}elseif ($type == 4) {
			$title = 'GiftCard';
		}
		return view('admin.cashFlow.cashFlowIncomes', ['type'=>$type,'title'=>$title, 'items'=>$items, 'f_id'=> $id,'store'=>$store]);
	}

	public function sales()
	{
		if (! \Session::get('store_id')) {
 		   return redirect('/home');
 	   }
		$store = Store::find(\Session::get('store_id'));
		$sales = Sales::where('store_id',$store->id)->get();
		$arrayClients = User::getClientsAutocomplete();
		$arrayEst = User::getWorkersAutocomplete();
		$scheduleStatus = Schedule::STATUS;

		$status = Sales::STATUS;
		$types = Sales::TYPES;


		return view('admin.sales.list', ['scheduleStatus'=>$scheduleStatus, 'arrayEst'=>$arrayEst,'status'=>$status,
		'types'=>$types,'arrayClients'=>$arrayClients,'sales'=>$sales,'store'=>$store]);
	}
	public function history_sales($id)
	{
		dd($id);
		if (! \Session::get('store_id')) {
 		   return redirect('/home');
 	   	}

		$itemS = salesItems;

		$store = Store::find(\Session::get('store_id'));
		$sales = Sales::where('store_id',$store->id)->get();
		$arrayClients = User::getClientsAutocomplete();
		$arrayEst = User::getWorkersAutocomplete();
		$scheduleStatus = Schedule::STATUS;

		$status = Sales::STATUS;
		$types = Sales::TYPES;


		return view('admin.sales.list', ['scheduleStatus'=>$scheduleStatus, 'arrayEst'=>$arrayEst,'status'=>$status,
'types'=>$types,'arrayClients'=>$arrayClients,'sales'=>$sales,'store'=>$store]);
	}

	public function goDahsboard(Request $request, $store_id)
	{
		\Session::put('store_id',$store_id);
		if (Auth::user()->type == 1) {
			return redirect('administrador/dashboard');
		}else{
			return redirect('administrador/agenda');
		}
	}

	public function getItemsByType($type, $id)
	{
		if ($type == 1) {
			return Products::where('category_id',$id)->get();
		}else{
			return Services::where('category_id',$id)->get();
		}
	}
}
