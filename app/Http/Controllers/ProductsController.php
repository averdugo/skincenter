<?php

namespace App\Http\Controllers;

use App\Models\{Products, Image};
use Illuminate\Http\Request;
use Illuminate\Http\Response;

class ProductsController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return Products::with('store')
        ->where('store_id', \Session::get('store_id'))
        ->orderBy('id','DESC')
        ->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      $store = Store::find(\Session::get('store_id'));
      $stores = Store::pluck('name', 'id');
      $categories = Category::where('store_id', $store->id)->where('type', 1)->pluck('name', 'id');
      return view('admin.products.crearModal', ['products'=> new Products(), 'categories'=>$categories,'store'=>$store,'stores'=>$stores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      
      $data = $request->all();
        
      $data['is_salient'] = ( isset($data['is_salient']) && $data['is_salient'] == 'on') ? 1 : 0;
      
      
      $product = Products::create($data);

      if ($request->file('file')) {
        $file = $request->file('file');
        $destinationPath = 'uploads/products';
        $hashImage = str_random(30);
        $ext = $file->getClientOriginalExtension();
        $file->move($destinationPath,"$hashImage.$ext");
        $image = new Image;
        $image->file = "$hashImage.$ext";
        $image->type = 1;
        $image->foreign_id = $product->id;
        $image->save();
      
      }

      return back()->with('status', 'Producto Creado');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function show( $id)
    {
        return Products::with('images','store','category')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        return Products::with('images','store','category')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data = $request->all();
        
        $data['is_salient'] = ( isset($data['is_salient']) && $data['is_salient'] == 'on') ? 1 : 0;
        
        $product = Products::find($id)->update($data);
 

        if ($request->hasFile('file')) {
          $file = $request->file('file');
          $ext = time().'.'.$file->getClientOriginalExtension();
          $destinationPath = public_path('uploads/products');
          $file->move($destinationPath, "$hashImage.$ext");
          $this->save();

      }

    return back()->with('status', 'Producto Actualizado');
    }
  

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Products  $products
     * @return \Illuminate\Http\Response
     */
    public function destroy(Products $products,$id)
    {
		  Products::find($id)->delete();
      return back()->with('status', 'Producto Eliminado');
    }
}
