<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$user = Auth::user();
		if ($user->type == 1) {
			return view('admin.home');

		}elseif ($user->type == 2 || $user->type == 3) {
			 return redirect("/goDahsboard/{$user->store_id}");
		}else{
			return redirect('/');
		}

    }
}
