<?php

namespace App\Http\Controllers;

use App\Models\{Category,Store, CategoryEspecifications, Services};
use App\SubCategory;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$s = Store::find(\Session::get('store_id'));
		
        return Category::with(['boxes','boxes.store'])->where('store_id', $s->id)->orderBy('id','DESC')->get();
    }
	public function categoriesServices()
    {
        return Category::with(['boxes','boxes.store'])->where('type',2)->orderBy('id','DESC')->get();
    }

	public function addBox(Request $request)
	{
		$request->merge((array) json_decode($request->get('data')));
		CategoryEspecifications::create($request->all());
		return CategoryEspecifications::with(['store'])->where('category_id', $request->category_id)->get();
	}

	public function deleteBox($id,$category)
	{
		CategoryEspecifications::find($id)->delete();
		return CategoryEspecifications::with(['store'])->where('category_id', $category)->get();
	}

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge((array) json_decode($request->get('data')));
		$this->validate($request, [
			'name' => 'required'
		]);

		$category = Category::create($request->all());
		if ($request->file('file')) {
			$file = $request->file('file');
			$destinationPath = 'uploads';
			$hashImage = str_random(30);
			$ext = $file->getClientOriginalExtension();
			$file->move($destinationPath,"$hashImage.$ext");
			$category->img = "$hashImage.$ext";
			$category->save();

		}

		return;
    }

	public function getSubCategories($id)
	{
		return SubCategory::where('category_id', $id)->get();
	}

	public function categoryImgApp(Request $request, $id)
	{
		$request->merge((array) json_decode($request->get('data')));
		if ($request->file('file')) {

			$file = $request->file('file');
			$destinationPath = 'uploads';
			$hashImage = str_random(30);
			$ext = $file->getClientOriginalExtension();
			$file->move($destinationPath,"$hashImage.$ext");

			$category= Category::find($id);
			$category->img = "$hashImage.$ext";
			$category->save();

		}
		return;
	}

	public function addSub(Request $request)
	{
		$request->merge((array) json_decode($request->get('data')));
		SubCategory::create($request->all());
		return SubCategory::where('category_id', $request->category_id)->get();

	}

	public function deleteSub($id, $category_id)
	{
		SubCategory::find($id)->delete();
		return SubCategory::where('category_id', $category_id)->get();
	}

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit( $id)
    {
        return Category::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,  $id)
    {
		$this->validate($request, [
			'name' => 'required'
		]);

		Category::find($id)->update( $request->all() );

		return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Services::where('category_id',$id)->delete();
        SubCategory::where('category_id',$id)->delete();
		Category::find($id)->delete();
        return;
    }

	public function getCategoryByType($type)
	{
        return Category::where('type',$type)->get();
	}
}
