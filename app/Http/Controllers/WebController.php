<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Auth;
use App\Models\{Products, GiftCard,  Promotion, Services, Store, Category, Timetable, Schedule};
use Carbon\Carbon;
use Mail;
use App\{Team, SubCategory};
use PDF;



class WebController extends Controller
{
    public function index()
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}


		$products = Products::with('category')->where('store_id', $store->id)->where('is_salient',true)->get();
		
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		$promotions = Promotion::where('store_id', $store->id)->get();
		$giftCards = GiftCard::where('store_id', $store->id)->get();
		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->get();



		$storeId = \Session::get('store_id');
		$user = Auth::user();

		$teams = Team::where('store_id',$storeId)->get();

		return view('web.index', ['teams'=>$teams, 'user'=>$user, 'giftCards'=>$giftCards,'categoryProducts'=>$categoryProducts,
			's'=>1, 'products'=>$products, 'promotions'=>$promotions, 'store'=>$store,'categoriesServices' => $categoriesServices,
			'categories'=>$categories
		]);
	}

	public function userSection()
	{

		$user = Auth::user();
		if(!$user){
			return redirect('login');
		}

		$categories = Category::where('store_id', \Session::get('store_id'))->where('type',2)->get();
		$categoryProducts = Category::where('store_id', \Session::get('store_id'))->where('type',1)->get();
		$categoriesServices = Category::where('type',2)->pluck('name', 'id');
		$store = Store::find(\Session::get('store_id'));
		$user = Auth::user();
		$buyItems = [];
		$buyProducts = [];
		foreach ($user->mysales as $k => $s) {
			foreach ($s->items as $key => $i) {
				$temp = array(
					"id"	=> $i->id ?? '',
					"type"	=> $i->type_label ?? '', 
					"name" 	=> $i->item->name ?? '',
					"qty" 	=> $i->qty ?? '',
					"price" => $i->price ?? '',
					"fecha" => $i->created_at ?? '',
					"sessions" => 0, //$i->sessions . ' de '. $i->item->sessions ?? '',
					"status"=> $i->status_label ?? ''
				);
				if($i->type==1){
					array_push($buyProducts, $temp);
				} else {
					array_push($buyItems, $temp);
				}
			}
		}
		return view('web.profile',compact('user','buyItems','categories','store', 'categoriesServices','categoryProducts','buyProducts'));
	}

	public function goStore($store_id)
	{
		\Session::put('store_id',$store_id);
		return redirect('/');
	}

	public function productos($id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}
		if ($id==0){
			$products = Products::where('store_id', $store->id)->get();
		} else {
			$products = Products::where('store_id', $store->id)->where('category_id', $id)->get();
		}
		$user = Auth::user();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type', 2)->get();

		return view('web.products', ['s'=>0,'products'=>$products,'categories'=>$categories,'store'=>$store,'categoryProducts'=>$categoryProducts,'categoriesServices'=>$categoriesServices,'user'=>$user]);
	
	}
	public function pDetails($id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$user = Auth::user();
		$type = 1;
		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$product = Products::find($id);
		$categories = Category::where('store_id', $store->id)->where('type', 1)->get();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		$products = Products::with('category')->where('store_id', $store->id)->where('is_salient',true)->get();

		return view('web.detailsProducts', ['s'=>0,'type'=>$type,'products'=>$products, 'product'=>$product,'categories'=>$categories, 'categoriesServices'=>$categoriesServices, 'store'=>$store,'categoryProducts'=>$categoryProducts, 'id'=>$id, 'user'=>$user]);
	}

	public function proDetails($id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$user = Auth::user();
		$type = 3;
		$promotion = Promotion::find($id);
		$categories = Category::where('store_id', $store->id)->where('type', 1)->get();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		return view('web.detailsPromotions', ['s'=>0, 'type'=>$type, 'promotion'=>$promotion, 'categories'=>$categories,'store'=>$store, 'categoriesServices'=>$categoriesServices,'categoryProducts'=>$categoryProducts, 'user'=>$user]);
	}

	public function gDetails($id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}
		$type = 4;
		$product = GiftCard::find($id);
		$categories = Category::where('store_id', $store->id)->where('type', 1)->get();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		return view('web.details', ['s'=>0,'type'=>$type,'product'=>$product,'categories'=>$categories,'store'=>$store,'categoryProducts'=>$categoryProducts]);
	}

	public function sDetails($name , $id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}
		$type = 2;

		$product = Services::find($id);

		$categories = Category::where('store_id', $store->id)->where('type', 1)->get();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();
		return view('web.details', ['s'=>0,'type'=>$type,'product'=>$product,'categories'=>$categories,'store'=>$store,'categoryProducts'=>$categoryProducts]);
	}

	public function tratamientos($id)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$c = Category::find($id);
		$subCategories = SubCategory::where('category_id', $id)->get();
		$services = Services::where('category_id',$id)->where('store_id', $store->id)->get();
		$categories = Category::where('store_id', $store->id)->where('type', 2)->get();
		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$user = Auth::user();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();

		return view('web.tratamientos', ['subCategories'=>$subCategories,'c'=>$c, 'user'=>$user, 's'=>0,'services'=>$services,'categories'=>$categories,'store'=>$store, 'categoriesServices'=>$categoriesServices,'categoryProducts'=>$categoryProducts]);
	}

	public function subtratamientos($cat,$sub)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$c = Category::find($cat);
		$subC = SubCategory::find($sub);
		$subCategories = SubCategory::where('category_id', $cat)->get();
		$services = Services::where('category_id',$cat)->where('subcategory_id', $sub)->where('store_id', $store->id)->get();
		$categories = Category::where('store_id', $store->id)->where('type', 2)->get();
		$categoriesServices = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');
		$user = Auth::user();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();

		return view('web.subtratamientos', ['subC'=>$subC,'subCategories'=>$subCategories,'c'=>$c, 'user'=>$user, 's'=>0,'services'=>$services,'categories'=>$categories,'store'=>$store, 'categoriesServices'=>$categoriesServices,'categoryProducts'=>$categoryProducts]);
	}

	public function servicios($name)
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$services = Services::get();
		$categories = Category::where('store_id', $store->id)->where('type', 2)->get();
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)->get();

		return view('web.services', ['s'=>0,'name'=>$name,'services'=>$services,'categories'=>$categories,'store'=>$store,'categoryProducts'=>$categoryProducts]);
	}
	public function contacto()
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}
		$categoryProducts = Category::where('store_id', $store->id)->where('type',1)>pluck('name', 'id');
		$categories = Category::where('store_id', $store->id)->where('type',2)->pluck('name', 'id');

		return view('web.contact', ['s'=>0,'categories'=>$categories,'store'=>$store,'categoryProducts'=>$categoryProducts]);
	}
	public function promociones()
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$promotions = Promotion::get();

		return view('web.promotions', ['s'=>0,'promotions'=>$promotions,'store'=>$store]);
	}

	public function giftcards()
	{
		if (! \Session::get('store_id')) {
			return view('web.home');
		}else{
			$store = Store::find(\Session::get('store_id'));
		}

		$giftcards = GiftCard::get();

		return view('web.giftcards', ['s'=>0,'giftcards'=>$giftcards,'store'=>$store]);
	}

	public function getService($id)
	{
		return Services::where('category_id', $id)->pluck('name','id');
	}
	public function getService2($id)
	{
		return Services::where('category_id', $id)->get();
	}

	public function reservationRequest(Request $request)
	{
		$data = (array) json_decode($request->get('reservation'));
		$data['service'] = Services::find($data['service'])->name;
		$data['category'] = Category::find($data['category'])->name;
		
		
		
		switch (\Session::get('store_id')) {
			case 1:
				$data['store_email'] = 'losangeles@skincenter.cl';
				break;
			case 3:
				$data['store_email'] = 'concepcion@skincenter.cl';
				break;
			case 2:
				$data['store_email'] = 'temuco@skincenter.cl';
				break;
		}

		Mail::send('mails.reservation', $data, function($message) use($data) {

		    $message->to($data['store_email'], 'Solicitud de Evaluacion')

					->replyTo($data['correo'], $data['name'])

					->subject('Solicitud de Evaluacion')

					->from($data['correo'], $data['name']);
		});
	}

	public function contact(Request $request){
		$data = (array) json_decode($request->get('reservation'));
		switch (\Session::get('store_id')) {
			case 1:
				$data['store_email'] = 'losangeles@skincenter.cl';
				break;
			case 3:
				$data['store_email'] = 'concepcion@skincenter.cl';
				break;
			case 2:
				$data['store_email'] = 'temuco@skincenter.cl';
				break;
		}
		Mail::send('mails.contacto', $data, function($message) use($data) {

		    $message->to($data['store_email'], '')

					->replyTo($data['email'], $data['name'])

					->subject('Contacto de la web')

					->from($data['email'], $data['name']);
		});

	}



	public function getDoctorsByDate(Request $request)
	{
		$dt = Carbon::parse($request->fecha);

		if ($dt->dayOfWeek == 0) {
			return '0';
		}

		$times = Timetable::where('category_id', $request->tratamiento['category_id'])->where('start_day','<=',$dt->dayOfWeek )->where('end_day','>=',$dt->dayOfWeek )->get();

		$hoursArray = Timetable::getHoursArray($request->tratamiento['category_id'], $dt->dayOfWeek);

		$schedules = Schedule::where('store_id', $request->tratamiento['store_id'])->where('est_id',$request->este_id)->where('start_at','LIKE',$request->fecha. '%')->get();

		foreach ($schedules as $key => $s) {
			$dt = Carbon::parse($s->start_at);
			foreach (array_keys($hoursArray, $dt->hour) as $key)
		    {
		        unset($hoursArray[$key]);
		    }
		}
		return $hoursArray;
	}
	public function consentimientoDownload(){
		

			$pdf = PDF::loadView('/consentimientos/cstoCavitacion');
		
	
		
		return $pdf->download(); 
	}

}
