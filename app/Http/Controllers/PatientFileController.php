<?php

namespace App\Http\Controllers;

use App\PatientFile;
use App\Models\Store;
use App\Models\Category;
use Illuminate\Http\Request;

class PatientFileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
        $patientFile = PatientFile::orderBy('id','asc')->get();
        
        $store = Store::find(\Session::get('store_id'));
        $stores = Store::pluck('name', 'id');

        return view('admin.users.serviciosClientes.fichasClientes.list', ['patientFile'=>$patientFile,'store'=>$store,'stores'=>$stores]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $store = Store::find(\Session::get('store_id'));
        $stores = Store::pluck('name', 'id');

        return view('admin.users.serviciosClientes.fichasClientes.crear', ['patientFile'=> new PatientFile(), 'store'=>$store,'stores'=>$stores]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $patientFile = PatientFile::create($request->all());

        return back()->with('status', 'Ficha Creada');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return PatientFile::with('store')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patientFile = PatientFile::find($id);
        return PatientFile::with('store')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $patientFile = PatientFile::findOrFail($id)->update($request->all());

        return back()->with('status', 'Ficha Actualizada');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        PatientFile::find($id)->delete();
        return back()->with('status', 'Ficha Eliminada');
    }
}
