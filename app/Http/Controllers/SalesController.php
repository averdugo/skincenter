<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\{Store, Sales, SaleItems, Services};
use Illuminate\Http\Request;

class SalesController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

	public function saleitem($id)
	{
		$item = SaleItems::with('sale')->find($id);
		$item->time = $item->item->time;
		return response()->json($item);
	}

	public function getSalesbyUser($id)
	{
		$sales = Sales::with('items')->where('user_id', $id)->where('store_id',\Session::get('store_id'))->get();
		$data = [];
		foreach ($sales as $key => $s) {
			foreach ($s->items as $k => $i) {
				if ($i->status == 1 || $i->type != 2 ) {
					unset($s->items[$k]);
				}else{
					$s->items[$k]->term = $i->item;
				}
			}
			$data = $s->items;
		}

		return response()->json($data);
	}

	public function saleItemStatus(Request $request)
	{
		$item = SaleItems::find($request->id);
		$item->status = $request->status;
		$item->save();
		return 1;
	}

	public function salesItems($id)
	{
		$items = SaleItems::where('sale_id', $id)->get();
		$data = '';

		foreach ($items as $key => $i) {

			if ($i->type == 2) {
				$c = $i->item->sessions - $i->sessions;
				$status = $i->sessions . ' de '. $i->item->sessions;
				if ($c != 0) {
					$button = "<button data-id='{$i->id}' class='btn btn-default btnAgendaItem' >Agendar Sesión</button>";
				}else{
					$button = Auth::user()->type != 4 ? "<div class='btn-group'>
							  <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							    Estado <span class='caret'></span>
							  </button>
							  <ul class='dropdown-menu' style='text-align:center;'>
							    <li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='0'>Pendiente</a></li>
							    <li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='1'>Terminado</a></li>
							    <li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='2'>Entregado</a></li>
							  </ul>
							</div>": "Tratamiento Finalizado";
				}

			}else{
				$status = SaleItems::STATUS[$i->status];
				$button = Auth::user()->type != 4 ? "<div class='btn-group'>
						  <button type='button' class='btn btn-default dropdown-toggle' data-toggle='dropdown' aria-haspopup='true' aria-expanded='false'>
							Estado <span class='caret'></span>
						  </button>
						  <ul class='dropdown-menu' style='text-align:center;'>
							<li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='0'>Pendiente</a></li>
							<li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='1'>Terminado</a></li>
							<li style'border-bottom: 1px solid #eee;'><a href='#' class='statusBtn' data-id='{$i->id}' data-status='2'>Entregado</a></li>
						  </ul>
						</div>": '';
			}

			$data .= sprintf('
				<tr>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td>%s</td>
					<td width: 10%;>
						%s
					</td>
				<tr>
			', $i->item->name, $i->qty, $i->item->price, $status, $button  );
		}
		return $data;
	}


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $store = Store::find(\Session::get('store_id'));

		$sale = Sales::create([
			'user_id' => $request->user_id,
			'store_id' => $store->id,
			'total' => $request->total,
			'status' => $request->status,
			'type' => $request->salestype,

		]);

		if ($request->type == 2) {
			$s = Services::find($request->f_id);
			$sessions = $s->sessions;
		}else{
			$sessions = 0;
		}

		$saleItem = SaleItems::create([
			'f_id' => $request->f_id,
			'type' => $request->type,
			'qty' => $request->qty,
			'price' => $request->total,
			'sale_id' => $sale->id,
			'sessions'=>$sessions,
			'status'=> 0
		]);

		return back()->with('status', 'Venta Creada!');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function show(Sales $sales)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function edit(Sales $sales)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Sales $sales)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Sales  $sales
     * @return \Illuminate\Http\Response
     */
    public function destroy(Sales $sales)
    {
        //
    }
}
