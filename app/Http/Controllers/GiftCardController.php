<?php

namespace App\Http\Controllers;

use App\Models\GiftCard;
use Illuminate\Http\Request;

class GiftCardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return GiftCard::with('store')->where('store_id', \Session::get('store_id'))->orderBy('id','DESC')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->merge((array) json_decode($request->get('data')));

		$this->validate($request, [
			'name' => 'required',
			'amount' => 'required',
			'store_id' => 'required',
			'type' => 'required'
		]);

		$giftcard = GiftCard::create($request->all());

		return;
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\GiftCard  $giftCard
     * @return \Illuminate\Http\Response
     */
    public function show(GiftCard $giftCard)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\GiftCard  $giftCard
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return GiftCard::find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\GiftCard  $giftCard
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, GiftCard $giftCard, $id)
    {
		$this->validate($request, [
			'name' => 'required',
			'amount' => 'required',
			'store_id' => 'required',
			'type' => 'required'
		]);

		GiftCard::find($id)->update( $request->all() );

		return;
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\GiftCard  $giftCard
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		GiftCard::find($id)->delete();
        return;
    }
}
