<?php

namespace App\Http\Controllers;

use App\Models\{Services,Image};
use App\{ClientService};
use Illuminate\Http\Request;

class ServicesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		return Services::with('store','category')->where('store_id', \Session::get('store_id'))->orderBy('id','DESC')->get();

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$request->merge((array) json_decode($request->get('data')));

		$this->validate($request, [
			'name' => 'required',
			'description' => 'required',
			'price' => 'required',
			'sessions' => 'required',
			'category_id' => 'required',
			'store_id' => 'required',
		]);

		$services = Services::create($request->all());

		if ($request->file('file')) {
			$file = $request->file('file');
			$destinationPath = 'uploads/services';
			$hashImage = str_random(30);
			$ext = $file->getClientOriginalExtension();
			$file->move($destinationPath,"$hashImage.$ext");
			$image = new Image;
			$image->file = "$hashImage.$ext";
			$image->type = 2;
			$image->foreign_id = $services->id;
			$image->save();
			return 'Ok';
		}else{
			return 'Ok';
		}
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return Services::with('images','store','category', 'subcategory')->find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return Services::with('images','store','category', 'subcategory')->find($id);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request,$id)
    {
		$this->validate($request, [
			'name' => 'required',
			'description' => 'required',
			'price' => 'required',
			'sessions' => 'required',
			'category_id' => 'required',
			'store_id' => 'required',
		]);

		$services = Services::find($id)->update( $request->all() );
		return 'Ok';
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Services  $services
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
		Services::find($id)->delete();
        return;
    }

    public function findBySubcategory($id){
      return Services::where('subcategory_id', $id)->get();
    }

    public function clientService(Request $request) {
      ClientService::create($request->all());
      return back()->with('status', 'Tratamiento Asignado');
    }
}
