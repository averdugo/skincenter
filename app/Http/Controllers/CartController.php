<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Cart;
use App\Models\{Sales, SaleItems, Services, Products, Images, GiftCard, Promotion, Category, Store};
use Auth;
use Freshwork\Transbank\CertificationBagFactory;
use Freshwork\Transbank\TransbankServiceFactory;
use Freshwork\Transbank\RedirectorHelper;
use App\Http\Controllers\FlowApi;


class CartController extends Controller
{
	public function goFlow(Request $request)
	{
		$cart = Cart::getContent()->toArray();

		if (empty($cart)) {
			return back();
		}
		$sale = new Sales;
		$sale->user_id = Auth::user()->id;
		$sale->store_id = \Session::get('store_id');
		$sale->total = Cart::getTotal();
		$sale->status = 9;
		$sale->save();

		foreach ($cart as $key => $v) {
			$item = new SaleItems;
			$item->f_id = $v['id'];
			$item->type = $v['attributes']['type'];
			$item->qty = $v['quantity'];
			$item->price = $v['price'];
			$item->sale_id = $sale->id;
			$item->save();
		}

		$optional = array(
			"rut" => Auth::user()->rut,
			"id" => $sale->id,
		);

		$optional = json_encode($optional);

		$params = array(
			"commerceOrder" => $sale->id,
			"subject" => "Pago en Skincenter",
			"currency" => "CLP",
			"amount" => Cart::getTotal(),
			"email" => Auth::user()->email,
			"paymentMethod" => 9,
			"urlConfirmation" =>  "https://skincenter.cl/flowConfirm",
			"urlReturn" => "https://skincenter.cl/flowResult",
			"optional" => $optional
		);

		$serviceName = "payment/create";
		$flowApi = new FlowApi(\Session::get('store_id'));
		$response = $flowApi->send($serviceName, $params,"POST");
		$redirect = $response["url"] . "?token=" . $response["token"];

		return redirect($redirect);

	}
	public function flowConfirm(Request $request)
	{
		$categories = Category::where('store_id', 1)->where('type',2)->get();
		try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
			);
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi(\Session::get('store_id'));
			$response = $flowApi->send($serviceName, $params, "GET");
			$sale = Sales::find($response['optional']['id']);
			if ($response['status'] == 2) {
				$sale->status = 1;
				$sale->save();
				Cart::clear();
				return redirect('/perfil')->with('status', 'Venta Aprobada!, revisa tu lista de compras y agenda tus servicios');
			}else{
				$sale = Sales::find($response['optional']['id']);
				$sale->status = 2;
				$sale->save();
				return back()->with('status', 'Venta Rechazada!');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
	}
	public function flowResult(Request $request)
	{
$categories = Category::where('store_id', 1)->where('type',2)->get();
		try {
			if(!isset($_POST["token"])) {
				throw new Exception("No se recibio el token", 1);
			}
			$token = filter_input(INPUT_POST, 'token');
			$params = array(
				"token" => $request->token
			);
			$serviceName = "payment/getStatus";
			$flowApi = new FlowApi(\Session::get('store_id'));
			$response = $flowApi->send($serviceName, $params, "GET");
//dd($response);
			$sale = Sales::find($response['optional']['id']);
			if ($response['status'] == 2) {
				$sale->status = 1;
				$sale->save();
				Cart::clear();
				return redirect('/perfil')->with('status', 'Venta Aprobada!, revisa tu lista de compras y agenda tus servicios');
			}else{
				$sale = Sales::find($response['optional']['id']);
				$sale->status = 2;
				$sale->save();
				return back()->with('status', 'Venta Rechazada!');
			}
		} catch (Exception $e) {
			echo "Error: " . $e->getCode() . " - " . $e->getMessage();
		}
	}

	public function goTransbank()
	{
		$cart = Cart::getContent()->toArray();

		if (empty($cart)) {
			return back();
		}
		$sale = new Sales;
		$sale->user_id = Auth::user()->id;
		$sale->store_id = \Session::get('store_id');
		$sale->total = Cart::getTotal();
		$sale->save();

		foreach ($cart as $key => $v) {
			$item = new SaleItems;
			$item->f_id = $v['id'];
			$item->type = $v['attributes']['type'];
			$item->qty = $v['quantity'];
			$item->price = $v['price'];
			$item->sale_id = $sale->id;
			$item->save();
		}

		$bag = CertificationBagFactory::integrationWebpayNormal();

		$plus = TransbankServiceFactory::normal($bag);

		$plus->addTransactionDetail($sale->total, $sale->id);

		$response = $plus->initTransaction('http://skincenter.cl/goTransbankResponse', 'http://skincenter.cl/goTransbankFinish');

		$sale->transbank_token = $response->token;
		$sale->save();

		echo RedirectorHelper::redirectHTML($response->url, $response->token);
	}

	public function goTransbankResponse(Request $request)
	{
		$bag = CertificationBagFactory::integrationWebpayNormal();

		$plus = TransbankServiceFactory::normal($bag);

		$response = $plus->getTransactionResult();
		$collect = collect($response);
		\Session::push('transbank', $collect);
		$sale = Sales::find($response->buyOrder);
		$sale->status = $response->detailOutput->responseCode;
		$sale->save();
		// Comprueba que el pago se haya efectuado correctamente
		$plus->acknowledgeTransaction();

		// Redirecciona al cliente a Webpay para recibir el Voucher
		return RedirectorHelper::redirectBackNormal($response->urlRedirection);
	}

	public function goTransbankFinish(Request $request)
	{
		Cart::clear();
		$transbankResponse = \Session::get('transbank');
		//dd($transbankResponse[0]);
		$sale = Sales::where('transbank_token', $request->token_ws)->first();
		$items = SaleItems::where('sale_id', $sale->id)->get();
		$forImg = ['', 'products','services','promotions'];
		$categoriesServices = Category::where('type',2)->pluck('name', 'id');
		$store = Store::find(\Session::get('store_id'));
		$user = Auth::user();
		return view('web.agendamiento',['forImg'=>$forImg, 'items'=>$items, 'transbankResponse'=>$transbankResponse[0], 'user'=>$user, 'store'=>$store, 'sale'=>$sale, 'categoriesServices'=>$categoriesServices]);
	}

	public function clear()
	{
		Cart::clear();
		return back();
	}

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

		return count(Cart::getContent());
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return ['list'=>Cart::getContent()->toJson(),'total'=>Cart::getTotal()];
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
		$typeName = "";
        switch ($request->type) {
        	case 1:
        		$add = Products::find($request->id);
				$typeName = 'Producto';
        		break;
			case 2:
        		$add = Services::find($request->id);
				$typeName = 'Tratamiento';
        		break;
			case 3:
        		$add = Promotion::find($request->id);
				$typeName = 'Promocion';
        		break;
			case 4:
        		$add = GiftCard::find($request->id);
				$typeName = 'GiftCard';
        		break;
        }

		$price = str_replace('.','',str_replace('$','',$add->price));

		Cart::add(array(
			'id'=>$add->id,
			'name'=> $add->name,
			'price'=> $price,
			'quantity'=>$request->qty,
			'attributes'=>array(
				'n_type' => $typeName,
				'type' => $request->type
			)
		));

		return count(Cart::getContent());
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Cart::clear();
    }




}
