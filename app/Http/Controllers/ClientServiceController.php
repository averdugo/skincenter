<?php

namespace App\Http\Controllers;

use App\User;
use App\Models\Store;
use App\ClientService;
use App\Models\Category;
use App\Models\Services;
use Illuminate\Http\Request;

class ClientServiceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $store = Store::find(\Session::get('store_id'));
        $stores = Store::pluck('name', 'id');

        $categories = Category::where('type', 2)->get();

        $user = User::find($id);
        $clientServices = ClientService::where('user_id', $id)->get();
        $tratamientos = [];
        
        #TODO revisar  fecha de inicio de tratamiento para ventas

        /*foreach ($user->mysales as $key => $s) {
        foreach ($s->items as $k => $i) {
            if($i->type == 2) {
            array_push($tratamientos, $i->item);
            }
        }
        }*/

        foreach ($clientServices as $key => $c) {
        $service = Services::find($c->service_id);
        $service->clientservice_id = $c->id; 
        $service->appointment = date('d-m-Y', strtotime($c->appointment));
        array_push($tratamientos, $service);
        }
        
        return view('admin.users.serviciosClientes.list', ['categories'=>$categories,'tratamientos' => $tratamientos, 'user' => $user,'store'=>$store,'stores'=>$stores]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return ClientService::find($id);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        ClientService::find($id)->delete();
        return back()->with('status', 'Tratamiento Eliminado');
    }

    public function subirConsentimiento(Request $request){

        $clientService = ClientService::find($request->clientservice_id);
        if($request->file('consent_path')){

            $file = $request->file('consent_path')->store('public');
            $img = str_replace('public/', '', $file);
            $clientService->consent_path = $img;
            $clientService->save();
            
        }


        return back()->with('status', 'Archivo guardado');
   
    }
}
