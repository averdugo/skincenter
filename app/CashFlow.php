<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CashFlow extends Model
{
    public function items()
	{
		return $this->hasMany('App\FlowItems','cashflow_id');
	}
}
