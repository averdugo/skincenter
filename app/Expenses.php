<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Expenses extends Model
{
	protected $fillable = ['type', 'f_id', 'store_id', 'amount', 'make_at', 'img', 'observation'];

    public function categoryItem()
	{
		if ($this->type == 1) {
			return $this->belongsTo('App\FlowItems', 'f_id');
		}else{
			return $this->belongsTo('App\FlowSubItems', 'f_id');
		}
	}
}
