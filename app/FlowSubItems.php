<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlowSubItems extends Model
{
    public function item()
	{
		return $this->belongsTo('App\FlowItems', 'cashflowitems_id');
	}
}
