<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FlowItems extends Model
{
    public function subItems()
	{
		return $this->hasMany('App\FlowSubItems','cashflowitems_id');
	}

	public function flow()
	{
		return $this->belongsTo('App\CashFlow','cashflow_id');
	}

	
}
