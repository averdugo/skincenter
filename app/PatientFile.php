<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PatientFile extends Model
{
    protected $table = 'patient_file';

    protected $fillable = [
        'user_id', 'clientservice_id', 'created_by', 'est_id', 'date', 'session', 'treatment_area', 'wavelength', 'j_cm2', 'wide_pulse', 'size_mm', 'observations' 
    ];

    public function clientservice()
	{
		return $this->belongsTo('App\ClientService');
    }
    
    public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}
}
