<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ClientService extends Model
{
    protected $table = 'clientservices';
    

    protected $fillable = ['user_id', 'status','store_id','service_id', 'schedule_id', 'sale_id', 'appointment', 'consent_path'];
    
    public function service()
	{
		return $this->belongsTo('App\Models\Services');
	}

}
