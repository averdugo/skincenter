<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone', 'type', 'store_id', 'rol','address', 'observation', 'category_id', 'social_law', 'salary', 'comuna', 'birthday_at', 'weight', 'height', 'refer_by', 'rut'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];


	public $typeArray = [
		1=>'Super Administrador',
		2=>'Administrador',
		3=>'Esteticista', 
		4=>'Cliente'
	];

	public function store()
	{
		return $this->belongsTo('App\Models\Store');
	}
	public function sales()
	{
		return $this->hasMany('App\Models\Sales','createdBy');
	}
	public function mysales()
	{
		return $this->hasMany('App\Models\Sales');
	}

	public function images()
	{
		return $this->hasMany('App\Models\Image','foreign_id')->where('type', 3);
	}

	public function histories()
	{
		return $this->hasMany('App\UserHistory');
	}

	public static function getClientsAutocomplete()
	{
		$clients = Self::where('type', 4)->get();
		$data=[];
		foreach ($clients as $key => $c) {
			array_push($data, ['id'=>$c->id,'name'=>$c->name.' '.$c->rut]);
		}
		return $data;
	}

	public static function getWorkersAutocomplete()
	{
		$workers = Self::where('type', 3)->get();
		$data=[];
		foreach ($workers as $key => $d) {
			array_push($data, ['id'=>$d->id, 'name'=> $d->name]);
		}
		return $data;
	}
}
